# Modern Visual Basic

#HSLIDE

# VBって。。。

- OOP対応してないんでしょ
- 改行するときにアンダーバー付けるんでしょ
- 変数はVariantなんでしょ
- データは取り敢えず配列に入れるんでしょ
- しかもindexは1からなんでしょ
- エラーはOnErrorとかで拾うんでしょ
- 凝った事はC++でdll書くハメになるんでしょ

#HSLIDE

# それは昔の話です

#HSLIDE

バージョン | 追加機能
---- | -----
2005 | Continue 配列の0オリジン ジェネリック型
2008 | LINQ ラムダ式 型推論 匿名型 拡張メソッド null許容型
2010 | 自動実装プロパティ 暗黙行連結 コレクション初期化子
2012 | 非同期サポート 反復子(yield)
2015 | 補完文字列 Null条件演算子

#HSLIDE

## lambda式、クロージャ

```vbnet
Dim isItemMasterExist As Boolean
Dim itemId As String
Dim namesByName = namesMaster.ToLookup(Function(x) Tuple.Create(x.NAME_DIVISION, x.NAME01))

items.Add(inc(idx), New CellImportItem With {
  .check = Function(x) If(isItemMasterExist, {}, CheckRequired("会計部門コード", x).Union(sectionById.CheckKey(x))).ToList,
  .setValue = Sub(o, cell)
    Dim s = DirectCast(o, SALES_BUDGET)
    s.SECTION_CD = If(isItemMasterExist, item.SECTION_CD, GetString(cell))
    If sectionById.ContainsKey(s.SECTION_CD) Then s.SECTION_NAME = sectionById(s.SECTION_CD).SECTION_NAME
  End Sub})
...
```

```vbnet
For Each c In r.Cells
  Dim comp = cellImportItems(c.Address.ColumnNumber)
  Dim errors = comp.check(c)
  If Not errors.Any Then
    comp.setValue(item.DataObject, c)
  End If
Next
```

#HSLIDE

## LINQ、型推論

```vbnet
Dim namesByName =
  namesMaster.ToLookup(
    Function(x) Tuple.Create(x.NAME_DIVISION, x.NAME01))
```

#HSLIDE

## DBじゃなくてもLINQ

```vbnet
Return Enumerable.Range(lotNum + 1, numLot).
  Select(Function(x) lotPrefix & x.ToString("00") & lotPostfix).
  Select(Function(x) New ProductionLot With {
    .ProductionMonth = ProductionMonth,
    .ItemId = Item.Id,
    .LotNo = x
  }).ToList
```

#HSLIDE

### DBとメモリオブジェクトの統合

```vbnet
 Dim lastLots =
   ExistingLots.Where(Function(x) x.ProductionMonth < ProductionMonth)
 lastLots =
   lastLots.Union(
     db.ProductionLots.Where(
       Function(x) x.ItemId = Item.Id And x.ProductionMonth < ProductionMonth).ToList)
 ```

#HSLIDE

# yield

```vbnet
 Public Iterator Function Validate(validationContext As ValidationContext) As
   IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate

  If material.IsLotUnmanaged = 1 AndAlso String.IsNullOrEmpty(LotNo) Then
    Yield New ValidationResult("ロット番号を設定してください。")
  End If

  If material.ExpirationDateType = ExpirationType.ByProduction AndAlso ProductionDate Is Nothing Then
    Yield New ValidationResult("製造日を設定してください。")
  End If
End Function
```

```vbnet
For Each e In DataItem.Validate(New ValidationContext(DataItem))
  Message += e.ErrorMessage & vbCrLf
Next
```

#HSLIDE

# Visual Studio

- Git Integration
- Code Lens
- NuGet
