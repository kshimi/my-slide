﻿Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Input

Public Class NumBox

#Region "Public properties"

    Public Sub New()

        ' この呼び出しはデザイナーで必要です。
        InitializeComponent()

        LayoutRoot.DataContext = Me
    End Sub

    '入力テキスト
    Public Property Text As String
        Get
            Return CType(GetValue(TextProperty), System.String)
        End Get
        Set(value As String)
            SetValue(TextProperty, value)
        End Set
    End Property

    Public Shared ReadOnly TextProperty As DependencyProperty =
    DependencyProperty.Register(
        "Text", GetType(System.String), GetType(NumBox),
        New FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault))

    'テンキーポップアップ表示方向
    Public Property Placement As Primitives.PlacementMode
        Get
            Return tenKeyPopup.Placement
        End Get
        Set(value As Primitives.PlacementMode)
            tenKeyPopup.Placement = value
        End Set
    End Property

#End Region

#Region "Event"

    Public Event TextChanged As TextChangedEventHandler

    Private Sub value_GotFocus(sender As Object, e As RoutedEventArgs) Handles value.GotFocus
        FirstPush = True
        tenKeyPopup.IsOpen = True
    End Sub

    Private Sub UserControl_LostFocus(sender As Object, e As RoutedEventArgs)
        tenKeyPopup.IsOpen = False
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Dim buttonContent As String = CType(sender, Button).Content
        Dim valueText As String = value.Text

        InChanging = True

        Select Case buttonContent
            Case "Back"
                If Len(valueText) > 0 Then
                    value.Text = Left(valueText, Len(valueText) - 1)
                    SetCursorPos()
                End If

            Case "C"
                value.Text = String.Empty

            Case "Enter"
                tenKeyPopup.IsOpen = False
                RaiseEvent TextChanged(Me, New TextChangedEventArgs(TextBox.TextChangedEvent, Nothing))

            Case Else
                If FirstPush Then
                    value.Text = String.Empty
                End If
                Me.Text = InsertButtonText(buttonContent)
                RaiseEvent TextChanged(Me, New TextChangedEventArgs(TextBox.TextChangedEvent, Nothing))

        End Select

        FirstPush = False
        InChanging = False
    End Sub

    Private Sub value_TextChanged(sender As Object, e As TextChangedEventArgs) Handles value.TextChanged
        If Not InChanging Then
            RaiseEvent TextChanged(Me, e)
        End If
    End Sub
#End Region

    Private Function InsertButtonText(text As String) As String
        If value.Text = Nothing Then
            value.Text = text
        Else
            value.Text = value.Text.Insert(value.SelectionStart, text)
        End If

        SetCursorPos()
        Return value.Text
    End Function

    Private Sub SetCursorPos()
        Dim len As Integer = value.Text.Length
        If len > -1 Then
            value.SelectionStart = len
        End If

        value.Focus()
    End Sub

    Private Property FirstPush As Boolean
    Private Property InChanging As Boolean = False

End Class
