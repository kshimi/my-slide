﻿Imports System.Linq
Imports System.Data.Entity
Imports Livet
Imports Livet.Commands
Imports AppCommon.Models
Imports DataModels

Namespace ViewModels
    Public Class DataGridViewModel
        Inherits ViewModel

        Private DataType As Type
        Private DataSourceQuery As Func(Of IList)
        Protected Overridable Sub Initialize(DataType As Type, DataSourceQuery As Func(Of IList))
            Me.DataType = DataType
            Me.DataSourceQuery = DataSourceQuery
        End Sub

        '一覧データ取得
        Public Sub DataUpdate()
            DataSource = DataSourceQuery()
            DataItem = Activator.CreateInstance(DataType)
            IsNew = True
        End Sub

        '一覧選択
        Public Sub DataSelection()
            If GridSelectedItem Is Nothing Then
                DataItem = Activator.CreateInstance(DataType)
                IsNew = True
            Else
                DataItem = GridSelectedItem.ShallowCopy(Activator.CreateInstance(DataType))
                IsNew = False
            End If
        End Sub

#Region "NewItemCommand"
        Private _NewItemCommand As ViewModelCommand

        Public ReadOnly Property NewItemCommand() As ViewModelCommand
            Get
                If _NewItemCommand Is Nothing Then
                    _NewItemCommand = New ViewModelCommand(AddressOf NewItem, AddressOf CanNewItem)
                End If
                Return _NewItemCommand
            End Get
        End Property

        Private Function CanNewItem() As Boolean
            Return True
        End Function

        Private Sub NewItem()
            DataItem = Activator.CreateInstance(DataType)
            IsNew = True
        End Sub
#End Region

#Region "UpdateCommand"
        Private _UpdateCommand As ViewModelCommand

        Public ReadOnly Property UpdateCommand() As ViewModelCommand
            Get
                If _UpdateCommand Is Nothing Then
                    _UpdateCommand = New ViewModelCommand(AddressOf Update, AddressOf CanUpdate)
                End If
                Return _UpdateCommand
            End Get
        End Property

        Private Function CanUpdate() As Boolean
            Return Not IsNew
        End Function

        Protected Overridable Sub Update()
            If Not DataItem.IsValid() Then
                Exit Sub
            End If

            Dim lUid = LoginModel.GetLoginUser().Id
            DataItem.UpdateUser = lUid

            Using db As New SMContext
                db.Set(DataType).Attach(DataItem)
                db.Entry(DataItem).State = EntityState.Modified
                db.SaveChanges()
            End Using

            DataUpdate()
        End Sub
#End Region

#Region "AddCommand"
        Private _AddCommand As ViewModelCommand

        Public ReadOnly Property AddCommand() As ViewModelCommand
            Get
                If _AddCommand Is Nothing Then
                    _AddCommand = New ViewModelCommand(AddressOf Add, AddressOf CanAdd)
                End If
                Return _AddCommand
            End Get
        End Property

        Private Function CanAdd() As Boolean
            Return IsNew
        End Function

        Private Sub Add()
            If Not DataItem.IsValid() Then
                RaisePropertyChanged("DataItem")
                Exit Sub
            End If

            Dim lUid = LoginModel.GetLoginUser().Id
            DataItem.InsertUser = lUid

            Using db As New SMContext
                If Not DataItem.IntegrityCheck(db) Then
                    RaisePropertyChanged("DataItem")
                    Exit Sub
                End If

                db.Set(DataType).Add(DataItem)
                db.SaveChanges()
            End Using

            DataUpdate()
        End Sub
#End Region

#Region "DeleteCommand"
        Private _DeleteCommand As ViewModelCommand

        Public ReadOnly Property DeleteCommand() As ViewModelCommand
            Get
                If _DeleteCommand Is Nothing Then
                    _DeleteCommand = New ViewModelCommand(AddressOf Delete, AddressOf CanDelete)
                End If
                Return _DeleteCommand
            End Get
        End Property

        Private Function CanDelete() As Boolean
            Return Not IsNew
        End Function

        Private Sub Delete()
            Dim lUid = LoginModel.GetLoginUser().Id
            DataItem.UpdateUser = lUid

            Using db As New SMContext
                db.Set(DataType).Attach(DataItem)
                DataItem.Deplecated = 1
                db.SaveChanges()
            End Using

            DataUpdate()
        End Sub
#End Region

#Region "DataSource変更通知プロパティ"
        Private _DataSource As IList

        Public Property DataSource() As IList
            Get
                Return _DataSource
            End Get
            Set(ByVal value As IList)
                If (_DataSource Is value) Then Return
                _DataSource = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "GridSelectedItem変更通知プロパティ"
        Private _GridSelectedItem As ModelBase

        Public Property GridSelectedItem() As ModelBase
            Get
                Return _GridSelectedItem
            End Get
            Set(ByVal value As ModelBase)
                If (_GridSelectedItem Is value) Then Return
                _GridSelectedItem = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "DataItem変更通知プロパティ"
        Private _DataItem As ModelBase

        Public Property DataItem() As ModelBase
            Get
                Return _DataItem
            End Get
            Set(ByVal value As ModelBase)
                If (_DataItem Is value) Then Return
                _DataItem = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "IsNew変更通知プロパティ"
        Private _IsNew As Boolean

        Public Property IsNew() As Boolean
            Get
                Return _IsNew
            End Get
            Set(ByVal value As Boolean)
                If (_IsNew = value) Then Return
                _IsNew = value
                RaisePropertyChanged()

                Me.UpdateCommand.RaiseCanExecuteChanged()
                Me.AddCommand.RaiseCanExecuteChanged()
                Me.DeleteCommand.RaiseCanExecuteChanged()
            End Set
        End Property
#End Region

    End Class
End Namespace