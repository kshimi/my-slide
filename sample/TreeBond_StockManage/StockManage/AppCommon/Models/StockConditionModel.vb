﻿Imports Livet
Imports System.Linq
Imports System.Data.Entity
Imports DataModels

Namespace Models
    Public Class StockConditionModel
        Inherits NotificationObject

        ' 在庫が格納されているDBコンテキスト型
        Private _ContextType As Type

        Public Sub New(ContextType As Type)
            _ContextType = ContextType

            Dim db As ICommonModels = Activator.CreateInstance(_ContextType)
            Warehouses = db.Materials.
                Select(Function(o) o.WarehouseCd).
                Distinct().OrderBy(Function(o) o).ToList

            Suppliers = (From s In db.Suppliers
                        Where s.Deplecated = 0
                        Order By s.Code
                        Select s).ToArray()

            db.Dispose()
        End Sub

        '倉庫の一覧
        Public Property Warehouses As IList(Of String)

        '棚の一覧
        Public Property Racks As IList(Of String)


        '取引先の一覧
        Public Property Suppliers As IList(Of MasterSupplier)

        ''倉庫
        Private Property _MasterWarehouse As String
        Public Property MasterWarehouse As String
            Get
                Return _MasterWarehouse
            End Get
            Set(value As String)
                _MasterWarehouse = value

                Dim db As ICommonModels = Activator.CreateInstance(_ContextType)
                Racks = db.Materials.Where(Function(o) o.WarehouseCd = _MasterWarehouse).Select(Function(o) o.RackCd).Distinct.OrderBy(Function(o) o).ToList
                db.Dispose()

                RaisePropertyChanged("Racks")
            End Set
        End Property

        '棚
        Public Property MasterRack As String

        '取引先
        Public Property Supplier As MasterSupplier

        '原材料
        Public Property MasterMaterial As New MasterMaterial

        'ロット番号
        Public Property LotNo As String
        Public Property LotBranchNo As String

        '開封
        Public Property OpenedFlag As OpenedType

        '持ち出し
        Public Property TakingOut As Boolean = False

        '' 日付条件項目
        Public Property ArrivalDateFrom As System.Nullable(Of DateTime)
        Public Property ArrivalDateTo As System.Nullable(Of DateTime)
        Public Property ProductionDateFrom As System.Nullable(Of DateTime)
        Public Property ProductionDateTo As System.Nullable(Of DateTime)
        Public Property OpeningDateFrom As System.Nullable(Of DateTime)
        Public Property OpeningDateTo As System.Nullable(Of DateTime)
        Public Property WorkDateFrom As System.Nullable(Of DateTime)
        Public Property WorkDateTo As System.Nullable(Of DateTime)

        Public Property ExpirationDate As System.Nullable(Of DateTime)
    End Class
End Namespace
