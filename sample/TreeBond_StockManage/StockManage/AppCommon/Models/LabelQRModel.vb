﻿Imports DataModels

Namespace Models
    Public Class LabelQRModel

        Private Const separator As Char = ";"c
        Private _code As String
        Private Const dateFormat As String = "yyyy/MM/dd"

#Region "列挙定義"
        'QRモデルデータ状態
        Enum StatusType As Integer
            Valid = 0
            Invalid = 1
            Void = 2
        End Enum
#End Region

        Public Property WarehouseCd As String
        Public Property RackCd As String
        Public Property MaterialCd As String
        Public Property MaterialName As String
        Public Property CapacityDesc As String
        Public Property ReferenceDate As String
        Public Property ExpirationDate As String
        Public Property LotNo As String
        Public Property Preliminary1 As String
        Public Property Preliminary2 As String
        Public Property LotBranchNo As String

        Private Property _Stock As Stock

#Region "Read QR code"
        Public Property Status As StatusType = StatusType.Void

        Public Sub New(code As String)
            If code Is Nothing Then
                Exit Sub
            End If

            _code = code
            Dim fields As String() = _code.Split(separator)
            If fields.Count >= 9 Then
                Status = StatusType.Valid
            Else
                Status = StatusType.Invalid
            End If

            WarehouseCd = getField(fields, 0)
            RackCd = getField(fields, 1)
            MaterialCd = getField(fields, 2)
            MaterialName = getField(fields, 3)
            CapacityDesc = getField(fields, 4)
            ReferenceDate = getField(fields, 5)
            ExpirationDate = getField(fields, 6)
            LotNo = getField(fields, 7)
            Preliminary1 = getField(fields, 8)
            Preliminary2 = getField(fields, 9)
            LotBranchNo = getField(fields, 10)
        End Sub

        Private Function getField(ByRef fields As String(), index As Integer)
            If fields.Length() > index Then
                Return fields(index).Trim()
            Else
                Return Nothing
            End If
        End Function
#End Region

#Region "Print QR label"
        Public Sub New(stock As Stock)
            _Stock = stock

        End Sub

        Public Function LabelPrintStr() As String
            Dim s As String = ""

            'QR
            s += QrCodeStr()
            s += vbTab

            '原材料CD
            s += "*" & _Stock.MasterMaterial.MaterialCd & "*"
            s += vbTab

            'LOT枝番
            s += _Stock.LotBranchNo.ToString
            s += vbTab

            '基準日付ラベル
            s += StockRefereceDateTitle()
            s += vbTab

            '倉庫棚
            s += _Stock.MasterMaterial.Location
            s += vbTab

            '原料名
            s += _Stock.MasterMaterial.Name
            s += vbTab

            '容量規格
            s += _Stock.MasterMaterial.CapacityDesc
            s += vbTab

            '基準日付
            s += StockReferenceDate()
            s += vbTab

            '使用期限
            s += dateStr(_Stock.ExpirationDate)
            s += vbTab

            'LOT
            s += _Stock.LotNo
            s += vbTab

            '注釈表示1
            s += noteStr(_Stock, 0)
            s += vbTab

            '注釈表示2
            s += noteStr(_Stock, 1)
            s += vbTab

            '注釈表示3
            s += noteStr(_Stock, 2)
            s += vbTab

            '注釈表示4
            s += noteStr(_Stock, 3)
            s += vbTab

            '注釈表示5
            s += noteStr(_Stock, 4)
            s += vbTab

            '発行枚数
            s += "1"

            Return s
        End Function

        Private Function QrCodeStr() As String
            Dim s As String = ""

            s += PadStr(_Stock.MasterMaterial.WarehouseCd, 4)
            s += ";"
            s += PadStr(_Stock.MasterMaterial.RackCd, 5)
            s += ";"
            s += PadStr(_Stock.MasterMaterial.MaterialCd, 9)
            s += ";"
            s += PadStr(_Stock.MasterMaterial.Name, 30)
            s += ";"
            s += PadStr(_Stock.MasterMaterial.CapacityDesc, 25)
            s += ";"
            s += PadStr(StockReferenceDate, 10)
            s += ";"
            s += PadStr(dateStr(_Stock.ExpirationDate), 10)
            s += ";"
            s += PadStr(_Stock.LotNo, 15)
            s += ";"
            s += PadStr(_Stock.StatusTextWoCR, 36)
            s += ";"
            s += PadStr("", 36)
            s += ";"
            s += PadStr(StockLotBranchNo, 5)

            Return s
        End Function

        Private Function PadStr(s As String, l As Integer) As String
            Return If(s, "").PadRight(l)
        End Function

        Private Function StockRefereceDateTitle() As String
            If _Stock.OpenedFlag = OpenedType.Opened Then
                Return "開封日　　："
            End If

            Select Case _Stock.MasterMaterial.ExpirationDateType
                Case ExpirationType.ByArrival
                    Return "受入日付　："
                Case ExpirationType.ByProduction
                    Return "製造日　　："
                Case Else
                    Return ""
            End Select
        End Function

        Private Function StockReferenceDate() As String
            If _Stock.OpenedFlag = OpenedType.Opened Then
                Return _Stock.OpeningDate.Value.ToString(dateFormat)
            End If

            Select Case _Stock.MasterMaterial.ExpirationDateType
                Case ExpirationType.ByArrival
                    Return dateStr(_Stock.ArrivalHistory.ArrivalDate)
                Case ExpirationType.ByProduction
                    Return dateStr(_Stock.ArrivalHistory.ProductionDate)
                Case Else
                    Return ""
            End Select
        End Function

        Private Function StockLotBranchNo() As String
            If _Stock.LotBranchNo Is Nothing Then
                Return ""
            Else
                Return _Stock.LotBranchNo.Value
            End If
        End Function

        Private Function dateStr(d As System.Nullable(Of DateTime)) As String
            Return If(d.HasValue, d.Value.ToString(dateFormat), "")
        End Function

        Private Function noteStr(stock As Stock, notesIdx As Integer)
            Dim notes As New List(Of String)

            If stock.OpenedFlag Then
                notes.Add(stock.OpenedFlagText)
            End If

            If stock.ExtendedFlag Then
                notes.Add(stock.ExtendedFlagText)
            End If

            If stock.ReprintFlag Then
                notes.Add(stock.ReprintFlagText)
            End If

            If stock.MasterMaterial.CantExtension Then
                notes.Add("使用期限延長不可")
            End If

            If stock.MasterMaterial.IsOnetime Then
                notes.Add("使い切り")
            End If

            If stock.MasterMaterial.IsEnsureStock Then
                notes.Add("在庫確保")
            End If

            If stock.MasterMaterial.IsLotUnmanaged Then
                notes.Add("ロット管理対象")
            End If

            If stock.MasterMaterial.IsResetExtensionWhenOpen Then
                notes.Add("開封時使用期限再設定")
            End If

            If stock.MasterMaterial.IsSimilar Then
                notes.Add("類似原料")
            End If

            Return If(notes.ElementAtOrDefault(notesIdx), String.Empty)
        End Function
#End Region

    End Class
End Namespace