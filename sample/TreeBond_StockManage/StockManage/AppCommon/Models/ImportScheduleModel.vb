﻿Imports System.IO
Imports System.Globalization
Imports System.Linq
Imports System.Data.Entity
Imports DataModels
Imports Livet

Namespace Models
    Public Class ImportScheduleModel
        Inherits NotificationObject

        'NotificationObjectはプロパティ変更通知の仕組みを実装したオブジェクトです。

        Private Shared dateFormats As String() = {"yMd", "yyMMdd", "yyyyMMdd", "yy/M/d", "yy/MM/dd", "yyyy/M/d", "yyyy/MM/dd", "yy-M-d", "yy-MM-dd", "yyyy-M-d", "yyyy-MM-dd"}

        Public Property filename As String

        Public Sub New(filename As String)
            Me.filename = filename

            Dim ext As String = Path.GetExtension(filename)
            If ext.ToUpper <> ".CSV" Then
                _IsValid = False
                Exit Sub
            Else
                _IsValid = True
            End If

            Dim fName As String = Path.GetFileNameWithoutExtension(filename)
            Dim items As String() = fName.Split("_")
            _ScheduleDate = Nothing
            For Each item In items
                If DateTime.TryParseExact(item, dateFormats, Nothing, DateTimeStyles.None, _ScheduleDate) Then
                    Exit For
                End If
            Next
        End Sub

        Private Property _IsValid As Boolean = False
        Public ReadOnly Property IsValid As Boolean
            Get
                Return _IsValid
            End Get
        End Property

        Private Property _ScheduleDate As DateTime
        Public ReadOnly Property ScheduleDate As DateTime
            Get
                Return _ScheduleDate
            End Get
        End Property

        Public Function GetData() As IList(Of Item)

            Using reader As StreamReader = File.OpenText(filename)
                Dim csv = New CsvHelper.CsvReader(reader)
                csv.Configuration.HasHeaderRecord = False
                csv.Configuration.RegisterClassMap(New ItemMap)

                Dim record = csv.GetRecords(Of Item)()
                Return CheckMaterialMaster(record.ToList())
            End Using
        End Function

        '受入予定データの原材料マスタをチェック、紐付けする
        Public Function CheckMaterialMaster(items As IList(Of Item)) As IList(Of Item)
            Using db As New SMContext
                For Each Item In items
                    Dim material As MasterMaterial = db.Materials.FirstOrDefault(Function(o) o.MaterialCd = Item.MaterialCd)
                    If material IsNot Nothing Then
                        Item.IsMasterExist = True
                        Item.MasterMaterial_Id = material.Id
                        Item.MasterMaterial = material

                        If material.MasterSupplier_Id IsNot Nothing Then
                            Dim supplier As MasterSupplier = db.Suppliers.Find(material.MasterSupplier_Id)
                            Item.MasterMaterial.MasterSupplier = supplier
                        End If
                    End If
                Next
            End Using

            Return items
        End Function

        '受入予定データをデータベースに反映する
        Public Shared Sub UpdateScheduleFromItems(items As IList(Of Item))
            Using db As New SMContext
                For Each item In items
                    If Not item.IsMasterExist OrElse item.ScheduleNum = 0 OrElse item.ScheduleDate Is Nothing Then
                        Continue For
                    End If

                    ' 同日に同一原材料の受入予定があったときは、該当データを更新する
                    Dim q = db.Schedules.Where(Function(o) o.Deplecated = 0 AndAlso
                                                   o.MasterMaterial_Id = item.MasterMaterial_Id AndAlso
                                                   o.ScheduleDate = item.ScheduleDate)

                    Dim schedule As Schedule

                    Dim lU = LoginModel.GetLoginUser()
                    Dim lUid = If(lU IsNot Nothing, lU.Id, 0)
                    If q.Any Then
                        schedule = q.First
                        schedule.NumSchedule = item.ScheduleNum

                        If schedule.NumArrival >= schedule.NumSchedule Then
                            schedule.CompleteFlag = DataModels.Schedule.CompletionType.Complete
                        Else
                            schedule.CompleteFlag = DataModels.Schedule.CompletionType.Incomplete
                        End If

                        schedule.UpdateUser = lUid
                        schedule.UpdateDate = DateTime.Now()
                    Else
                        ' 受入予定を追加する
                        schedule = New Schedule

                        schedule.ScheduleDate = item.ScheduleDate
                        schedule.NumSchedule = item.ScheduleNum
                        schedule.MasterMaterial_Id = item.MasterMaterial_Id

                        schedule.InsertUser = lUid
                        db.Schedules.Add(schedule)
                    End If
                Next

                db.SaveChanges()
            End Using
        End Sub

        ' CSV読み込みデータ
        Public Class Item
            Public Property MaterialCd As String
            Public Property MaterialName As String
            Public Property ScheduleDate As DateTime?
            Public Property ScheduleNum As Double

            Public Property IsMasterExist As Boolean
            Public Property MasterMaterial_Id As Integer
            Public Property MasterMaterial As MasterMaterial
        End Class

        ' CSVファイルのマッピング定義
        Public NotInheritable Class ItemMap
            Inherits CsvHelper.Configuration.CsvClassMap(Of Item)

            Public Sub New()
                Map(Function(m) m.MaterialCd).Index(0)
                Map(Function(m) m.MaterialName).Index(1)

                Map(Function(m) m.ScheduleDate).ConvertUsing(Function(r)
                                                                 Dim s As String = Nothing
                                                                 If r.TryGetField(2, s) Then
                                                                     Return ParseDate(s)
                                                                 Else
                                                                     Return Nothing
                                                                 End If
                                                             End Function)

                Map(Function(m) m.ScheduleNum).ConvertUsing(Function(r)
                                                                Dim s As String = Nothing
                                                                Dim v As Double
                                                                If r.TryGetField(3, s) AndAlso Double.TryParse(s, v) Then
                                                                    Return v
                                                                Else
                                                                    Return 0
                                                                End If
                                                            End Function)
            End Sub

            Private Function ParseDate(s As String) As DateTime?
                Dim d As DateTime
                If DateTime.TryParseExact(s, dateFormats, Nothing, DateTimeStyles.None, d) Then
                    Return d
                Else
                    Return Nothing
                End If
            End Function
        End Class
    End Class
End Namespace
