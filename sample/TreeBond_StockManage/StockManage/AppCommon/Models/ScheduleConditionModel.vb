﻿Imports Livet
Imports DataModels

Namespace Models
    Public Class ScheduleConditionModel
        Inherits NotificationObject

        ' 発行予定が格納されているDBコンテキスト型
        Private _ContextType As Type

        Public Sub New(ContextType As Type)
            _ContextType = ContextType

            Dim db As ICommonModels = Activator.CreateInstance(_ContextType)
            Warehouses = db.Materials.
                Select(Function(o) o.WarehouseCd).
                Distinct().OrderBy(Function(o) o).ToList

            Suppliers = (From s In db.Suppliers
                        Where s.Deplecated = 0
                        Order By s.Code
                        Select s).ToArray()

            db.Dispose()
        End Sub

        Public Property IsSearchDelayed As Boolean = False
        Public Property IsSearchToday As Boolean = True
        Public Property IsSearchFuture As Boolean = False

        '倉庫の一覧
        Public Property Warehouses As IList(Of String)

        '棚の一覧
        Public Property Racks As IList(Of String)

        '取引先の一覧
        Public Property Suppliers As IList(Of MasterSupplier)

        ''倉庫
        Private Property _MasterWarehouse As String
        Public Property MasterWarehouse As String
            Get
                Return _MasterWarehouse
            End Get
            Set(value As String)
                _MasterWarehouse = value

                Dim db As ICommonModels = Activator.CreateInstance(_ContextType)
                Racks = db.Materials.Where(Function(o) o.WarehouseCd = _MasterWarehouse).Select(Function(o) o.RackCd).Distinct.OrderBy(Function(o) o).ToList
                db.Dispose()

                RaisePropertyChanged("Racks")
            End Set
        End Property

        '棚
        Public Property MasterRack As String

        '取引先
        Public Property Supplier As MasterSupplier

        '原材料
        Public Property MasterMaterial As New MasterMaterial

        '遅れチェック日
        Public Property DelayCheckDate As System.Nullable(Of DateTime)
    End Class
End Namespace