﻿Imports Livet
Imports System.Linq
Imports System.Data.Entity
Imports DataModels

Namespace Models
    Public Class LoginModel
        Inherits NotificationObject

        ' ユーザーマスタが格納されているDBコンテキスト型
        Private _ContextType As Type

        Public Sub New(ContextType As Type)
            _ContextType = ContextType
        End Sub

#Region "UserId変更通知プロパティ"
        Private _UserId As String

        Public Property UserId() As String
            Get
                Return _UserId
            End Get
            Set(ByVal value As String)
                If (_UserId = value) Then Return
                _UserId = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "Message変更通知プロパティ"
        Private _Message As String

        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                If (_Message = value) Then Return
                _Message = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "LoginUser変更通知プロパティ"
        Private _LoginUser As MasterUser

        Public Property LoginUser() As MasterUser
            Get
                Return _LoginUser
            End Get
            Set(ByVal value As MasterUser)
                If (_LoginUser Is value) Then Return
                _LoginUser = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

        Public Function CheckLogin() As Boolean
            Message = ""
            LoginUser = Nothing
            Dim loginFlag As Boolean = False

            If _UserId Is Nothing OrElse _UserId.Length = 0 Then
                Message = "ユーザーIDを入力して下さい。"
                Return loginFlag
            End If

            'ユーザーマスタを検索する
            Dim db As ICommonModels = Activator.CreateInstance(_ContextType)
            Dim q = From u In db.Users
                    Where u.Deplecated = 0 AndAlso u.UserCd = UserId
                    Select u

            If q.Any() Then
                LoginUser = q.First()
                loginFlag = True
            Else
                Message = "ログインできません"
            End If
            db.Dispose()

            Return loginFlag
        End Function

#Region "アプリケーション共有データ操作"
        Public Shared Sub SetLoginUser(user As MasterUser)
            Dim app As IAppContext = System.Windows.Application.Current
            app.LoginUser = user
        End Sub

        Public Shared Sub DisposeLoginUser()
            Dim app As IAppContext = System.Windows.Application.Current
            app.LoginUser = Nothing
        End Sub
        Public Shared Function GetLoginUser() As MasterUser
            Dim app As IAppContext = System.Windows.Application.Current
            Return If(app IsNot Nothing, app.LoginUser, Nothing)
        End Function
#End Region

    End Class
End Namespace
