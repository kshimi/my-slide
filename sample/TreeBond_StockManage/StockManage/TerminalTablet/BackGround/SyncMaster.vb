﻿Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Net.Http.Formatting
Imports System.Data.Entity
Imports System.Data.Entity.Design
Imports System.Data.SQLite
Imports System.Linq
Imports System.Configuration
Imports System.Globalization
Imports System.Threading.Tasks
Imports DataModels

Public Class SyncMaster

    Delegate Function revisionDelegate() As Integer
    Private Structure targetModel
        Public context As DbSet
        Public colType As Type
        Public newest As revisionDelegate
    End Structure

    Private Shared Function setDataDef(db As LocalContext) As Dictionary(Of Type, targetModel)
        Dim dataDef As New Dictionary(Of Type, targetModel)

        'マスタごとの定義

        dataDef.Add(GetType(MasterSection), New targetModel With {.context = db.Sections, .colType = GetType(List(Of MasterSection)),
                                                          .newest = Function() If(db.Sections.Count > 0,
                                                                       db.Sections.OrderByDescending(Function(x) x.Revision).First.Revision,
                                                                       0)})
        dataDef.Add(GetType(MasterSupplier), New targetModel With {.context = db.Suppliers, .colType = GetType(List(Of MasterSupplier)),
                                                  .newest = Function() If(db.Suppliers.Count > 0,
                                                               db.Suppliers.OrderByDescending(Function(x) x.Revision).First.Revision,
                                                               0)})
        dataDef.Add(GetType(MasterUser), New targetModel With {.context = db.Users, .colType = GetType(List(Of MasterUser)),
                                                               .newest = Function() If(db.Users.Count > 0,
                                                                             db.Users.OrderByDescending(Function(x) x.Revision).First.Revision,
                                                                             0)})

        dataDef.Add(GetType(MasterMaterial), New targetModel With {.context = db.Materials, .colType = GetType(List(Of MasterMaterial)),
                                       .newest = Function() If(db.Materials.Count > 0,
                                                     db.Materials.OrderByDescending(Function(x) x.Revision).First.Revision,
                                                     0)})
        dataDef.Add(GetType(Schedule), New targetModel With {.context = db.Schedules, .colType = GetType(List(Of Schedule)),
                                                             .newest = Function() If(db.Schedules.Count > 0,
                                                                                     db.Schedules.OrderByDescending(Function(x) x.Revision).First.Revision,
                                                                                     0)})
        dataDef.Add(GetType(ArrivalHistory), New targetModel With {.context = db.ArrivalHistories, .colType = GetType(List(Of ArrivalHistory)),
                                                     .newest = Function() If(db.ArrivalHistories.Count > 0,
                                                                             db.ArrivalHistories.OrderByDescending(Function(x) x.Revision).First.Revision,
                                                                             0)})
        dataDef.Add(GetType(Stock), New targetModel With {.context = db.Stocks, .colType = GetType(List(Of Stock)),
                                                     .newest = Function() If(db.Stocks.Count > 0,
                                                                             db.Stocks.OrderByDescending(Function(x) x.Revision).First.Revision,
                                                                             0)})

        Return dataDef
    End Function

    ''' <summary>
    ''' マスターデータをサーバーより差分取得する
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function SyncMasters() As String
        Try
            Dim db = New LocalContext
            Dim dataDef = setDataDef(db)

            Dim updateCount As Long = 0
            For Each t As Type In dataDef.Keys
                updateCount += SyncMaster(t, dataDef, db)
            Next

            db.Dispose()
            Return ""

        Catch ex As Exception
            Dim msg = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg += ex.InnerException.Message

                If ex.InnerException.InnerException IsNot Nothing Then
                    msg += ex.InnerException.InnerException.Message
                End If
            End If

            Return msg
        End Try
    End Function

    'サーバより追加データ取得
    Private Shared Function SyncMaster(ByVal type As Type, ByVal dataDef As Dictionary(Of Type, targetModel), ByVal db As LocalContext) As Long
        Dim client = New HttpClient
        Dim pluralizer As PluralizationServices.PluralizationService =
            PluralizationServices.PluralizationService.CreateService(New CultureInfo("en-US"))

        Dim newest As ModelBase
        Dim taskResponse As Task(Of HttpResponseMessage) = client.GetAsync(ServerUri(NewestAction(type)))
        Dim taskContent As Task(Of Object)
        Using response As HttpResponseMessage = taskResponse.Result
            taskContent = response.Content.ReadAsAsync(type)
            newest = taskContent.Result
        End Using
        taskResponse.Wait()
        taskContent.Wait()

        Dim localNewest As Integer = dataDef(type).newest()
        Dim updateCount As Long = 0
        If newest IsNot Nothing AndAlso localNewest < newest.Revision Then
            Dim taskNewerResponse As Task(Of HttpResponseMessage) = client.GetAsync(ServerUri(GetNewerAction(type), localNewest.ToString()))
            Dim taskAddData As Task(Of Object)

            Using response As HttpResponseMessage = taskNewerResponse.Result
                taskAddData = response.Content.ReadAsAsync(dataDef(type).colType)
                Dim adddata = taskAddData.Result

                For Each o In adddata
                    Dim localData = dataDef(type).context.Find(o.Id)
                    If localData Is Nothing Then
                        Dim sql As String = "INSERT INTO " &
                            pluralizer.Pluralize(type.Name) &
                            " (Id, Revision, Deplecated, InsertUser, UpdateUser) values (" &
                            o.Id.ToString &
                            ", 0, 0, 0, 0);"
                        db.Database.ExecuteSqlCommand(sql)
                    End If

                    If TypeOf o Is IDetachable Then
                        DirectCast(o, IDetachable).Detach(db)
                    End If

                    dataDef(type).context.Attach(o)
                    db.Entry(o).State = EntityState.Modified

                    updateCount += 1
                    If updateCount Mod 100 = 0 Then
                        db.SaveChanges()
                    End If
                Next
                db.SaveChanges()
            End Using

            taskNewerResponse.Wait()
            taskAddData.Wait()
        End If

        Return updateCount
    End Function

    '最新データ取得コントローラー名
    Private Shared Function NewestAction(ByVal type As Type) As String
        Return type.Name & "Newest"
    End Function

    '差分データ取得コントローラー名
    Private Shared Function GetNewerAction(ByVal type As Type) As String
        Dim pluralizer As PluralizationServices.PluralizationService =
            PluralizationServices.PluralizationService.CreateService(New CultureInfo("en-US"))

        Dim cName = pluralizer.Pluralize(type.Name) & "ByRevision"
        Return cName
    End Function

    'コントローラー呼び出しURI文字列
    Private Shared Function ServerUri(ByVal controller As String, Optional ByVal param As String = "") As String
        Dim baseUri As String = ConfigurationManager.AppSettings.Get("baseUri")

        Return baseUri & controller & If(param = "", "", "/") & param
    End Function
End Class
