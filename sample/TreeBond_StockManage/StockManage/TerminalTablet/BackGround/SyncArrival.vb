﻿Imports System.Data.Entity
Imports System.Data.SQLite
Imports System.Linq
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Net.Http.Formatting
Imports AppCommon
Imports DataModels

Public Class SyncArrival

    ''' <summary>
    ''' 発行データをサーバーに連携する
    ''' </summary>
    ''' <param name="arrival"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function PushArrival(arrival As Arrival) As String
        Try
            Dim client As New HttpClient
            Dim uri As String = ConfigurationManager.AppSettings.Get("baseUri") & "Arrival"

            Dim res = client.PostAsJsonAsync(uri, arrival)
            If res.Result.IsSuccessStatusCode Then
                Return ""
            Else
                Dim c = res.Result.Content
                Return c.ReadAsStringAsync().Result()
            End If
        Catch ex As Exception
            Dim msg = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg += ex.InnerException.Message

                If ex.InnerException.InnerException IsNot Nothing Then
                    msg += ex.InnerException.InnerException.Message
                End If
            End If

            Return msg
        End Try
    End Function

    ''' <summary>
    ''' ローカルの発行未同期データをサーバーに連携する
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function SyncArrivals() As String
        Try
            Dim client As New HttpClient
            Dim uri As String = ConfigurationManager.AppSettings.Get("baseUri") & "Arrival"

            Using db As New LocalContext
                Dim query = From o In db.Arrivals
                            Where o.Status <> LocalModelBase.StatusType.Synced And o.Deplecated = 0
                            Order By o.Id
                            Select o

                For Each o In query
                    Dim res = client.PostAsJsonAsync(uri, o)
                    If res.Result.IsSuccessStatusCode Then
                        o.Status = Arrival.StatusType.Synced
                    Else
                        o.Status = LocalModelBase.StatusType.Errored

                        Dim c = res.Result.Content
                        Return c.ReadAsStringAsync().Result()
                    End If
                Next

                db.SaveChanges()
            End Using

            Return ""
        Catch ex As Exception
            Dim msg = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg += ex.InnerException.Message

                If ex.InnerException.InnerException IsNot Nothing Then
                    msg += ex.InnerException.InnerException.Message
                End If
            End If

            Return msg
        End Try
    End Function

    ''' <summary>
    ''' ローカルの発行未同期データをキャンセルする
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CancelArrivals() As String
        Try
            Using db As New LocalContext
                Dim q = From o In db.Arrivals
                        Where o.Status <> LocalModelBase.StatusType.Synced And o.Deplecated = 0
                        Order By o.Id
                        Select o

                For Each o In q
                    o.Deplecated = 1
                Next

                db.SaveChanges()
            End Using

            Return ""
        Catch ex As Exception
            Dim msg = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg += ex.InnerException.Message

                If ex.InnerException.InnerException IsNot Nothing Then
                    msg += ex.InnerException.InnerException.Message
                End If
            End If

            Return msg
        End Try
    End Function
End Class
