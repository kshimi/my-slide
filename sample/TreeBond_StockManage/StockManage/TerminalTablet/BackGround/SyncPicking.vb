﻿Imports System.Data.Entity
Imports System.Linq
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Net.Http.Formatting
Imports AppCommon
Imports DataModels

Public Class SyncPicking

    ''' <summary>
    ''' ローカルのピッキング未同期データをサーバーに連携する
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SyncPickings() As String
        Try
            Dim client As New HttpClient
            Dim url As String = ConfigurationManager.AppSettings.Get("baseUri") & "Picking"

            Using db As New LocalContext
                Dim query = From o In db.Pickings
                            Where o.Status <> Picking.StatusType.Synced And o.Deplecated = 0
                            Order By o.Id
                            Select o

                For Each o In query
                    Dim res = client.PostAsJsonAsync(url, o)
                    If res.Result.IsSuccessStatusCode Then
                        o.Status = Picking.StatusType.Synced
                    Else
                        o.Status = LocalModelBase.StatusType.Errored

                        ' ピッキングデータ送信がエラーになり、在庫がピッキングデータ送信時より新しく
                        ' なっていた場合、当該ピッキングデータを削除
                        Dim stock = db.Stocks.Find(o.Stock_Id)
                        If stock IsNot Nothing AndAlso stock.Revision > o.UnderlyingStockRevision Then
                            o.Deplecated = 1
                        End If
                    End If
                Next

                db.SaveChanges()
            End Using

            Return ""

        Catch ex As Exception
            Dim msg = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg += ex.InnerException.Message

                If ex.InnerException.InnerException IsNot Nothing Then
                    msg += ex.InnerException.InnerException.Message
                End If
            End If

            Return msg
        End Try
    End Function

    ''' <summary>
    ''' ローカルのピッキング未同期データをキャンセルする
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CancelPickings() As String
        Try
            Using db As New LocalContext
                Dim q = From o In db.Pickings
                        Where o.Status <> LocalModelBase.StatusType.Synced And o.Deplecated = 0
                        Order By o.Id
                        Select o

                For Each o In q
                    o.Deplecated = 1
                Next

                db.SaveChanges()
            End Using

            Return ""
        Catch ex As Exception
            Dim msg = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg += ex.InnerException.Message

                If ex.InnerException.InnerException IsNot Nothing Then
                    msg += ex.InnerException.InnerException.Message
                End If
            End If

            Return msg
        End Try
    End Function
End Class
