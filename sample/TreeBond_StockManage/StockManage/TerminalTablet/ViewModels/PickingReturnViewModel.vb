﻿Imports AppCommon
Imports AppCommon.Models
Imports DataModels

Namespace ViewModels
    Public Class PickingReturnViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Private db As New LocalContext

        Public Sub Initialize()
            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub

#Region "StockPickingCheck変更通知プロパティ"
        Private _StockPickingCheck As New Models.StockPickingReturnCheckModel

        Public Property StockPickingCheck() As Models.StockPickingReturnCheckModel
            Get
                Return _StockPickingCheck
            End Get
            Set(ByVal value As Models.StockPickingReturnCheckModel)
                If (_StockPickingCheck Is value) Then Return
                _StockPickingCheck = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "QRinput変更通知プロパティ"
        Private _QRinput As String

        Public Property QRinput() As String
            Get
                Return _QRinput
            End Get
            Set(ByVal value As String)
                If (_QRinput = value) Then Return
                _QRinput = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "QREnterCommand"
        Private _QREnterCommand As ViewModelCommand

        Public ReadOnly Property QREnterCommand() As ViewModelCommand
            Get
                If _QREnterCommand Is Nothing Then
                    _QREnterCommand = New ViewModelCommand(AddressOf QREnter, AddressOf CanQREnter)
                End If
                Return _QREnterCommand
            End Get
        End Property

        Private Function CanQREnter() As Boolean
            Return True
        End Function

        Private Sub QREnter()
            If Trim(_QRinput) = String.Empty Then
                Exit Sub
            End If

            Dim qr As LabelQRModel = New LabelQRModel(_QRinput)
            AppCommon.Util.ShortBeep()

            SelectStock(qr)

            RaisePropertyChanged("StockPickingCheck")
            _PickingOpenCommand.RaiseCanExecuteChanged()
            _PickingNotOpenCommand.RaiseCanExecuteChanged()

            QRinput = ""
        End Sub
#End Region

        Private Function SelectStock(qr As LabelQRModel) As Boolean
            Return _StockPickingCheck.CheckStock(qr)
        End Function

#Region "PickingOpenCommand"
        Private _PickingOpenCommand As ViewModelCommand

        Public ReadOnly Property PickingOpenCommand() As ViewModelCommand
            Get
                If _PickingOpenCommand Is Nothing Then
                    _PickingOpenCommand = New ViewModelCommand(AddressOf PickingOpen, AddressOf CanPickingOpen)
                End If
                Return _PickingOpenCommand
            End Get
        End Property

        Private Function CanPickingOpen() As Boolean
            If StockPickingCheck.Material IsNot Nothing AndAlso
                StockPickingCheck.Stock IsNot Nothing Then

                Return True
            Else
                Return False
            End If
        End Function

        Private Sub PickingOpen()
            Dim picking As New Picking
            picking.PickingFlag = PickingType.ReturningInOpen
            picking.Stock_Id = _StockPickingCheck.Stock.Id
            picking.UnderlyingStockRevision = _StockPickingCheck.Stock.Revision

            Dim lUid = LoginModel.GetLoginUser().Id
            picking.InsertUser = lUid
            picking.UpdateUser = lUid

            db.Pickings.Add(picking)
            db.SaveChanges()

            '入力項目を初期化
            StockPickingCheck.Material = Nothing
            StockPickingCheck.Stock = Nothing
            StockPickingCheck.Message = "持ち戻り受け付けました。"
            RaisePropertyChanged("StockPickingCheck")

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub
#End Region

#Region "PickingNotOpenCommand"
        Private _PickingNotOpenCommand As ViewModelCommand

        Public ReadOnly Property PickingNotOpenCommand() As ViewModelCommand
            Get
                If _PickingNotOpenCommand Is Nothing Then
                    _PickingNotOpenCommand = New ViewModelCommand(AddressOf PickingNotOpen, AddressOf CanPickingNotOpen)
                End If
                Return _PickingNotOpenCommand
            End Get
        End Property

        Private Function CanPickingNotOpen() As Boolean
            If StockPickingCheck.Material IsNot Nothing AndAlso
                StockPickingCheck.Stock IsNot Nothing AndAlso
                StockPickingCheck.Stock.OpenedFlag = OpenedType.NotOpen Then

                Return True
            Else
                Return False
            End If
        End Function

        Private Sub PickingNotOpen()
            Dim picking As New Picking
            picking.PickingFlag = PickingType.ReturningInNotopen
            picking.Stock_Id = _StockPickingCheck.Stock.Id
            picking.UnderlyingStockRevision = _StockPickingCheck.Stock.Revision

            Dim lUid = LoginModel.GetLoginUser().Id
            picking.InsertUser = lUid
            picking.UpdateUser = lUid

            db.Pickings.Add(picking)
            db.SaveChanges()

            '入力項目を初期化
            StockPickingCheck.Material = Nothing
            StockPickingCheck.Stock = Nothing
            StockPickingCheck.Message = "持ち戻り受け付けました。"
            RaisePropertyChanged("StockPickingCheck")

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub
#End Region

#Region "CancelCommand"
        Private _CancelCommand As ViewModelCommand

        Public ReadOnly Property CancelCommand() As ViewModelCommand
            Get
                If _CancelCommand Is Nothing Then
                    _CancelCommand = New ViewModelCommand(AddressOf Cancel, AddressOf CanCancel)
                End If
                Return _CancelCommand
            End Get
        End Property

        Private Function CanCancel() As Boolean
            Return True
        End Function

        Private Sub Cancel()
            '入力項目を初期化
            StockPickingCheck.Material = Nothing
            StockPickingCheck.Stock = Nothing
            StockPickingCheck.Message = "ピッキングをキャンセルしました。"
            RaisePropertyChanged("StockPickingCheck")
            _PickingOpenCommand.RaiseCanExecuteChanged()
            _PickingNotOpenCommand.RaiseCanExecuteChanged()

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub
#End Region

    End Class
End Namespace