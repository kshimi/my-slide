﻿Imports AppCommon
Imports AppCommon.Models
Imports DataModels
Imports Livet

Namespace ViewModels
    Public Class PickingReissueViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Private db As New LocalContext

        Public Sub Initialize()
            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub

#Region "StockPickingCheck変更通知プロパティ"
        Private _StockPickingCheck As New Models.StockPickingReissueCheckModel

        Public Property StockPickingCheck() As Models.StockPickingReissueCheckModel
            Get
                Return _StockPickingCheck
            End Get
            Set(ByVal value As Models.StockPickingReissueCheckModel)
                If (_StockPickingCheck Is value) Then Return
                _StockPickingCheck = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "QRinput変更通知プロパティ"
        Private _QRinput As String

        Public Property QRinput() As String
            Get
                Return _QRinput
            End Get
            Set(ByVal value As String)
                If (_QRinput = value) Then Return
                _QRinput = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "QREnterCommand"
        Private _QREnterCommand As ViewModelCommand

        Public ReadOnly Property QREnterCommand() As ViewModelCommand
            Get
                If _QREnterCommand Is Nothing Then
                    _QREnterCommand = New ViewModelCommand(AddressOf QREnter, AddressOf CanQREnter)
                End If
                Return _QREnterCommand
            End Get
        End Property

        Private Function CanQREnter() As Boolean
            Return True
        End Function

        Private Sub QREnter()
            If Trim(_QRinput) = String.Empty Then
                Exit Sub
            End If

            Dim qr As LabelQRModel = New LabelQRModel(_QRinput)
            AppCommon.Util.ShortBeep()

            SelectStock(qr)
            If _StockPickingCheck.Stock IsNot Nothing Then
                ModifyStock = _StockPickingCheck.Stock.ShallowCopy(New Stock)
            Else
                ModifyStock = Nothing
            End If

            RaisePropertyChanged("StockPickingCheck")
            _ReissueCommand.RaiseCanExecuteChanged()

            QRinput = ""
        End Sub

        Private Function SelectStock(qr As LabelQRModel) As Boolean
            Return _StockPickingCheck.CheckStock(qr)
        End Function
#End Region

#Region "ModifyStock変更通知プロパティ"
        Private _ModifyStock As Stock

        Public Property ModifyStock() As Stock
            Get
                Return _ModifyStock
            End Get
            Set(ByVal value As Stock)
                If (_ModifyStock Is value) Then Return
                _ModifyStock = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ReissueCommand"
        Private _ReissueCommand As ViewModelCommand

        Public ReadOnly Property ReissueCommand() As ViewModelCommand
            Get
                If _ReissueCommand Is Nothing Then
                    _ReissueCommand = New ViewModelCommand(AddressOf Reissue, AddressOf CanReissue)
                End If
                Return _ReissueCommand
            End Get
        End Property

        Private Function CanReissue() As Boolean
            Return If(StockPickingCheck.Material IsNot Nothing AndAlso
                      StockPickingCheck.Stock IsNot Nothing,
                      True, False)
        End Function

        Private Sub Reissue()
            ' 未開封（半端原料でない）ときは開封日の設定をクリアする
            If ModifyStock.OpenedFlag = OpenedType.NotOpen Then
                ModifyStock.OpeningDate = Nothing
            End If

            ' 開封済み（半端原料）のとき、開封日が設定されていないとエラーにする
            If ModifyStock.OpenedFlag = OpenedType.Opened AndAlso Not ModifyStock.OpeningDate.HasValue Then
                StockPickingCheck.Message = "半端原料の場合、開封日の設定が必要です。"
                RaisePropertyChanged("StockPickingCheck")
                Exit Sub
            End If

            Dim reissue As New Reissue
            reissue.Stock_Id = _StockPickingCheck.Stock.Id
            reissue.UnderlyingStockRevision = _StockPickingCheck.Stock.Revision
            reissue.ModifyExpirationDate = ModifyStock.ExpirationDate
            reissue.ModifyOpeningDate = ModifyStock.OpeningDate
            reissue.ModifyExtendedFlag = ModifyStock.ExtendedFlag
            reissue.ModifyOpenedFlag = ModifyStock.OpenedFlag

            Dim lUid = LoginModel.GetLoginUser().Id
            reissue.InsertUser = lUid
            reissue.UpdateUser = lUid

            db.Reissues.Add(reissue)
            db.SaveChanges()

            '入力項目を初期化
            StockPickingCheck.Material = Nothing
            StockPickingCheck.Stock = Nothing
            ModifyStock = Nothing
            StockPickingCheck.Message = "ラベル再発行受け付けました。"
            RaisePropertyChanged("StockPickingCheck")
            RaisePropertyChanged("ModifyStock")

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub
#End Region

#Region "CancelCommand"
        Private _CancelCommand As ViewModelCommand

        Public ReadOnly Property CancelCommand() As ViewModelCommand
            Get
                If _CancelCommand Is Nothing Then
                    _CancelCommand = New ViewModelCommand(AddressOf Cancel, AddressOf CanCancel)
                End If
                Return _CancelCommand
            End Get
        End Property

        Private Function CanCancel() As Boolean
            Return True
        End Function

        Private Sub Cancel()
            '入力項目を初期化
            StockPickingCheck.Material = Nothing
            StockPickingCheck.Stock = Nothing
            ModifyStock = Nothing
            StockPickingCheck.Message = "ピッキングをキャンセルしました。"
            RaisePropertyChanged("StockPickingCheck")
            _ReissueCommand.RaiseCanExecuteChanged()

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub
#End Region

        Public Sub StockOpenCheck()
            If StockPickingCheck.Material Is Nothing OrElse StockPickingCheck.Stock Is Nothing OrElse ModifyStock Is Nothing Then
                Exit Sub
            End If

            ModifyStock.OpeningDate = DateTime.Today

            ' 原材料が開封時使用期限設定対象の場合、使用期限を再設定する
            If StockPickingCheck.Material.IsResetExtensionWhenOpen Then
                Dim exDate As New ExpirationDateModel
                exDate.OpenedDate = ModifyStock.OpeningDate
                If exDate.DefaultExpirationDate < StockPickingCheck.Stock.ExpirationDate Then
                    ModifyStock.ExpirationDate = exDate.DefaultExpirationDate
                End If
            End If

            RaisePropertyChanged("ModifyStock")
        End Sub

        Public Sub OpeningDateChanged()
            If StockPickingCheck.Material Is Nothing OrElse StockPickingCheck.Stock Is Nothing OrElse ModifyStock Is Nothing Then
                Exit Sub
            End If

            If StockPickingCheck.Material.IsResetExtensionWhenOpen Then
                Dim exDate As New ExpirationDateModel
                exDate.OpenedDate = ModifyStock.OpeningDate
                If exDate.DefaultExpirationDate < StockPickingCheck.Stock.ExpirationDate Then
                    ModifyStock.ExpirationDate = exDate.DefaultExpirationDate
                End If

                RaisePropertyChanged("ModifyStock")
            End If
        End Sub

    End Class
End Namespace