﻿Imports System.Linq
Imports System.Linq.Expressions
Imports System.Data.Entity
Imports DataModels
Imports AppCommon
Imports AppCommon.Models
Imports Livet.Commands
Imports TerminalTablet.Models

Namespace ViewModels
    Public Class ScheduleListViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        'DBコンテキスト。原材料マスタ等マスタ項目の遅延評価が出来るようにする為に保持したままにする。
        Private db As New LocalContext

        'Page遷移時にViewに送るイベント
        Public Event NavigationEvent As RoutedEventHandler

        Public Overloads Sub initialize()
            Condition = New ScheduleConditionModel(GetType(LocalContext))

            MyBase.Initialize(GetType(Schedule), ConditionLambda)

            'グリッドデータ初期表示
            MyBase.DataUpdate()
        End Sub

#Region "ArrivalCommand"
        Private _ArrivalCommand As ViewModelCommand

        Public ReadOnly Property ArrivalCommand() As ViewModelCommand
            Get
                If _ArrivalCommand Is Nothing Then
                    _ArrivalCommand = New ViewModelCommand(AddressOf Arrival)
                End If
                Return _ArrivalCommand
            End Get
        End Property

        Private Sub Arrival()
            If GridSelectedItem Is Nothing Then
                ErrorMessage = "発行予定情報を選んでください。"
                Return
            Else
                ErrorMessage = ""
            End If

            ' 選択している発行予定情報を設定して、発行画面に遷移する
            Dim o As New Views.ArrivalPage
            Dim vm As ArrivalViewModel = o.DataContext
            vm.SetData(GridSelectedItem)
            RaiseEvent NavigationEvent(o, New RoutedEventArgs)
        End Sub
#End Region

#Region "ArrivalUnscheduledCommand"
        Private _ArrivalUnscheduledCommand As ViewModelCommand

        Public ReadOnly Property ArrivalUnscheduledCommand() As ViewModelCommand
            Get
                If _ArrivalUnscheduledCommand Is Nothing Then
                    _ArrivalUnscheduledCommand = New ViewModelCommand(AddressOf ArrivalUnscheduled)
                End If
                Return _ArrivalUnscheduledCommand
            End Get
        End Property

        Private Sub ArrivalUnscheduled()
            Dim o As New Views.ArrivalPage
            Dim vm As ArrivalViewModel = o.DataContext
            vm.SetData()
            RaiseEvent NavigationEvent(o, New RoutedEventArgs)
        End Sub
#End Region

#Region "ErrorMessage変更通知プロパティ"
        Private _ErrorMessage As String

        Public Property ErrorMessage() As String
            Get
                Return _ErrorMessage
            End Get
            Set(ByVal value As String)
                If (_ErrorMessage = value) Then Return
                _ErrorMessage = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "Condition変更通知プロパティ"
        Private _Condition As ScheduleConditionModel

        Public Property Condition() As ScheduleConditionModel
            Get
                Return _Condition
            End Get
            Set(ByVal value As ScheduleConditionModel)
                If (_Condition Is value) Then Return
                _Condition = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ListSearchCommand"
        Private _ListSearchCommand As ViewModelCommand

        Public ReadOnly Property ListSearchCommand() As ViewModelCommand
            Get
                If _ListSearchCommand Is Nothing Then
                    _ListSearchCommand = New ViewModelCommand(AddressOf ListSearch, AddressOf CanListSearch)
                End If
                Return _ListSearchCommand
            End Get
        End Property

        Private Function CanListSearch() As Boolean
            Return True
        End Function

        Private Sub ListSearch()
            MyBase.Initialize(GetType(Schedule), ConditionLambda)

            'グリッドデータ初期表示
            MyBase.DataUpdate()
        End Sub

        ' 一覧検索クエリ
        Private Function ConditionLambda() As Func(Of IList)
            Return Function()
                       Dim q = From o In db.Schedules
                               Where o.Deplecated = 0 And o.CompleteFlag <> Schedule.CompletionType.Complete
                               Order By o.ScheduleDate, o.MasterMaterial.WarehouseCd, o.MasterMaterial.RackCd, o.MasterMaterial.MaterialCd
                               Select o

                       If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                           q = q.Where(Function(o) o.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
                       End If

                       If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                           q = q.Where(Function(o) o.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.Name))
                       End If

                       Dim paramExpr = Expression.Parameter(GetType(Schedule), "o")
                       Dim todayExpr = Expression.Constant(DateTime.Today)
                       Dim scheduleDateExpr = Expression.Property(Expression.Property(paramExpr, "ScheduleDate"),
                                           GetType(System.Nullable(Of DateTime)).GetProperty("Value"))

                       Dim bodyExpr As Expression = Nothing
                       If Condition.IsSearchDelayed Then
                           bodyExpr = Expression.LessThan(scheduleDateExpr, todayExpr)
                       End If
                       If Condition.IsSearchToday Then
                           bodyExpr = ExpOr(bodyExpr, Expression.Equal(scheduleDateExpr, todayExpr))
                       End If
                       If Condition.IsSearchFuture Then
                           bodyExpr = ExpOr(bodyExpr, Expression.GreaterThan(scheduleDateExpr, todayExpr))
                       End If

                       If bodyExpr IsNot Nothing Then
                           q = q.Where(Expression.Lambda(Of Func(Of Schedule, Boolean))(bodyExpr, paramExpr))
                       End If

                       q.Load()
                       Return q.ToList()
                   End Function
        End Function

        Private Function ExpOr(left As Expression, right As Expression) As Expression
            If left IsNot Nothing Then
                Return Expression.OrElse(left, right)
            Else
                Return right
            End If
        End Function
#End Region

    End Class
End Namespace