﻿Imports System.Linq
Imports System.Data.Entity
Imports AppCommon
Imports AppCommon.Models
Imports DataModels
Imports TerminalTablet.Models

Namespace ViewModels
    Public Class PickingViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Private db As New LocalContext

        '画面遷移時にViewに送るイベント
        Public Event NavigationEvent As RoutedEventHandler

        Public Sub Initialize()
            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub

#Region "Material変更通知プロパティ"
        Private _Material As MasterMaterial

        Public Property Material() As MasterMaterial
            Get
                Return _Material
            End Get
            Set(ByVal value As MasterMaterial)
                If (_Material Is value) Then Return
                _Material = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "StockListDataSource変更通知プロパティ"
        Private _StockListDataSource As IList(Of GridItem)

        Public Property StockListDataSource() As IList(Of GridItem)
            Get
                Return _StockListDataSource
            End Get
            Set(ByVal value As IList(Of GridItem))
                If (_StockListDataSource Is value) Then Return
                _StockListDataSource = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

        '在庫一覧データ取得
        Public Sub StockListSearch()
            If Material Is Nothing Then
                StockListDataSource = Nothing
                Exit Sub
            End If

            Dim q = db.Stocks.
                Where(Function(o) o.Deplecated = 0 AndAlso o.StockFlag = StockType.Stock AndAlso o.MasterMaterial_Id = Material.Id).
                GroupBy(
                    Function(o) New With {o.MasterMaterial_Id, o.LotNo},
                    Function(key, e) New GridItem With {
                        .Key = New KeyItem With {.MasterMaterial_Id = key.MasterMaterial_Id, .LotNo = key.LotNo},
                        .ExpirationDate = e.Min(Function(o) o.ExpirationDate),
                        .ExpirationDateNotopen = e.Where(Function(o) o.OpenedFlag = OpenedType.NotOpen).Min(Function(o) o.ExpirationDate),
                        .ExpirationDateOpen = e.Where(Function(o) o.OpenedFlag = OpenedType.Opened).Min(Function(o) o.ExpirationDate),
                        .Count = e.Count(),
                        .NotOpened = e.Count(Function(o) o.OpenedFlag = OpenedType.NotOpen),
                        .Opened = e.Count(Function(o) o.OpenedFlag = OpenedType.Opened)
                    }).OrderBy(Function(o) New With {
                                   .ExpirationDate = If(o.ExpirationDate, DateTime.MaxValue),
                                   o.Key.LotNo
                               })

            StockListDataSource = q.ToList()
        End Sub

#Region "StockSelectedItem変更通知プロパティ"
        Private _StockSelectedItem As Object

        Public Property StockSelectedItem() As Object
            Get
                Return _StockSelectedItem
            End Get
            Set(ByVal value As Object)
                If (_StockSelectedItem Is value) Then Return
                _StockSelectedItem = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "StockPickingCheck変更通知プロパティ"
        Private _StockPickingCheck As New Models.StockPickingCheckModel

        Public Property StockPickingCheck() As Models.StockPickingCheckModel
            Get
                Return _StockPickingCheck
            End Get
            Set(ByVal value As Models.StockPickingCheckModel)
                If (_StockPickingCheck Is value) Then Return
                _StockPickingCheck = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "QRinput変更通知プロパティ"
        Private _QRinput As String

        Public Property QRinput() As String
            Get
                Return _QRinput
            End Get
            Set(ByVal value As String)
                If (_QRinput = value) Then Return
                _QRinput = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "QREnterCommand"
        Private _QREnterCommand As ViewModelCommand

        Public ReadOnly Property QREnterCommand() As ViewModelCommand
            Get
                If _QREnterCommand Is Nothing Then
                    _QREnterCommand = New ViewModelCommand(AddressOf QREnter, AddressOf CanQREnter)
                End If
                Return _QREnterCommand
            End Get
        End Property

        Private Function CanQREnter() As Boolean
            Return True
        End Function

        Private Sub QREnter()
            If Trim(_QRinput) = String.Empty Then
                Exit Sub
            End If

            Dim qr As LabelQRModel = New LabelQRModel(_QRinput)
            AppCommon.Util.ShortBeep()

            CanPickingStatus = SelectStock(qr)
            _PickingCommand.RaiseCanExecuteChanged()

            QRinput = ""
        End Sub

#Region "CanPickingStatus変更通知プロパティ"
        Private _CanPickingStatus As Boolean = False

        Public Property CanPickingStatus() As Boolean
            Get
                Return _CanPickingStatus
            End Get
            Set(ByVal value As Boolean)
                If (_CanPickingStatus = value) Then Return
                _CanPickingStatus = value
                _PickingCommand.RaiseCanExecuteChanged()
            End Set
        End Property
#End Region

#End Region

#Region "GridSelectedIndex変更通知プロパティ"
        Private _GridSelectedIndex As Integer

        Public Property GridSelectedIndex() As Integer
            Get
                Return _GridSelectedIndex
            End Get
            Set(ByVal value As Integer)
                If (_GridSelectedIndex = value) Then Return
                _GridSelectedIndex = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "PickingCommand"
        Private _PickingCommand As ViewModelCommand

        Public ReadOnly Property PickingCommand() As ViewModelCommand
            Get
                If _PickingCommand Is Nothing Then
                    _PickingCommand = New ViewModelCommand(AddressOf Picking, AddressOf CanPicking)
                End If
                Return _PickingCommand
            End Get
        End Property

        Private Function CanPicking() As Boolean
            Return CanPickingStatus
        End Function

        Private Sub Picking()
            Dim picking As New Picking
            picking.PickingFlag = PickingType.TakingOut
            picking.Stock_Id = _StockPickingCheck.Stock.Id
            picking.UnderlyingStockRevision = _StockPickingCheck.Stock.Revision

            Dim lUid = LoginModel.GetLoginUser().Id
            picking.InsertUser = lUid
            picking.UpdateUser = lUid

            db.Pickings.Add(picking)
            db.SaveChanges()

            ' ピッキング一覧画面に遷移する
            RaiseEvent NavigationEvent(ListPage, New RoutedEventArgs)
        End Sub
#End Region

#Region "BackToListCommand"
        Private _BackToListCommand As ViewModelCommand

        Public ReadOnly Property BackToListCommand() As ViewModelCommand
            Get
                If _BackToListCommand Is Nothing Then
                    _BackToListCommand = New ViewModelCommand(AddressOf BackToList)
                End If
                Return _BackToListCommand
            End Get
        End Property

        Private Sub BackToList()

            RaiseEvent NavigationEvent(ListPage, New RoutedEventArgs)
        End Sub
#End Region
        Public Property ListPage As Views.PickingListPage

        Public Function SelectStock(qr As LabelQRModel) As Boolean
            If _StockPickingCheck.CheckStock(qr) Then

                ' 原材料が初期表示と変わっていれば表示を更新する
                If Material Is Nothing OrElse _StockPickingCheck.Material.Id <> Material.Id Then
                    Material = _StockPickingCheck.Material
                    StockListSearch()
                End If

                ' 在庫一覧で、読み取ったQRの在庫行を選択状態にする
                SelectStockInList(qr.LotNo)

                Return True
            Else
                GridSelectedIndex = -1

                Return False
            End If
        End Function

        ' 在庫一覧で、指定したロット番号の在庫行を選択状態にする
        Private Sub SelectStockInList(lotno As String)
            For i = 0 To _StockListDataSource.Count - 1
                If _StockListDataSource(i).Key.LotNo = lotno Then
                    GridSelectedIndex = i
                End If
            Next
        End Sub

        Public Class GridItem
            Public Property Key As KeyItem
            Public Property ExpirationDate As System.Nullable(Of DateTime)
            Public Property ExpirationDateNotopen As System.Nullable(Of DateTime)
            Public Property ExpirationDateOpen As System.Nullable(Of DateTime)
            Public Property Count As Integer
            Public Property NotOpened As Integer
            Public Property Opened As Integer
        End Class

        Public Class KeyItem
            Public Property MasterMaterial_Id As Integer
            Public Property LotNo As String
        End Class

    End Class
End Namespace