﻿Imports Livet
Imports Livet.Commands
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Net.Http.Formatting
Imports System.Data.Entity.Validation
Imports DataModels
Imports AppCommon
Imports AppCommon.Models
Imports System.ComponentModel.DataAnnotations

Namespace ViewModels
    Public Class ArrivalViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        'Page遷移時にViewに送るイベント
        Public Event NavigationEvent As RoutedEventHandler

        Public Sub Initialize()

            ' 予定外発行で新規原材料が入力されたとき、その原材料を表示する。
            If IsNotScheduled AndAlso NewMaterial IsNot Nothing Then
                DataItem.MasterMaterial = NewMaterial
                IsNotScheduled = False
            End If
        End Sub

        ' 予定なし受入の初期データ設定
        Public Sub SetData()
            DataItem = New Arrival
            IsNotScheduled = True
            ConfirmationVisibility = Visibility.Collapsed
        End Sub

        ' 予定あり受入の初期データ設定
        Public Sub SetData(ByVal schedule As DataModels.Schedule)
            Me.SetData()

            '発行情報データを設定する
            DataItem.Schedule = schedule.ShallowCopy(New Schedule)
            DataItem.NumArrived = schedule.NumSchedule - schedule.NumArrival
            If schedule.MasterMaterial IsNot Nothing AndAlso schedule.MasterMaterial.UnitNum > 0 Then
                Dim numLabel As Integer = DataItem.NumArrived / schedule.MasterMaterial.UnitNum
                DataItem.NumLabel = If(numLabel > 0, numLabel, 1)
            Else
                DataItem.NumLabel = 1
            End If

            '原材料マスタ表示データを設定する
            If schedule.MasterMaterial IsNot Nothing Then
                DataItem.Schedule.MasterMaterial = schedule.MasterMaterial
            End If

            IsNotScheduled = False
            ConfirmationVisibility = Visibility.Visible
            RaisePropertyChanged("DataItem")
        End Sub

#Region "DataItem変更通知プロパティ"
        Private _DataItem As Arrival

        Public Property DataItem() As Arrival
            Get
                Return _DataItem
            End Get
            Set(ByVal value As Arrival)
                If (_DataItem Is value) Then Return
                _DataItem = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "IsNotScheduled変更通知プロパティ"
        Private _IsNotScheduled As Boolean

        Public Property IsNotScheduled() As Boolean
            Get
                Return _IsNotScheduled
            End Get
            Set(ByVal value As Boolean)
                If (_IsNotScheduled = value) Then Return
                _IsNotScheduled = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "Message変更通知プロパティ"
        Private _Message As String

        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                If (_Message = value) Then Return
                _Message = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "NewMaterial変更通知プロパティ"
        Private _NewMaterial As MasterMaterial

        Public Property NewMaterial() As MasterMaterial
            Get
                Return _NewMaterial
            End Get
            Set(ByVal value As MasterMaterial)
                If (_NewMaterial Is value) Then Return
                _NewMaterial = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

        '原材料絞り込み選択
        Public Property MaterialSelection As New MaterialSelectionModel

#Region "ArriveCommand"
        Private _ArriveCommand As ViewModelCommand

        Public ReadOnly Property ArriveCommand() As ViewModelCommand
            Get
                If _ArriveCommand Is Nothing Then
                    _ArriveCommand = New ViewModelCommand(AddressOf Arrive, AddressOf CanArrive)
                End If
                Return _ArriveCommand
            End Get
        End Property

        Private Function CanArrive() As Boolean
            Return ConfirmationVisibility <> Visibility.Visible
        End Function

        Private Sub Arrive()
            ' ラベル即時発行指定で受入処理実行
            DataItem.IsDelayedPrint = 0
            ArriveAction()
        End Sub
#End Region

#Region "ArriveDelayedPrintCommand"
        Private _ArriveDelayedPrintCommand As ViewModelCommand

        Public ReadOnly Property ArriveDelayedPrintCommand() As ViewModelCommand
            Get
                If _ArriveDelayedPrintCommand Is Nothing Then
                    _ArriveDelayedPrintCommand = New ViewModelCommand(AddressOf ArriveDelayedPrint, AddressOf CanArriveDelayedPrint)
                End If
                Return _ArriveDelayedPrintCommand
            End Get
        End Property

        Private Function CanArriveDelayedPrint() As Boolean
            Return ConfirmationVisibility <> Visibility.Visible
        End Function

        Private Sub ArriveDelayedPrint()
            ' ラベル遅延発行指定で受入処理実行
            DataItem.IsDelayedPrint = 1
            ArriveAction()
        End Sub
#End Region

#Region "MakeNewMaterialCommand"
        Private _MakeNewMaterialCommand As ViewModelCommand

        Public ReadOnly Property MakeNewMaterialCommand() As ViewModelCommand
            Get
                If _MakeNewMaterialCommand Is Nothing Then
                    _MakeNewMaterialCommand = New ViewModelCommand(AddressOf MakeNewMaterial, AddressOf CanMakeNewMaterial)
                End If
                Return _MakeNewMaterialCommand
            End Get
        End Property

        Private Function CanMakeNewMaterial() As Boolean
            Return ConfirmationVisibility <> Visibility.Visible
        End Function

        Private Sub MakeNewMaterial()
            NewMaterial = Nothing

            Dim o As New Views.NewMaterialPage
            RaiseEvent NavigationEvent(o, New RoutedEventArgs)
        End Sub
#End Region

        Private Sub ArriveAction()
            Message = ""
            If DataItem.MasterMaterial Is Nothing Then
                Message = "原材料を選択、もしくは新規に入力して下さい。"
                Exit Sub
            End If

            ' 原材料を新規追加する場合、発行データに新規入力した原材料を設定
            If NewMaterial IsNot Nothing Then
                DataItem.NewMaterial = NewMaterial
            End If

            ' ユーザー情報を設定
            Dim lUid = LoginModel.GetLoginUser().Id
            DataItem.InsertUser = lUid
            DataItem.UpdateUser = lUid

            'エラーチェック(リレーションのエンティティに依存するもの)
            For Each e In DataItem.Validate(New ValidationContext(DataItem))
                Message += e.ErrorMessage & vbCrLf
            Next

            ' FK項目のオブジェクトをNull設定して、新規オブジェクト作成されるのを防ぐ
            Object2Id(DataItem)

            Using db As New LocalContext
                '原材料を新規追加したとき、
                If DataItem.NewMaterial IsNot Nothing AndAlso DataItem.MasterMaterial Is Nothing Then
                    DataItem.MasterMaterial_Id = Nothing
                End If

                ' エラーチェック(ModelのAnnotationによるもの)
                db.Arrivals.Add(DataItem)
                Dim errors As IEnumerable = db.GetValidationErrors()
                Dim validationMsg As String = ""
                For Each e As DbEntityValidationResult In errors
                    For Each ee As DbValidationError In e.ValidationErrors
                        validationMsg = validationMsg & ee.ErrorMessage & vbCrLf
                    Next
                Next
                Message += validationMsg

                ' エラーがなければ発行API処理を呼び出し
                If Message = "" Then

                    Dim m = SyncArrival.PushArrival(DataItem)
                    If m = "" Then
                        DataItem.Status = LocalModelBase.StatusType.Synced
                    Else
                        DataItem.Status = LocalModelBase.StatusType.Errored
                        Message = "ラベル発行データのサーバ登録が失敗しました : " & vbCrLf & m

                        Id2Object(DataItem, db)
                        RaisePropertyChanged("DataItem")
                        Exit Sub
                    End If

                    db.SaveChanges()

                    Dim o As New Views.ScheduleListPage
                    RaiseEvent NavigationEvent(o, New RoutedEventArgs)
                Else
                    Id2Object(DataItem, db)
                    RaisePropertyChanged("DataItem")
                End If
            End Using
        End Sub

        ' FK項目のオブジェクトをNull設定して、新規オブジェクト作成されるのを防ぐ
        Private Sub Object2Id(o As Arrival)

            If o.MasterMaterial IsNot Nothing Then
                o.MasterMaterial_Id = o.MasterMaterial.Id
                o.MasterMaterial = Nothing
            End If
            If o.Schedule IsNot Nothing Then
                o.Schedule_Id = o.Schedule.Id
                o.Schedule = Nothing
            End If
            If o.NewMaterial IsNot Nothing AndAlso o.NewMaterial.MasterSupplier IsNot Nothing Then
                o.NewMaterial.MasterSupplier_Id = o.NewMaterial.MasterSupplier.Id
                o.NewMaterial.MasterSupplier = Nothing
            End If
        End Sub

        ' FK項目のオブジェクトをNull設定したものを元に戻す
        Private Sub Id2Object(o As Arrival, db As LocalContext)
            If o.MasterMaterial_Id IsNot Nothing AndAlso o.NewMaterial Is Nothing Then
                o.MasterMaterial = db.Materials.Find(o.MasterMaterial_Id)
                o.MasterMaterial_Id = Nothing
            ElseIf o.NewMaterial IsNot Nothing Then
                o.MasterMaterial = o.NewMaterial
            End If

            If o.Schedule_Id IsNot Nothing Then
                o.Schedule = db.Schedules.Find(o.Schedule_Id)
                o.Schedule_Id = Nothing
            End If
        End Sub

#Region "ConfirmationVisibility変更通知プロパティ"
        Private _ConfirmationVisibility As Visibility

        Public Property ConfirmationVisibility() As Visibility
            Get
                Return _ConfirmationVisibility
            End Get
            Set(ByVal value As Visibility)
                If (_ConfirmationVisibility = value) Then Return
                _ConfirmationVisibility = value
                RaisePropertyChanged()
                _ArriveCommand.RaiseCanExecuteChanged()
                _ArriveDelayedPrintCommand.RaiseCanExecuteChanged()
                _MakeNewMaterialCommand.RaiseCanExecuteChanged()
            End Set
        End Property
#End Region

#Region "ConfirmOkCommand"
        Private _ConfirmOkCommand As ViewModelCommand

        Public ReadOnly Property ConfirmOkCommand() As ViewModelCommand
            Get
                If _ConfirmOkCommand Is Nothing Then
                    _ConfirmOkCommand = New ViewModelCommand(AddressOf ConfirmOk)
                End If
                Return _ConfirmOkCommand
            End Get
        End Property

        Private Sub ConfirmOk()
            ConfirmationVisibility = Visibility.Collapsed
        End Sub
#End Region

#Region "ConfirmCancelCommand"
        Private _ConfirmCancelCommand As ViewModelCommand

        Public ReadOnly Property ConfirmCancelCommand() As ViewModelCommand
            Get
                If _ConfirmCancelCommand Is Nothing Then
                    _ConfirmCancelCommand = New ViewModelCommand(AddressOf ConfirmCancel)
                End If
                Return _ConfirmCancelCommand
            End Get
        End Property

        Private Sub ConfirmCancel()
            Dim o As New Views.ScheduleListPage
            RaiseEvent NavigationEvent(o, New RoutedEventArgs)
        End Sub
#End Region


    End Class
End Namespace