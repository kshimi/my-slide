﻿Imports AppCommon
Imports AppCommon.Models
Imports DataModels

Namespace ViewModels
    Public Class LoginViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        '画面遷移時にViewに送るイベント
        Public Event NavigationEvent As RoutedEventHandler

        Public Sub Initialize()
            Login = New LoginModel(GetType(LocalContext))

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub

#Region "Login変更通知プロパティ"
        Private _Login As LoginModel

        Public Property Login() As LoginModel
            Get
                Return _Login
            End Get
            Set(ByVal value As LoginModel)
                If (_Login Is value) Then Return
                _Login = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "LoginCheckCommand"
        Private _LoginCheckCommand As ViewModelCommand

        Public ReadOnly Property LoginCheckCommand() As ViewModelCommand
            Get
                If _LoginCheckCommand Is Nothing Then
                    _LoginCheckCommand = New ViewModelCommand(AddressOf LoginCheck, AddressOf CanLoginCheck)
                End If
                Return _LoginCheckCommand
            End Get
        End Property

        Private Function CanLoginCheck() As Boolean
            Return True
        End Function

        Private Sub LoginCheck()
            If Login.CheckLogin() Then

                ' ログインしたユーザーをアプリケーション共有領域に設定する
                LoginModel.SetLoginUser(Login.LoginUser)
                
                'メニュー画面に遷移する
                Dim o As New Views.MenuPage
                RaiseEvent NavigationEvent(o, New RoutedEventArgs)
            End If
        End Sub
#End Region

    End Class
End Namespace
