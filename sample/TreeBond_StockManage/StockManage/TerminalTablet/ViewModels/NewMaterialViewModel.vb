﻿Imports DataModels
Imports Livet.Commands

Namespace ViewModels
    Public Class NewMaterialViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        'Page遷移時にViewに送るイベント
        Public Event NavigationEvent As RoutedEventHandler

        Public Sub Initialize()
            Using db As New LocalContext
                Dim sq = From s In db.Suppliers
                         Order By s.Code
                         Select s

                SupplierItems = sq.ToList()
            End Using

            DataItem = New MasterMaterial
        End Sub

        Public Property ArrivalPage As Views.ArrivalPage

#Region "DataItem変更通知プロパティ"
        Private _DataItem As MasterMaterial

        Public Property DataItem() As MasterMaterial
            Get
                Return _DataItem
            End Get
            Set(ByVal value As MasterMaterial)
                If (_DataItem Is value) Then Return
                _DataItem = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "SupplierItems変更通知プロパティ"
        Private _SupplierItems As IList

        Public Property SupplierItems() As IList
            Get
                Return _SupplierItems
            End Get
            Set(ByVal value As IList)
                If (_SupplierItems Is value) Then Return
                _SupplierItems = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "RegistCommand"
        Private _RegistCommand As ViewModelCommand

        Public ReadOnly Property RegistCommand() As ViewModelCommand
            Get
                If _RegistCommand Is Nothing Then
                    _RegistCommand = New ViewModelCommand(AddressOf Regist, AddressOf CanRegist)
                End If
                Return _RegistCommand
            End Get
        End Property

        Private Function CanRegist() As Boolean
            Return True
        End Function

        Private Sub Regist()
            If Not DataItem.IsValid() Then
                Exit Sub
            End If

            Dim vm = ArrivalPage.DataContext
            CType(vm, ArrivalViewModel).NewMaterial = DataItem

            RaiseEvent NavigationEvent(ArrivalPage, New RoutedEventArgs)
        End Sub
#End Region

#Region "BackCommand"
        Private _BackCommand As ViewModelCommand

        Public ReadOnly Property BackCommand() As ViewModelCommand
            Get
                If _BackCommand Is Nothing Then
                    _BackCommand = New ViewModelCommand(AddressOf Back)
                End If
                Return _BackCommand
            End Get
        End Property

        Private Sub Back()
            RaiseEvent NavigationEvent(ArrivalPage, New RoutedEventArgs)
        End Sub
#End Region

    End Class
End Namespace