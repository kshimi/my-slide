﻿Imports System.Threading.Tasks
Imports System.ComponentModel

Namespace ViewModels
    Public Class MainViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Public Sub Initialize()
            _backgroundWorker = New BackgroundWorker()
            _backgroundWorker.RunWorkerAsync()

            DataCancelVisibility = Visibility.Collapsed
        End Sub

#Region "SyncMessage変更通知プロパティ"
        Private _SyncMessage As String

        Public Property SyncMessage() As String
            Get
                Return _SyncMessage
            End Get
            Set(ByVal value As String)
                If (_SyncMessage = value) Then Return
                _SyncMessage = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ErrorMessage変更通知プロパティ"
        Private _ErrorMessage As String

        Public Property ErrorMessage() As String
            Get
                Return _ErrorMessage
            End Get
            Set(ByVal value As String)
                If (_ErrorMessage = value) Then Return
                _ErrorMessage = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

        Private WithEvents _backgroundWorker As BackgroundWorker

        Private Sub OnDoWork(sender As Object, e As DoWorkEventArgs) Handles _backgroundWorker.DoWork
            Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
            ErrorMessage = ""

            '未連携データの確認
            Dim waitingDataMsg = WaitingSendDataModel.WaitingDataStr

            If System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() Then
                SyncMessage = waitingDataMsg & "データ連携中"

                '送信データ連携
                ErrorMessage += SyncPicking.SyncPickings()
                ErrorMessage += SyncReissue.SyncReissue()
                '' ErrorMessage += SyncArrival.SyncArrivals()

                'マスタデータ連携
                ErrorMessage += SyncMaster.SyncMasters()

                waitingDataMsg = WaitingSendDataModel.WaitingDataStr
                SyncMessage = waitingDataMsg & "データ連携完了"
            Else
                SyncMessage = waitingDataMsg & "ネットワークが接続できません"
            End If

            System.Threading.Thread.Sleep(1000 * 30)
        End Sub

        Private Sub RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles _backgroundWorker.RunWorkerCompleted
            ' タイマー処理完了後、再度実行する
            CType(sender, BackgroundWorker).RunWorkerAsync()
        End Sub

#Region "同期待ちデータキャンセル"

#Region "DataCancelVisibility変更通知プロパティ"
        Private _DataCancelVisibility As System.Windows.Visibility

        Public Property DataCancelVisibility() As System.Windows.Visibility
            Get
                Return _DataCancelVisibility
            End Get
            Set(ByVal value As System.Windows.Visibility)
                If (_DataCancelVisibility = value) Then Return
                _DataCancelVisibility = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

        ' 同期待ちデータキャンセルボタンを表示する
        Public Sub CancelRemainVisible()
            DataCancelVisibility = Visibility.Visible
        End Sub

#Region "RemainSyncDataCancelCommand"
        Private _RemainSyncDataCancelCommand As ViewModelCommand

        Public ReadOnly Property RemainSyncDataCancelCommand() As ViewModelCommand
            Get
                If _RemainSyncDataCancelCommand Is Nothing Then
                    _RemainSyncDataCancelCommand = New ViewModelCommand(AddressOf RemainSyncDataCancel, AddressOf CanRemainSyncDataCancel)
                End If
                Return _RemainSyncDataCancelCommand
            End Get
        End Property

        Private Function CanRemainSyncDataCancel() As Boolean
            Return True
        End Function

        Private Sub RemainSyncDataCancel()
            Dim dlgMesg = "同期されていないピッキングデータ、受け取りデータを全てキャンセルします。" & vbCrLf &
                "本当によろしいですか？"

            If MsgBox(dlgMesg, MsgBoxStyle.YesNo + MsgBoxStyle.Critical) = MsgBoxResult.Yes Then
                ErrorMessage = ""

                ErrorMessage += SyncPicking.CancelPickings()
                ErrorMessage += SyncArrival.CancelArrivals()
                ErrorMessage += SyncReissue.CancelReissue()

                Dim waitingDataMsg = WaitingSendDataModel.WaitingDataStr
                SyncMessage = waitingDataMsg & If(ErrorMessage Is Nothing, "データキャンセル完了", "")
            End If

            DataCancelVisibility = Visibility.Collapsed
        End Sub
#End Region

#End Region
    End Class
End Namespace
