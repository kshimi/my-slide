﻿Imports System.Linq
Imports System.Linq.Expressions
Imports System.Reflection
Imports System.Data.Entity
Imports TerminalTablet.Models
Imports DataModels
Imports AppCommon
Imports AppCommon.Models

Namespace ViewModels
    Public Class PickingListViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)
  
        'DBコンテキスト。原材料マスタ等マスタ項目の遅延評価が出来るようにする為に保持したままにする。
        Private db As New LocalContext

        'PickingPage遷移時にViewに送るイベント
        Public Event NavigationEvent As RoutedEventHandler

        Public Sub Initialize()
            Condition = New StockConditionModel(GetType(LocalContext))

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub

#Region "QRinput変更通知プロパティ"
        Private _QRinput As String

        Public Property QRinput() As String
            Get
                Return _QRinput
            End Get
            Set(ByVal value As String)
                If (_QRinput = value) Then Return
                _QRinput = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ErrorMessage変更通知プロパティ"
        Private _ErrorMessage As String

        Public Property ErrorMessage() As String
            Get
                Return _ErrorMessage
            End Get
            Set(ByVal value As String)
                If (_ErrorMessage = value) Then Return
                _ErrorMessage = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "PickingCommand"
        Private _PickingCommand As ViewModelCommand

        Public ReadOnly Property PickingCommand() As ViewModelCommand
            Get
                If _PickingCommand Is Nothing Then
                    _PickingCommand = New ViewModelCommand(AddressOf Picking, AddressOf CanPicking)
                End If
                Return _PickingCommand
            End Get
        End Property

        Private Function CanPicking() As Boolean
            Return True
        End Function

        Private Sub Picking()
            If Trim(_QRinput) = String.Empty Then
                Exit Sub
            End If

            Dim qr As LabelQRModel = New LabelQRModel(_QRinput)
            AppCommon.Util.ShortBeep()

            Dim o As New Views.PickingPage
            Dim vm As PickingViewModel = o.DataContext

            QRinput = ""

            ' 読み取ったQRコードから原材料、在庫を検索し、ピッキング画面に設定する
            If vm.SelectStock(qr) Then
                vm.CanPickingStatus = True
                ErrorMessage = ""

                ' ピッキング画面に遷移する
                RaiseEvent NavigationEvent(o, New RoutedEventArgs)
            Else
                ' QR 読み取り時のメッセージを取得し、自画面のメッセージ欄に設定する
                ErrorMessage = vm.StockPickingCheck.Message
            End If
        End Sub
#End Region

#Region "Condition変更通知プロパティ"
        Private _Condition As StockConditionModel

        Public Property Condition() As StockConditionModel
            Get
                Return _Condition
            End Get
            Set(ByVal value As StockConditionModel)
                If (_Condition Is value) Then Return
                _Condition = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ListDataSource変更通知プロパティ"
        Private _ListDataSource As IList

        Public Property ListDataSource() As IList
            Get
                Return _ListDataSource
            End Get
            Set(ByVal value As IList)
                If (_ListDataSource Is value) Then Return
                _ListDataSource = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ListSearchCommand"
        Private _ListSearchCommand As ViewModelCommand

        Public ReadOnly Property ListSearchCommand() As ViewModelCommand
            Get
                If _ListSearchCommand Is Nothing Then
                    _ListSearchCommand = New ViewModelCommand(AddressOf ListSearch, AddressOf CanListSearch)
                End If
                Return _ListSearchCommand
            End Get
        End Property

        Private Function CanListSearch() As Boolean
            Return True
        End Function

        Private Sub ListSearch()
            Dim q = db.Stocks.Where(Function(o) o.Deplecated = 0 AndAlso o.MasterMaterial IsNot Nothing AndAlso o.MasterMaterial.Deplecated = 0 AndAlso o.StockFlag = StockType.Stock AndAlso o.LotNo IsNot Nothing)

            If _Condition.MasterWarehouse IsNot Nothing Then
                q = q.Where(Function(o) o.MasterMaterial.WarehouseCd = _Condition.MasterWarehouse)
            End If

            If _Condition.MasterRack IsNot Nothing Then
                q = q.Where(Function(o) o.MasterMaterial.RackCd = _Condition.MasterRack)
            End If

            If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                q = q.Where(Function(o) o.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
            End If

            If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                q = q.Where(Function(o) o.MasterMaterial.Name.Contains(_Condition.MasterMaterial.Name))
            End If

            Dim groupedQ = From o In q
                           Group o By o.MasterMaterial.WarehouseCd, o.MasterMaterial.RackCd, o.MasterMaterial.MaterialCd, o.MasterMaterial.Name, o.LotNo Into Group
                           Order By WarehouseCd, RackCd, MaterialCd, Group.OrderBy(Function(o) o.ExpirationDate)
                            Select New With {
                                WarehouseCd,
                                RackCd,
                                MaterialCd,
                                Name,
                                LotNo,
                                .NumLot = Group.Count(),
                                .ExpirationDate = Group.Min(Function(it) it.ExpirationDate),
                                .ExpirationDateNotopen = Group.Where(Function(it) it.OpenedFlag = OpenedType.NotOpen).Min(Function(it) it.ExpirationDate),
                                .ExpirationDateOpened = Group.Where(Function(it) it.OpenedFlag = OpenedType.Opened).Min(Function(it) it.ExpirationDate),
                                .NotopenCount = Group.Count(Function(it) it.OpenedFlag = OpenedType.NotOpen),
                                .OpenedCount = Group.Count(Function(it) it.OpenedFlag = OpenedType.Opened)
                            }

            ListDataSource = groupedQ.OrderBy(Function(o) New With {
                                                  o.WarehouseCd,
                                                  o.RackCd,
                                                  o.MaterialCd,
                                                  .ExpirationDate = If(o.ExpirationDate, DateTime.MaxValue),
                                                  o.LotNo
                                              }).ToList()

            Messenger.Raise(New InteractionMessage("resetFocus"))
        End Sub
#End Region

#Region "SelectedItem変更通知プロパティ"
        Private _SelectedItem As Object

        Public Property SelectedItem() As Object
            Get
                Return _SelectedItem
            End Get
            Set(ByVal value As Object)
                If (_SelectedItem Is value) Then Return
                _SelectedItem = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "PickingWithListItemCommand"
        Private _PickingWithListItemCommand As ViewModelCommand

        Public ReadOnly Property PickingWithListItemCommand() As ViewModelCommand
            Get
                If _PickingWithListItemCommand Is Nothing Then
                    _PickingWithListItemCommand = New ViewModelCommand(AddressOf PickingWithListItem, AddressOf CanPickingWithListItem)
                End If
                Return _PickingWithListItemCommand
            End Get
        End Property

        Private Function CanPickingWithListItem() As Boolean
            Return True
        End Function

        Private Sub PickingWithListItem()
            If _SelectedItem Is Nothing Then
                Exit Sub
            End If

            ' ピッキング画面に遷移する
            Dim o As New Views.PickingPage
            Dim vm As PickingViewModel = o.DataContext

            Dim matCd As String = _SelectedItem.MaterialCd
            Dim mat = db.Materials.Where(Function(m) m.MaterialCd = matCd)
            If mat.Any Then
                vm.Material = mat.First
                vm.StockListSearch()
            End If

            RaiseEvent NavigationEvent(o, New RoutedEventArgs)
        End Sub
#End Region

    End Class
End Namespace