﻿Imports DataModels
Imports AppCommon

Class Application
    Implements IAppContext

    ' Startup、Exit、DispatcherUnhandledException などのアプリケーション レベルのイベントは、
    ' このファイルで処理できます。

    Private Sub Application_Startup(sender As Object, e As System.Windows.StartupEventArgs) Handles Me.Startup
        DispatcherHelper.UIDispatcher = Me.Dispatcher
    End Sub

    '集約エラーハンドラ
    Private Sub Application_DispatcherUnhandledException(sender As Object, e As System.Windows.Threading.DispatcherUnhandledExceptionEventArgs) Handles Me.DispatcherUnhandledException
        Dim exMessage As String = ": " & e.Exception.Message
        If e.Exception.InnerException IsNot Nothing Then
            exMessage += vbCrLf & ":: " & e.Exception.InnerException.Message

            If e.Exception.InnerException.InnerException IsNot Nothing Then
                exMessage += vbCrLf & "::: " & e.Exception.InnerException.InnerException.Message

                If e.Exception.InnerException.InnerException.InnerException IsNot Nothing Then
                    exMessage += vbCrLf & ":::: " & e.Exception.InnerException.InnerException.InnerException.Message
                End If
            End If
        End If

        MessageBox.Show("不明なエラーが発生しました。アプリケーションを終了します。" +
                        exMessage,
                        "エラー",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error)

        Environment.Exit(1)
    End Sub

    Public Property LoginUser As MasterUser Implements IAppContext.LoginUser
End Class
