﻿Imports Livet.EventListeners.WeakEvents
Imports AppCommon
Imports TerminalTablet.ViewModels

Namespace Views
    ' ViewModelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedWeakEventListenerや
    ' CollectionChangedWeakEventListenerを使うと便利です。独自イベントの場合はLivetWeakEventListenerが使用できます。
    ' クローズ時などに、LivetCompositeDisposableに格納した各種イベントリスナをDisposeする事でイベントハンドラの開放が容易に行えます。
    '
    ' WeakEventListenerなので明示的に開放せずともメモリリークは起こしませんが、できる限り明示的に開放するようにしましょう。
    '
    Class ArrivalPage
        Private _listenerList As New List(Of IDisposable)

        Private Sub Page_Loaded(sender As Object, e As RoutedEventArgs)

            ' ナビゲーションサービスのイベントリスナを登録
            Dim viewModel = DirectCast(DataContext, ViewModels.ArrivalViewModel)
            If viewModel IsNot Nothing Then

                Dim listener = New LivetWeakEventListener(Of EventHandler, EventArgs)(
                               Function(h) New EventHandler(Function() h),
                               Sub() AddHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               Sub() RemoveHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               AddressOf NavigatePage)

                _listenerList.Add(listener)
            End If
        End Sub

        Private Sub NavigatePage(sender As Object, e As RoutedEventArgs)
            If TypeOf sender Is NewMaterialPage Then
                Dim vm As ViewModels.NewMaterialViewModel = CType(sender, Views.NewMaterialPage).DataContext
                vm.ArrivalPage = Me
            End If

            NavigationService.Navigate(sender)
        End Sub

        Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
            Dim page As New Uri("Views/ScheduleListPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub TxtNumArrived_TextChanged(sender As Object, e As TextChangedEventArgs) Handles TxtNumArrived.TextChanged
            Dim v = Me.TxtNumArrived.Text
            If Not IsNumeric(v) Then
                Exit Sub
            End If
            Dim nv = Double.Parse(v)

            Dim vm = DirectCast(Me.DataContext, ArrivalViewModel)
            If vm IsNot Nothing AndAlso vm.DataItem IsNot Nothing AndAlso vm.DataItem.MasterMaterial IsNot Nothing AndAlso vm.DataItem.MasterMaterial.UnitNum IsNot Nothing Then
                Dim unitNum = vm.DataItem.MasterMaterial.UnitNum.Value

                Dim numLabel = Math.Ceiling(nv / unitNum)
                Me.TxtNumLabel.Text = numLabel
            End If
        End Sub
    End Class
End Namespace
