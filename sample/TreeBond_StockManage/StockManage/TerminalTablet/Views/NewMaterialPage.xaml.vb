﻿Imports Livet.EventListeners.WeakEvents
Imports AppCommon

Namespace Views
    Class NewMaterialPage
        Private _listenerList As New List(Of IDisposable)

        Private Sub Page_Loaded(sender As Object, e As RoutedEventArgs)

            ' ナビゲーションサービスのイベントリスナを登録
            Dim viewModel = DirectCast(DataContext, ViewModels.NewMaterialViewModel)
            If viewModel IsNot Nothing Then

                Dim listener = New LivetWeakEventListener(Of EventHandler, EventArgs)(
                               Function(h) New EventHandler(Function() h),
                               Sub() AddHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               Sub() RemoveHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               AddressOf NavigatePage)

                _listenerList.Add(listener)
            End If
        End Sub

        Private Sub NavigatePage(sender As Object, e As RoutedEventArgs)
            NavigationService.Navigate(sender)
        End Sub
    End Class
End Namespace
