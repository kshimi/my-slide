﻿Imports Livet.EventListeners.WeakEvents

Namespace Views
    ' ViewModelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedWeakEventListenerや
    ' CollectionChangedWeakEventListenerを使うと便利です。独自イベントの場合はLivetWeakEventListenerが使用できます。
    ' クローズ時などに、LivetCompositeDisposableに格納した各種イベントリスナをDisposeする事でイベントハンドラの開放が容易に行えます。
    '
    ' WeakEventListenerなので明示的に開放せずともメモリリークは起こしませんが、できる限り明示的に開放するようにしましょう。
    '
    Class ScheduleListPage

        Private _listenerList As New List(Of IDisposable)

        Private Sub Page_Loaded(sender As Object, e As RoutedEventArgs)

            ' ナビゲーションサービスのイベントリスナを登録
            Dim viewModel = DirectCast(DataContext, ViewModels.ScheduleListViewModel)
            If viewModel IsNot Nothing Then

                Dim listener = New LivetWeakEventListener(Of EventHandler, EventArgs)(
                               Function(h) New EventHandler(Function() h),
                               Sub() AddHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               Sub() RemoveHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               AddressOf NavigatePage)

                _listenerList.Add(listener)
            End If
        End Sub

        Private Sub NavigatePage(sender As Object, e As RoutedEventArgs)
            NavigationService.Navigate(sender)
        End Sub

        Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
            Dim page As New Uri("Views/MenuPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub
    End Class
End Namespace