﻿Imports Livet.EventListeners.WeakEvents

Namespace Views
    Class PickingListPage

        Private _listenerList As New List(Of IDisposable)

        Private Sub Page_Loaded(sender As Object, e As RoutedEventArgs)

            ' ナビゲーションサービスのイベントリスナを登録（ピッキング画面への画面遷移）
            Dim viewModel = DirectCast(DataContext, ViewModels.PickingListViewModel)
            If viewModel IsNot Nothing Then

                Dim listener = New LivetWeakEventListener(Of EventHandler, EventArgs)(
                               Function(h) New EventHandler(Function() h),
                               Sub() AddHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               Sub() RemoveHandler viewModel.NavigationEvent, AddressOf NavigatePage,
                               AddressOf NavigatePage)

                _listenerList.Add(listener)
            End If
        End Sub

        Private Sub NavigatePage(sender As Object, e As RoutedEventArgs)
            '遷移先のピッキングページに戻り先として一覧画面のオブジェクトを設定する
            Dim vm As ViewModels.PickingViewModel = CType(sender, Views.PickingPage).DataContext
            vm.ListPage = Me

            NavigationService.Navigate(sender)
        End Sub

        Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
            Dim page As New Uri("Views/MenuPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

    End Class
End Namespace