﻿Imports System.Linq
Imports System.Data.Entity
Imports AppCommon
Imports AppCommon.Models
Imports DataModels

Namespace Models
    Public Class StockPickingReturnCheckModel
        Inherits StockPickingCheckBaseModel

        Protected Overrides Function CheckStock(material As MasterMaterial, lotno As String, lotbranchno As String) As Boolean
            Using db As New LocalContext

                ' 在庫を検索して持ち出し状態をチェックする
                Dim lot = db.Stocks.Where(Function(o) o.Deplecated = 0 And
                              o.MasterMaterial_Id = material.Id And
                              o.LotBranchNo = lotbranchno)

                If lotno IsNot Nothing AndAlso lotno.Length > 0 Then
                    lot = lot.Where(Function(o) o.LotNo = lotno)
                Else
                    lot = lot.Where(Function(o) o.LotNo Is Nothing)
                End If

                ' ロットの在庫チェック
                If Not lot.Any() Then
                    Message = "ロットの在庫データがありません。"
                    Stock = Nothing
                    RaisePropertyChanged("Message")
                    RaisePropertyChanged("Stock")
                    Return False
                End If
                Stock = lot.First

                ' 該当ロットの持ち出し状態チェック
                If Stock.StockFlag = StockType.Stock Then
                    Message = "すでに在庫に戻されています。"
                    Stock = Nothing
                    RaisePropertyChanged("Message")
                    RaisePropertyChanged("Stock")
                    Return False
                End If

                ' 該当ロットがピッキング済みでないかチェック
                Dim pick = db.Pickings.Where(Function(o) o.Deplecated = 0 And
                                 o.Status = LocalModelBase.StatusType.NotSynced And
                                 o.Stock.MasterMaterial_Id = material.Id And
                                 o.Stock.LotNo = lotno And
                                 o.Stock.LotBranchNo = lotbranchno)
                If pick.Any() Then
                    Message = "すでにピッキング済みです。"
                    Stock = Nothing
                    RaisePropertyChanged("Message")
                    RaisePropertyChanged("Stock")
                    Return False
                End If

            End Using

            Message = ""
            RaisePropertyChanged("Message")
            RaisePropertyChanged("Stock")
            Return True
        End Function
    End Class
End Namespace