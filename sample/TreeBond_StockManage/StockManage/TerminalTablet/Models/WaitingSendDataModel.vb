﻿Imports System.Linq
Imports System.Data.Entity
Imports DataModels

Public Class WaitingSendDataModel

    Public Shared Function WaitingDataStr() As String
        Dim s As String = ""

        Using db As New LocalContext
            Dim NumPicking = db.Pickings.Where(Function(o) o.Status <> LocalModelBase.StatusType.Synced And o.Deplecated = 0).Count
            Dim NumReissue = db.Reissues.Where(Function(o) o.Status <> LocalModelBase.StatusType.Synced And o.Deplecated = 0).Count
            '' Dim NumArrival = db.Arrivals.Where(Function(o) o.Status <> LocalModelBase.StatusType.Synced And o.Deplecated = 0).Count

            If NumPicking > 0 Then
                s += "ピッキング（" & NumPicking.ToString & "）"
            End If
            If NumReissue > 0 Then
                s += "ラベル再発行（" & NumReissue.ToString & "）"
            End If
            'If NumArrival > 0 Then
            '    s += "受け取り（" & NumArrival.ToString & "）"
            'End If

            If Len(s) > 0 Then
                s = "未連携データ：" & s & "  "
            End If
        End Using

        Return s
    End Function

End Class
