﻿Imports System.Linq
Imports System.Data.Entity
Imports AppCommon
Imports AppCommon.Models
Imports DataModels

Namespace Models
    Public MustInherit Class StockPickingCheckBaseModel
        Inherits NotificationObject

        'NotificationObjectはプロパティ変更通知の仕組みを実装したオブジェクトです。

        Public Property Message As String

        Public Property Material As MasterMaterial
        Public Property Stock As Stock

        Public Function CheckStock(qr As LabelQRModel) As Boolean
            If qr.Status = LabelQRModel.StatusType.Void Then
                Message = "QRコードが読み取れませんでした。"
                RaisePropertyChanged("Message")
                Return False
            ElseIf qr.Status = LabelQRModel.StatusType.Invalid Then
                Message = "QRコードの内容が在庫ラベルではありません。"
                RaisePropertyChanged("Message")
                Return False
            End If

            Using db As New LocalContext
                Material = db.Materials.Where(Function(o) o.MaterialCd = qr.MaterialCd).First
                If Material.MasterSupplier_Id IsNot Nothing Then
                    Dim Supplier = db.Suppliers.Find(Material.MasterSupplier_Id)
                    Material.MasterSupplier = Supplier
                End If
                RaisePropertyChanged("Material")
            End Using

            Return CheckStock(Material, qr.LotNo, qr.LotBranchNo)
        End Function

        Protected MustOverride Function CheckStock(material As MasterMaterial, lotno As String, lotbranchno As String) As Boolean
    End Class
End Namespace