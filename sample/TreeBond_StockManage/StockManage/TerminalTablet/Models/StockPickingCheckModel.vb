﻿Imports System.Linq
Imports System.Data.Entity
Imports AppCommon
Imports AppCommon.Models
Imports DataModels

Namespace Models
    Public Class StockPickingCheckModel
        Inherits StockPickingCheckBaseModel

        Protected Overrides Function CheckStock(material As MasterMaterial, lotno As String, lotbranchno As String) As Boolean
            ' 期限チェック時の持ち出し可否（通常ユーザーはチェック時持ち出し不可）
            Dim isForceTakeout As Boolean =
                If(LoginModel.GetLoginUser.TakeoutPermission = TakeoutPermissionType.CanNewerTakeout, True, False)

            Using db As New LocalContext

                ' 在庫を検索して使用期限をチェックする
                Dim lots = db.Stocks.Where(Function(o) o.Deplecated = 0 And
                                              o.StockFlag = StockType.Stock And
                                              o.MasterMaterial_Id = material.Id)

                Dim lot = lots.Where(Function(o) o.LotBranchNo = lotbranchno)
                If lotno IsNot Nothing AndAlso lotno.Length > 0 Then
                    lot = lot.Where(Function(o) o.LotNo = lotno)
                Else
                    lot = lot.Where(Function(o) o.LotNo Is Nothing)
                End If

                Dim comparisonLots = lots.Where(Function(o) o.StockFlag = StockType.Stock And o.ExpirationDate >= DateTime.Today).Except(lot)
                Dim recentLots = comparisonLots.Where(Function(o) o.LotNo = lotno)
                Dim otherRecentLots = comparisonLots.Where(Function(o) o.LotNo <> lotno)

                Dim recentExpDate = recentLots.Min(Function(o) o.ExpirationDate)
                Dim otherStockExpDate = otherRecentLots.Min(Function(o) o.ExpirationDate)

                Dim openedLots = recentLots.Where(Function(o) o.OpenedFlag = OpenedType.Opened)
                Dim otherOpenedLots = otherRecentLots.Where(Function(o) o.OpenedFlag = OpenedType.Opened)

                ' ロットの在庫チェック
                If Not lot.Any() Then
                    Message = "ロットの在庫データがありません。もしくは持ち出されています。"
                    Stock = Nothing
                    RaisePropertyChanged("Message")
                    RaisePropertyChanged("Stock")
                    Return False
                End If
                Stock = lot.First

                ' 該当ロットがピッキング済みでないかチェック
                Dim pick = db.Pickings.Where(Function(o) o.Deplecated = 0 And
                                                 o.Status = LocalModelBase.StatusType.NotSynced And
                                                 o.Stock.MasterMaterial_Id = material.Id And
                                                 o.Stock.LotNo = lotno And
                                                 o.Stock.LotBranchNo = lotbranchno)
                If pick.Any() Then
                    Message = "すでにピッキング済みです。"
                    Stock = Nothing
                    RaisePropertyChanged("Message")
                    RaisePropertyChanged("Stock")
                    Return False
                End If

                ' 使用期限のチェック
                If Stock.ExpirationDate IsNot Nothing AndAlso Stock.ExpirationDate < DateTime.Today Then
                    Message = "使用期限を過ぎています。"
                ElseIf (Stock.ExpirationDate Is Nothing AndAlso (recentExpDate IsNot Nothing OrElse otherStockExpDate IsNot Nothing)) OrElse
                    (Stock.ExpirationDate IsNot Nothing AndAlso otherStockExpDate IsNot Nothing AndAlso Stock.ExpirationDate > otherStockExpDate) OrElse
                    (Stock.ExpirationDate IsNot Nothing AndAlso recentExpDate IsNot Nothing AndAlso Stock.ExpirationDate > recentExpDate) Then

                    Message = "選択した原料より使用期限が近いものがあります。"

                ElseIf (openedLots.Any() OrElse otherOpenedLots.Any()) AndAlso Stock.OpenedFlag = OpenedType.NotOpen Then
                    Message = "選択した原料以外で開封済みのものがあります。"

                ElseIf Stock.OpenedFlag = OpenedType.Opened Then
                    '開封済みロットの開封日チェック
                    Message = CheckDate(Stock.OpeningDate, recentLots, otherRecentLots, Function(o) o.OpeningDate, "開封")

                ElseIf material.ExpirationDateType = ExpirationType.ByArrival Then
                    '受入日基準品の受入日チェック
                    Message = CheckDate(Stock.ArrivalHistory.ArrivalDate, recentLots, otherRecentLots, Function(o) o.ArrivalHistory.ArrivalDate, "受入")

                ElseIf material.ExpirationDateType = ExpirationType.ByProduction Then
                    '製造日基準品の製造日チェック
                    Message = CheckDate(Stock.ArrivalHistory.ProductionDate, recentLots, otherRecentLots, Function(o) o.ArrivalHistory.ProductionDate, "製造")

                Else
                    'チェックOK
                    Message = String.Empty
                End If
            End Using

            If Message.Length > 0 Then
                AppCommon.Util.LongBeep()
            End If

            RaisePropertyChanged("Message")
            RaisePropertyChanged("Stock")
            Return If(Message.Length > 0, isForceTakeout, True)
        End Function

        Private Function CheckDate(targetDate As DateTime?,
                                   recentLots As IQueryable(Of Stock),
                                   otherRecentLots As IQueryable(Of Stock),
                                   getminF As Func(Of Stock, DateTime?),
                                   msgField As String) As String

            ' 日付が設定されていないとき、ピッキングできるようにする
            If Not targetDate.HasValue Then
                Return String.Empty
            End If

            Dim firstestDate = recentLots.Min(getminF)
            Dim otherStockDate = otherRecentLots.Min(getminF)

            If (firstestDate IsNot Nothing AndAlso targetDate > firstestDate) OrElse
                (otherRecentLots IsNot Nothing AndAlso targetDate > otherStockDate) Then

                Return "選択した原料より先に" & msgField & "したものがあります。"
            End If

            Return String.Empty
        End Function

    End Class
End Namespace