﻿Imports System.Linq
Imports System.Data.Entity
Imports DataModels

Namespace Models
    Public Class StockPickingReissueCheckModel
        Inherits StockPickingCheckBaseModel

        Protected Overrides Function CheckStock(material As DataModels.MasterMaterial, lotno As String, lotbranchno As String) As Boolean
            Using db As New LocalContext

                ' 在庫を検索する
                Dim lot = db.Stocks.Where(Function(o) o.Deplecated = 0 AndAlso
                                              o.MasterMaterial_Id = material.Id AndAlso
                                              o.LotBranchNo = lotbranchno)

                If lotno IsNot Nothing AndAlso lotno.Length > 0 Then
                    lot = lot.Where(Function(o) o.LotNo = lotno)
                Else
                    lot = lot.Where(Function(o) o.LotNo Is Nothing)
                End If

                ' ロットの在庫チェック
                If Not lot.Any() Then
                    Message = "ロットの在庫データがありません。"
                    Stock = Nothing
                    RaisePropertyChanged("Message")
                    RaisePropertyChanged("Stock")
                    Return False
                End If
                Stock = lot.First

                ' 該当ロットがピッキング済みでないかチェック
                Dim pick = db.Pickings.Where(Function(o) o.Deplecated = 0 And
                                                 o.Status = LocalModelBase.StatusType.NotSynced And
                                                 o.Stock.MasterMaterial_Id = material.Id And
                                                 o.Stock.LotNo = lotno And
                                                 o.Stock.LotBranchNo = lotbranchno)
                If pick.Any() Then
                    Message = "すでにピッキング済みです。"
                    Stock = Nothing
                    RaisePropertyChanged("Message")
                    RaisePropertyChanged("Stock")
                    Return False
                End If
            End Using

            Message = ""
            RaisePropertyChanged("Message")
            RaisePropertyChanged("Stock")
            Return True
        End Function
    End Class
End Namespace