﻿Imports System.ComponentModel.DataAnnotations

Public Class MasterSupplier
    Inherits ModelBase

    <Key()> Public Property Id As Integer
    <Required()> <Index(IsUnique:=True)> <MaxLength(60)> Public Property Code As String
    <MaxLength(60)> Public Property Name As String

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        db.Entry(DirectCast(db, ICommonModels).Suppliers.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Overrides Function IntegrityCheck(ByRef db As Entity.DbContext) As Boolean
        Dim d = TryCast(db, SMContext)
        If d Is Nothing Then
            Return MyBase.IntegrityCheck(db)
        End If

        ' 取引先コードの重複チェック
        Dim q = d.Suppliers.Where(Function(o) o.Deplecated = 0 AndAlso o.Code = Code AndAlso o.Id <> Id)
        If q.Any Then
            ErrorMessage += "取引先コードが重複しています"
            Return False
        End If

        Return MyBase.IntegrityCheck(db)
    End Function
End Class
