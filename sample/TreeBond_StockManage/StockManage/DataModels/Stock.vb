﻿
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports DataModels

Public Class Stock
    Inherits ModelBase

    <Key()> Public Property Id As Integer
    <MaxLength(20)> Public Property LotNo As String
    Public Property LotBranchNo As System.Nullable(Of Integer)
    Public Property OpeningDate As System.Nullable(Of DateTime)
    Public Property ExpirationDate As System.Nullable(Of DateTime)

    Private _OpenedFlag As System.Nullable(Of OpenedType)
    Public Property OpenedFlag As System.Nullable(Of OpenedType)
        Get
            Return _OpenedFlag
        End Get
        Set(value As System.Nullable(Of OpenedType))
            _OpenedFlag = value
            RaisePropertyChanged()
        End Set
    End Property
    Public Property ExtendedFlag As System.Nullable(Of ExtendedType)
    Public Property ReprintFlag As System.Nullable(Of ReprintType)
    Public Property StockFlag As System.Nullable(Of StockType)

    Public Property MasterMaterial_Id As System.Nullable(Of Integer)
    <ForeignKey("MasterMaterial_Id")> Public Overridable Property MasterMaterial As MasterMaterial
    Public Property ArrivalHistory_Id As System.Nullable(Of Integer)
    <ForeignKey("ArrivalHistory_Id")> Public Overridable Property ArrivalHistory As ArrivalHistory

    <NotMapped> Public Property IsLastLabel As Boolean = False

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal arrival As Arrival, Optional arrivalHist As ArrivalHistory = Nothing)
        MyBase.New(arrival)

        Me.LotNo = arrival.LotNo
        Me.ExpirationDate = arrival.ExpirationDate
        Me.MasterMaterial_Id = arrival.MasterMaterial_Id

        If arrivalHist IsNot Nothing Then
            Me.ArrivalHistory = arrivalHist
        End If
    End Sub

    '新規在庫作成
    Public Shared Function StockFactory(ByVal arrival As Arrival, arrivalHist As ArrivalHistory, lastBranchNo As Integer) As IList(Of Stock)
        Dim stocks As New List(Of Stock)
        For i = 1 To arrival.NumLabel
            Dim stock = NewStock(arrival, arrivalHist)
            stock.LotBranchNo = i + lastBranchNo

            stocks.Add(stock)
        Next

        Return stocks
    End Function

    Private Shared Function NewStock(arrival As Arrival, arrivalHist As ArrivalHistory) As Stock
        Dim stock As New Stock(arrival, arrivalHist)
        stock.OpenedFlag = OpenedType.NotOpen
        stock.StockFlag = StockType.Stock
        stock.ReprintFlag = ReprintType.NotReprinted
        stock.ExtendedFlag = ExtendedType.NotExtended

        Return stock
    End Function

    '発行済み在庫の最大ロット枝番取得
    Public Shared Function LastBranchNo(ByRef db As ICommonModels, ByVal arrival As Arrival) As Integer
        Dim maxNo = db.Stocks.Where(Function(o) o.Deplecated = 0 And
                                    o.MasterMaterial_Id = arrival.MasterMaterial_Id And
                                    o.LotNo = arrival.LotNo).Max(Function(o) o.LotBranchNo)

        Return If(maxNo IsNot Nothing, maxNo, 0)
    End Function

#Region "文字列化表示項目"

    <NotMapped> Public ReadOnly Property OpenedFlagText As String
        Get
            Select Case OpenedFlag
                Case OpenedType.Opened
                    Return "半端原料"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    <NotMapped> Public ReadOnly Property ExtendedFlagText As String
        Get
            Select Case ExtendedFlag
                Case ExtendedType.Extended
                    Return "使用期限延長済"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    <NotMapped> Public ReadOnly Property ReprintFlagText As String
        Get
            Select Case ReprintFlag
                Case ReprintType.Reprinted
                    Return "ラベル再発行済"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    <NotMapped> Public ReadOnly Property StockTypeText As String
        Get
            Select Case StockFlag
                Case StockType.TakingOut
                    Return "持ち出し"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    <NotMapped> Public ReadOnly Property StatusText As String
        Get
            Dim s As String = ""

            s += If(OpenedFlagText.Length > 0, OpenedFlagText & vbCrLf, "")
            s += If(ExtendedFlagText.Length > 0, ExtendedFlagText & vbCrLf, "")
            s += If(ReprintFlagText.Length > 0, ReprintFlagText & vbCrLf, "")

            Return s
        End Get
    End Property

    <NotMapped> Public ReadOnly Property StatusTextWoCR As String
        Get
            Dim s As String = ""

            s += If(OpenedFlagText.Length > 0, OpenedFlagText & " ", "")
            s += If(ExtendedFlagText.Length > 0, ExtendedFlagText & " ", "")
            s += If(ReprintFlagText.Length > 0, ReprintFlagText & " ", "")

            Return s
        End Get
    End Property
#End Region

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As ICommonModels = DirectCast(db, ICommonModels)

        Me.MasterMaterial = Nothing
        Me.ArrivalHistory = Nothing
        db.Entry(d.Stocks.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

End Class
