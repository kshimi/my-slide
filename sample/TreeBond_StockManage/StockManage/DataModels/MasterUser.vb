﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class MasterUser
    Inherits ModelBase

    <Key()> Public Property Id As Integer
    <Required()> <MaxLength(20)> Public Property UserCd As String
    <MaxLength(100)> Public Property Name As String
    Public Property TakeoutPermission As System.Nullable(Of TakeoutPermissionType)

    Public Property MasterSection_Id As System.Nullable(Of Integer)
    <ForeignKey("MasterSection_Id")> Public Overridable Property MasterSection As MasterSection

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As ICommonModels = DirectCast(db, ICommonModels)

        Me.MasterSection = Nothing
        db.Entry(d.Users.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Overrides Function IntegrityCheck(ByRef db As Entity.DbContext) As Boolean
        Dim d = TryCast(db, SMContext)
        If d Is Nothing Then
            Return MyBase.IntegrityCheck(db)
        End If

        ' ユーザーコードの重複チェック
        Dim q = d.Users.Where(Function(o) o.Deplecated = 0 AndAlso o.UserCd = UserCd AndAlso o.Id <> Id)
        If q.Any Then
            ErrorMessage += "社員番号が重複しています"
            Return False
        End If

        Return MyBase.IntegrityCheck(db)
    End Function
End Class
