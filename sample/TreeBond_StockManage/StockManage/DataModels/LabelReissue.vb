﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.ComponentModel

Public Class LabelReissue
    Inherits ModelBase
    Implements INotifyPropertyChanged

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(baseData As IModelBase)
        MyBase.New(baseData)
    End Sub

    <Key()> Public Property Id As Integer
    Public Property Status As System.Nullable(Of LabelPrintStatusType)

    ' 開封日
    Private _ModifyOpeningDate As System.Nullable(Of DateTime)
    Public Property ModifyOpeningDate As System.Nullable(Of DateTime)
        Get
            Return _ModifyOpeningDate
        End Get
        Set(value As System.Nullable(Of DateTime))
            _ModifyOpeningDate = value
            RaisePropertyChanged()
        End Set
    End Property

    ' 使用期限
    Private _ModifyExpirationDate As System.Nullable(Of DateTime)
    Public Property ModifyExpirationDate As System.Nullable(Of DateTime)
        Get
            Return _ModifyExpirationDate
        End Get
        Set(value As System.Nullable(Of DateTime))
            _ModifyExpirationDate = value
            RaisePropertyChanged()
        End Set
    End Property

    ' 開封（半端原料）
    Private _ModifyOpenedFlag As System.Nullable(Of OpenedType)
    Public Property ModifyOpenedFlag As System.Nullable(Of OpenedType)
        Get
            Return _ModifyOpenedFlag
        End Get
        Set(value As System.Nullable(Of OpenedType))
            _ModifyOpenedFlag = value
            RaisePropertyChanged()
        End Set
    End Property

    ' 使用期限延長
    Private _ModifyExtendedFlag As System.Nullable(Of ExtendedType)
    Public Property ModifyExtendedFlag As System.Nullable(Of ExtendedType)
        Get
            Return _ModifyExtendedFlag
        End Get
        Set(value As System.Nullable(Of ExtendedType))
            _ModifyExtendedFlag = value
            RaisePropertyChanged()
        End Set
    End Property

    Public Property Stock_Id As System.Nullable(Of Integer)
    <ForeignKey("Stock_Id")> Public Overridable Property Stock As Stock

#Region "文字列化表示項目"

    <NotMapped> Public ReadOnly Property ModifyOpenedFlagText As String
        Get
            Return If(ModifyOpenedFlag = OpenedType.Opened, "半端原料", "")
        End Get
    End Property

    <NotMapped> Public ReadOnly Property ModifyExtendedFlagText As String
        Get
            Return If(ModifyExtendedFlag = ExtendedType.Extended, "使用期限延長済", "")
        End Get
    End Property

    <NotMapped> Public ReadOnly Property StatusText As String
        Get
            Dim s As String = ""

            s += If(ModifyOpenedFlagText.Length > 0, ModifyOpenedFlagText & vbCrLf, "")
            s += If(ModifyExtendedFlagText.Length > 0, ModifyExtendedFlagText & vbCrLf, "")
            s += If(Stock.ReprintFlagText.Length > 0, Stock.ReprintFlagText & vbCrLf, "")

            Return s
        End Get
    End Property
#End Region

#Region "一覧制御項目"
    <NotMapped> Public Property Check As Boolean = True
#End Region

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As SMContext = db

        Me.Stock = Nothing
        db.Entry(d.LabelReissues.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

End Class
