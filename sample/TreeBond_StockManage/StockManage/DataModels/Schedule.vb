﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Schedule
    Inherits ModelBase
    Implements IDetachable

    <Key()> Public Property Id As Integer
    Public Property ScheduleDate As System.Nullable(Of DateTime)
    Public Property NumSchedule As System.Nullable(Of Double) = 0
    Public Property NumArrival As System.Nullable(Of Double) = 0
    Public Property CompleteFlag As System.Nullable(Of CompletionType) = CompletionType.Incomplete

    Public Property MasterMaterial_Id As System.Nullable(Of Integer)
    <ForeignKey("MasterMaterial_Id")> Public Overridable Property MasterMaterial As MasterMaterial

    <NotMapped> Public ReadOnly Property DelayedFlag As System.Nullable(Of DelayType)
        Get
            If ScheduleDate Is Nothing OrElse
                CompleteFlag Is Nothing Then

                Return Nothing
            End If

            If CompleteFlag = CompletionType.Incomplete AndAlso ScheduleDate < DateTime.Today Then
                Return DelayType.Delayed
            Else
                Return DelayType.NonDelay
            End If
        End Get
    End Property

#Region "列挙定義"

    '発行完了
    Enum CompletionType As Integer
        Incomplete = 0
        Complete = 1
    End Enum

    '納期遅れ
    Enum DelayType As Integer
        NonDelay = 0
        Delayed = 1
    End Enum

#End Region

#Region "文字列化表示項目"

    <NotMapped> Public ReadOnly Property CompleteFlagText As String
        Get
            Select Case CompleteFlag
                Case CompletionType.Complete
                    Return "発行完了"
                Case Else
                    Return "未発行"
            End Select
        End Get
    End Property

    <NotMapped> Public ReadOnly Property DelayedFlagText As String
        Get
            Select Case DelayedFlag
                Case DelayType.Delayed
                    Return "納期遅れ"
                Case Else
                    Return ""
            End Select
        End Get
    End Property
#End Region

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Me.MasterMaterial = Nothing
        db.Entry(DirectCast(db, ICommonModels).Schedules.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Shared Sub Arrival(ByRef db As ICommonModels, ByVal id As Integer, ByVal userId As Integer)

        Dim schedule As Schedule = db.Schedules.Find(id)
        If schedule IsNot Nothing Then

            schedule.NumArrival = db.ArrivalHistories.Where(Function(o) o.Deplecated = 0 And
                                                  o.Schedule_Id = id).Sum(Function(o) o.NumArrived)


            If schedule.NumArrival >= schedule.NumSchedule Then
                schedule.CompleteFlag = CompletionType.Complete
            End If

            schedule.UpdateDate = Now
            schedule.UpdateUser = userId
        End If
    End Sub


End Class
