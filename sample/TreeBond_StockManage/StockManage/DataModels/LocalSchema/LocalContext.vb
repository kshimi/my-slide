﻿Imports System.Data.Entity

Public Class LocalContext
    Inherits DbContext
    Implements ICommonModels

    'マスタ項目
    Public Property Sections As DbSet(Of MasterSection) Implements ICommonModels.Sections
    Public Property Users As DbSet(Of MasterUser) Implements ICommonModels.Users
    Public Property Suppliers As DbSet(Of MasterSupplier) Implements ICommonModels.Suppliers
    Public Property Materials As DbSet(Of MasterMaterial) Implements ICommonModels.Materials

    'サーバ管理データ
    Public Property Schedules As DbSet(Of Schedule) Implements ICommonModels.Schedules
    Public Property Stocks As DbSet(Of Stock) Implements ICommonModels.Stocks
    Public Property ArrivalHistories As DbSet(Of ArrivalHistory) Implements ICommonModels.ArrivalHistories

    'タブレット管理データ
    Public Property Arrivals As DbSet(Of Arrival)
    Public Property Pickings As DbSet(Of Picking) Implements ICommonModels.Pickings
    Public Property Reissues As DbSet(Of Reissue)
End Class
