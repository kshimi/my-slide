﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Reissue
    Inherits LocalModelBase

    <Key()> Public Property Id As Integer
    Public Property AcceptDateTime As System.Nullable(Of DateTime)
    Public Property UnderlyingStockRevision As System.Nullable(Of Long)

    Public Property ModifyOpeningDate As System.Nullable(Of DateTime)
    Public Property ModifyExpirationDate As System.Nullable(Of DateTime)
    Public Property ModifyOpenedFlag As System.Nullable(Of OpenedType)
    Public Property ModifyExtendedFlag As System.Nullable(Of ExtendedType)

    Public Property Stock_Id As System.Nullable(Of Integer)
    <ForeignKey("Stock_Id")> Public Overridable Property Stock As Stock

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As LocalContext = DirectCast(db, LocalContext)

        Me.Stock = Nothing
        db.Entry(d.Pickings.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Sub New()
        MyBase.New()

        AcceptDateTime = DateTime.Now()
    End Sub
End Class
