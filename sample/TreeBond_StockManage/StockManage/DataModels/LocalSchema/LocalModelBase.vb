﻿Imports System.ComponentModel.DataAnnotations

Public MustInherit Class LocalModelBase
    Inherits ModelBase

    Public Property Status As StatusType

#Region "列挙定義"
    'ステータス
    Enum StatusType As Integer
        NotSynced = 0
        Synced = 1
        Errored = 2
    End Enum
#End Region

End Class
