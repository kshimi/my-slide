﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Arrival
    Inherits LocalModelBase
    Implements IValidatableObject

    <Key()> Public Property Id As Integer
    <MaxLength(20)> Public Property LotNo As String
    Public Property IsDelayedPrint As System.Nullable(Of Integer) = 0

    <NotMapped()> Public Property NewMaterial As MasterMaterial

    Private Property _ExpirationDateModel As New ExpirationDateModel
    Public Property ArrivalDate As System.Nullable(Of DateTime)
        Get
            Return _ExpirationDateModel.ArrivalDate
        End Get
        Set(value As System.Nullable(Of DateTime))
            _ExpirationDateModel.ArrivalDate = value
            RaisePropertyChanged("ExpirationDate")
        End Set
    End Property
    Public Property ProductionDate As System.Nullable(Of DateTime)
        Get
            Return _ExpirationDateModel.ProductionDate

        End Get
        Set(value As System.Nullable(Of DateTime))
            _ExpirationDateModel.ProductionDate = value
            RaisePropertyChanged("ExpirationDate")
        End Set
    End Property

    Private Property _ExpirationDate As System.Nullable(Of DateTime)
    Public Property ExpirationDate As System.Nullable(Of DateTime)
        Get
            If _ExpirationDate.HasValue Then
                Return _ExpirationDate
            Else
                _ExpirationDateModel.material = MasterMaterial
                _ExpirationDate = _ExpirationDateModel.DefaultExpirationDate
                Return _ExpirationDate
            End If
        End Get
        Set(value As System.Nullable(Of DateTime))
            _ExpirationDate = value
        End Set
    End Property

    <Range(1, 999999, ErrorMessage:="数量を入力して下さい")> Public Property NumArrived As Double
    <Range(1, 999999, ErrorMessage:="ラベル枚数を入力して下さい")> Public Property NumLabel As Integer
    <MaxLength(20)> Public Property InsertDevice As String

    Public Property MasterMaterial_Id As System.Nullable(Of Integer)
    Private _MasterMaterial As MasterMaterial
    <ForeignKey("MasterMaterial_Id")> Public Overridable Property MasterMaterial As MasterMaterial        '入荷予定設定しない場合のみ設定する
        Get
            If Schedule IsNot Nothing AndAlso Schedule.MasterMaterial IsNot Nothing Then
                Return Schedule.MasterMaterial
            Else
                Return _MasterMaterial
            End If
        End Get
        Set(value As MasterMaterial)
            _MasterMaterial = value
            RaisePropertyChanged()

            RaisePropertyChanged("ExpirationDate")
        End Set
    End Property
    Public Property Schedule_Id As System.Nullable(Of Integer)
    <ForeignKey("Schedule_Id")> Public Overridable Property Schedule As Schedule

    Public Sub New()
        MyBase.New()

        ArrivalDate = DateTime.Today
    End Sub

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As LocalContext = DirectCast(db, LocalContext)

        Me._MasterMaterial = Nothing
        Me.Schedule = Nothing
        db.Entry(d.Arrivals.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Iterator Function Validate(validationContext As ValidationContext) As IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate
        Dim material = Me.MasterMaterial
        If material Is Nothing AndAlso NewMaterial IsNot Nothing Then
            material = NewMaterial
        End If
        If material IsNot Nothing Then

            If material.IsLotUnmanaged = 1 AndAlso String.IsNullOrEmpty(LotNo) Then
                Yield New ValidationResult("ロット番号を設定してください。")
            End If

            If material.ExpirationDateType = ExpirationType.ByProduction AndAlso ProductionDate Is Nothing Then
                Yield New ValidationResult("製造日を設定してください。")
            End If

        End If
    End Function
End Class
