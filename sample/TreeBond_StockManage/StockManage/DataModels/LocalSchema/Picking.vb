﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports DataModels

Public Class Picking
    Inherits LocalModelBase

    <Key()> Public Property Id As Integer
    Public Property PickingDateTime As System.Nullable(Of DateTime)
    Public Property PickingFlag As System.Nullable(Of PickingType)
    Public Property UnderlyingStockRevision As System.Nullable(Of Long)

    Public Property ModifyExpirationDate As System.Nullable(Of DateTime)
    Public Property ModifyOpeningDate As System.Nullable(Of DateTime)
    Public Property ModifyOpenedFlag As System.Nullable(Of OpenedType)

    Public Property Stock_Id As System.Nullable(Of Integer)
    <ForeignKey("Stock_Id")> Public Overridable Property Stock As Stock

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As ICommonModels = DirectCast(db, ICommonModels)

        Me.Stock = Nothing
        db.Entry(d.Pickings.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Sub New()
        MyBase.New()

        PickingDateTime = DateTime.Now()
    End Sub

End Class
