﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class ArrivalHistory
    Inherits ModelBase

    <Key()> Public Property Id As Integer
    Public Property ArrivalDate As System.Nullable(Of DateTime)
    Public Property ProductionDate As System.Nullable(Of DateTime)
    Public Property NumArrived As System.Nullable(Of Double)
    Public Property InsertDevice As String
    Public Property Status As System.Nullable(Of LabelPrintStatusType) = LabelPrintStatusType.NotYet

    Private Property _MasterMaterial_Id As System.Nullable(Of Integer)
    Public Property MasterMaterial_Id As System.Nullable(Of Integer)
        Get
            If Schedule IsNot Nothing AndAlso Schedule.MasterMaterial IsNot Nothing Then
                Return Schedule.MasterMaterial.Id
            Else
                Return _MasterMaterial_Id
            End If
        End Get
        Set(value As System.Nullable(Of Integer))
            _MasterMaterial_Id = value
        End Set
    End Property
    Private _MasterMaterial As MasterMaterial
    <ForeignKey("MasterMaterial_Id")> Public Overridable Property MasterMaterial As MasterMaterial        '入荷予定設定しない場合のみ設定する
        Get
            If Schedule IsNot Nothing Then
                Return Schedule.MasterMaterial
            Else
                Return _MasterMaterial
            End If
        End Get
        Set(value As MasterMaterial)
            _MasterMaterial = value
        End Set
    End Property
    Public Property Schedule_Id As System.Nullable(Of Integer)
    <ForeignKey("Schedule_Id")> Public Overridable Property Schedule As Schedule

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal arrival As Arrival)
        MyBase.New(arrival)

        Me.ArrivalDate = arrival.ArrivalDate
        Me.ProductionDate = arrival.ProductionDate
        Me.NumArrived = arrival.NumArrived
        Me.InsertDevice = arrival.InsertDevice

        If arrival.Schedule IsNot Nothing Then
            Me.Schedule_Id = arrival.Schedule.Id
        ElseIf arrival.Schedule_Id IsNot Nothing Then
            Me.Schedule_Id = arrival.Schedule_Id
        ElseIf arrival.MasterMaterial IsNot Nothing Then
            Me.MasterMaterial_Id = arrival.MasterMaterial.Id
        ElseIf arrival.MasterMaterial_Id IsNot Nothing Then
            Me.MasterMaterial_Id = arrival.MasterMaterial_Id
        End If
    End Sub

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As ICommonModels = DirectCast(db, ICommonModels)

        Me._MasterMaterial = Nothing
        Me.Schedule = Nothing
        db.Entry(d.ArrivalHistories.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

End Class
