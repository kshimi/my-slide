﻿Public Interface IModelBase
    Inherits IDetachable

    Property Revision As Long
    Property Deplecated As Integer
    Property InsertDate As System.Nullable(Of DateTime)
    Property InsertUser As Integer
    Property UpdateDate As System.Nullable(Of DateTime)
    Property UpdateUser As Integer

    Function ShallowCopy(o As Object) As Object
    Function IsValid() As Boolean
End Interface
