﻿Imports System.Linq
Imports System.Data.Entity

Public Class MaterialSelectionModel
    Inherits Livet.NotificationObject

    Private db As New LocalContext

    '絞り込んだ原材料の一覧
    Public Property Materials As IList(Of MasterMaterial)

    '原材料コード 絞り込み条件
    Private Property _Cd As String
    Public Property Cd As String
        Get
            Return _Cd
        End Get
        Set(value As String)
            _Cd = value

            SearchMaterials()
        End Set
    End Property

    '原材料名 絞り込み条件
    Private Property _Name As String
    Public Property Name As String
        Get
            Return _Name
        End Get
        Set(value As String)
            _Name = value

            SearchMaterials()
        End Set
    End Property

    Private Sub SearchMaterials()
        ' コード、名称とも空欄の時は検索しない
        If Cd Is Nothing AndAlso Name Is Nothing Then
            Exit Sub
        End If

        ' コードと名称で検索し、結果を原材料リストに設定する
        Dim q = db.Materials.Where(Function(o) o.Deplecated = 0)

        If Cd IsNot Nothing AndAlso Cd.Length > 0 Then
            q = q.Where(Function(o) o.MaterialCd.Contains(Cd))
        End If

        If Name IsNot Nothing AndAlso Name.Length > 0 Then
            q = q.Where(Function(o) o.Name.Contains(Name))
        End If

        Materials = q.ToList

        RaisePropertyChanged("Materials")
    End Sub

End Class
