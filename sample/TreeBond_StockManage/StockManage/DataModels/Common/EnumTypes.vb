﻿Imports System.ComponentModel
Imports System.Windows.Data
Imports System.Reflection

#Region "MasterUser"

'持ち出し時権限
Public Enum TakeoutPermissionType As Integer
    <Description("標準")> Normal = 0
    <Description("新在庫持ち出し可")> CanNewerTakeout = 1
End Enum

#End Region

#Region "Material"
'使用期限管理方式
Public Enum ExpirationType As Integer
    <Description("受入日")> ByArrival = 0
    <Description("製造日")> ByProduction = 1
    <Description("使用期限なし")> None = 2
End Enum
#End Region

#Region "Stock"
'開封有無
Public Enum OpenedType As Integer
    NotOpen = 0
    Opened = 1
End Enum

'使用期限延長有無
Public Enum ExtendedType As Integer
    NotExtended = 0
    Extended = 1
End Enum

'ラベル再発行有無（在庫がラベル再発行済みか否か）
Public Enum ReprintType As Integer
    NotReprinted = 0
    Reprinted = 1
End Enum

'持ち出し状態
Public Enum StockType As Integer
    Stock = 0
    TakingOut = 1
End Enum

#End Region

#Region "ArrivalHistory"

'ラベル発行状態（発行要求が処理済みか否か)
Public Enum LabelPrintStatusType As Integer
    NotYet = 0
    Printed = 1
    ExceptIssue = 2
End Enum

#End Region

#Region "Picking"

'ピッキング種別（持ち出し/持ち戻り）
Public Enum PickingType As Integer
    TakingOut = 0
    ReturningInOpen = 1
    ReturningInNotopen = 2
End Enum

#End Region

<ValueConversion(GetType(Object), GetType(String))>
Public Class EnumToFriendlyNameConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type,
                            parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert

        If value IsNot Nothing Then
            Dim fi As FieldInfo = value.GetType().GetField(value.ToString())

            If fi IsNot Nothing Then
                Dim attributes As DescriptionAttribute() = CType(fi.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

                Return If(attributes.Length > 0 AndAlso
                          Not String.IsNullOrEmpty(attributes(0).Description),
                          attributes(0).Description, value.ToString())
            End If
        End If

        Return String.Empty
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New Exception("Can't convert back")
    End Function
End Class

Public Class EnumToBooleanConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        If value IsNot Nothing Then
            If TypeOf value Is [Enum] Then
                If CInt(value) = 1 Then
                    Return True
                End If
            End If
        End If

        Return False
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        If value Is Nothing Then
            Return Nothing
        End If

        Dim t As Type = targetType
        If targetType.GenericTypeArguments.Length > 0 Then
            t = targetType.GenericTypeArguments(0)
        End If

        If TypeOf value Is Boolean Then
            If DirectCast(value, Boolean) Then
                Return [Enum].ToObject(t, 1)
            Else
                Return [Enum].ToObject(t, 0)
            End If
        End If

        Return Nothing
    End Function
End Class