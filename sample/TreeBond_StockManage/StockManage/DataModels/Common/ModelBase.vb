﻿Imports System.ComponentModel.DataAnnotations

Public MustInherit Class ModelBase
    Inherits Livet.NotificationObject
    Implements IModelBase

    Public Property Revision As Long Implements IModelBase.Revision
    Public Property Deplecated As Integer Implements IModelBase.Deplecated
    Public Property InsertDate As System.Nullable(Of DateTime) Implements IModelBase.InsertDate
    Public Property InsertUser As Integer Implements IModelBase.InsertUser
    Public Property UpdateDate As System.Nullable(Of DateTime) Implements IModelBase.UpdateDate
    Public Property UpdateUser As Integer Implements IModelBase.UpdateUser

    Public Sub New()
        Deplecated = 0 '初期値を使用中状態にする
        InsertDate = DateTime.Now
        UpdateDate = DateTime.Now
    End Sub

    Public Sub New(baseData As IModelBase)
        Deplecated = baseData.Deplecated
        InsertDate = baseData.InsertDate
        InsertUser = baseData.InsertUser
        UpdateDate = baseData.UpdateDate
        UpdateUser = baseData.UpdateUser
    End Sub

    Public Function ShallowCopy(o As Object) As Object Implements IModelBase.ShallowCopy
        Dim oType As Type = Me.GetType
        Dim properties = oType.GetProperties().Where( _
            Function(p) p.CanRead _
                AndAlso p.CanWrite _
                AndAlso p.GetCustomAttributes(GetType(System.ComponentModel.DataAnnotations.Schema.NotMappedAttribute), True).Length = 0 _
                AndAlso Not p.Module.ScopeName.Contains("EntityProxyModule") _
                )

        For Each p In properties
            p.SetValue(o, p.GetValue(Me, Nothing), Nothing)
        Next

        Return o
    End Function

    ' モデルValidation
    <Schema.NotMapped()> Public Property ErrorMessage As String
    Public Function IsValid() As Boolean Implements IModelBase.IsValid
        Dim context As New ValidationContext(Me, Nothing, Nothing)
        Dim results As New List(Of ValidationResult)
        Dim isValidFlg As Boolean = Validator.TryValidateObject(Me, context, results, True)

        Dim msg As New System.Text.StringBuilder()
        If Not isValidFlg Then
            For Each r As ValidationResult In results
                msg.AppendLine(r.ErrorMessage)
            Next
        End If
        ErrorMessage = msg.ToString()
        RaisePropertyChanged("ErrorMessage")

        Return isValidFlg
    End Function

    Public MustOverride Sub Detach(ByRef db As Entity.DbContext) Implements IDetachable.Detach

    ' クラス固有の整合性チェック
    Public Overridable Function IntegrityCheck(ByRef db As Entity.DbContext) As Boolean
        Return True
    End Function
End Class
