﻿Imports DataModels

Public Class ExpirationDateModel

    Public Property ArrivalDate As System.Nullable(Of DateTime)
    Public Property ProductionDate As System.Nullable(Of DateTime)
    Public Property OpenedDate As System.Nullable(Of DateTime)

    Public Property material As MasterMaterial

    Public Function DefaultExpirationDate() As System.Nullable(Of DateTime)
        ' 開封されている場合は開封日の一ヶ月後
        If OpenedDate.HasValue() Then
            Return OpenedDate.Value.AddMonths(1).AddDays(-1)
        End If

        If material Is Nothing Then
            Return Nothing
        End If

        Select Case material.ExpirationDateType
            Case ExpirationType.ByArrival
                If ArrivalDate.HasValue AndAlso material.ExpirationDateMonths IsNot Nothing Then
                    Dim a As DateTime = ArrivalDate.Value.AddMonths(material.ExpirationDateMonths + 1)
                    Dim b As New DateTime(a.Year, a.Month, 1)
                    Return b.AddDays(-1)
                Else
                    Return Nothing
                End If
            Case ExpirationType.ByProduction
                If ProductionDate.HasValue AndAlso material.ExpirationDateMonths IsNot Nothing Then
                    Dim a As DateTime = ProductionDate.Value.AddMonths(material.ExpirationDateMonths + 1)
                    Dim b As New DateTime(a.Year, a.Month, 1)
                    Return b.AddDays(-1)
                Else
                    Return Nothing
                End If
            Case Else
                Return Nothing
        End Select

    End Function

End Class
