﻿Imports System.Data.Entity

Public Interface ICommonModels
    Inherits IDisposable

    'マスタ項目
    Property Sections As DbSet(Of MasterSection)
    Property Users As DbSet(Of MasterUser)
    Property Suppliers As DbSet(Of MasterSupplier)
    Property Materials As DbSet(Of MasterMaterial)

    'サーバ管理データ
    Property Schedules As DbSet(Of Schedule)
    Property Stocks As DbSet(Of Stock)
    Property ArrivalHistories As DbSet(Of ArrivalHistory)

    'クライアントからサーバーへ連携するデータ
    Property Pickings As DbSet(Of Picking)
End Interface
