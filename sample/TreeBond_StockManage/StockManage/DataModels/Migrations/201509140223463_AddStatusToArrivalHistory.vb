Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AddStatusToArrivalHistory
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.ArrivalHistories", "Status", Function(c) c.Int())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.ArrivalHistories", "Status")
        End Sub
    End Class
End Namespace
