Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AddTakeoutPermissionToUser
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.MasterUsers", "TakeoutPermission", Function(c) c.Int())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.MasterUsers", "TakeoutPermission")
        End Sub
    End Class
End Namespace
