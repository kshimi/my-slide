Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AddIsResetExtensionWhenOpenToMasterMaterial
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.MasterMaterials", "IsResetExtensionWhenOpen", Function(c) c.Int())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.MasterMaterials", "IsResetExtensionWhenOpen")
        End Sub
    End Class
End Namespace
