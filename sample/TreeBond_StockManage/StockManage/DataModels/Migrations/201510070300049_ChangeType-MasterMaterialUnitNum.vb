Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class ChangeTypeMasterMaterialUnitNum
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.MasterMaterials", "UnitNum", Function(c) c.Double())
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.MasterMaterials", "UnitNum", Function(c) c.Int())
        End Sub
    End Class
End Namespace
