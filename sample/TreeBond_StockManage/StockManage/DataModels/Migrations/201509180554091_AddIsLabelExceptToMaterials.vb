Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AddIsLabelExceptToMaterials
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.MasterMaterials", "IsLabelExcept", Function(c) c.Int())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.MasterMaterials", "IsLabelExcept")
        End Sub
    End Class
End Namespace
