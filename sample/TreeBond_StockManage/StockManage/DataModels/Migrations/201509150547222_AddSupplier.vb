Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AddSupplier
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.MasterSuppliers",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Code = c.String(nullable := False, maxLength := 60),
                        .Name = c.String(maxLength := 60),
                        .Revision = c.Long(nullable := False),
                        .Deplecated = c.Int(nullable := False),
                        .InsertDate = c.DateTime(),
                        .InsertUser = c.Int(nullable := False),
                        .UpdateDate = c.DateTime(),
                        .UpdateUser = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .Index(Function(t) t.Code, unique := True)
            
            AddColumn("dbo.MasterMaterials", "MasterSupplier_Id", Function(c) c.Int())
            CreateIndex("dbo.MasterMaterials", "MasterSupplier_Id")
            AddForeignKey("dbo.MasterMaterials", "MasterSupplier_Id", "dbo.MasterSuppliers", "Id")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.MasterMaterials", "MasterSupplier_Id", "dbo.MasterSuppliers")
            DropIndex("dbo.MasterSuppliers", New String() { "Code" })
            DropIndex("dbo.MasterMaterials", New String() { "MasterSupplier_Id" })
            DropColumn("dbo.MasterMaterials", "MasterSupplier_Id")
            DropTable("dbo.MasterSuppliers")
        End Sub
    End Class
End Namespace
