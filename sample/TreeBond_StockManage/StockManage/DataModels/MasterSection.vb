﻿Imports System.ComponentModel.DataAnnotations

Public Class MasterSection
    Inherits ModelBase

    <Key()> Public Property Id As Integer
    <MaxLength(60)> Public Property Name As String

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        db.Entry(DirectCast(db, ICommonModels).Sections.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Overrides Function IntegrityCheck(ByRef db As Entity.DbContext) As Boolean
        Dim d = TryCast(db, SMContext)
        If d Is Nothing Then
            Return MyBase.IntegrityCheck(db)
        End If

        ' 部署名の重複チェック
        Dim q = d.Sections.Where(Function(o) o.Deplecated = 0 AndAlso o.Name = Name AndAlso o.Id <> Id)
        If q.Any Then
            ErrorMessage += "部署名が重複しています"
            Return False
        End If

        Return MyBase.IntegrityCheck(db)
    End Function
End Class
