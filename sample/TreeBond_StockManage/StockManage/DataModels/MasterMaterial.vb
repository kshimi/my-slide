﻿
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Windows

Public Class MasterMaterial
    Inherits ModelBase

    <Key()> Public Property Id As Integer
    <Required()> <MaxLength(10)> Public Property MaterialCd As String
    <MaxLength(60)> Public Property Name As String
    <MaxLength(30)> Public Property CapacityDesc As String
    Public Property UnitNum As System.Nullable(Of Double)
    Public Property ExpirationDateType As System.Nullable(Of ExpirationType)
    Public Property ExpirationDateMonths As System.Nullable(Of Integer)
    Public Property Image As Byte()
    <MaxLength(30)> Public Property Note1 As String
    <MaxLength(30)> Public Property Note2 As String
    <MaxLength(30)> Public Property Note3 As String
    <MaxLength(10)> Public Property WarehouseCd As String
    <MaxLength(10)> Public Property RackCd As String

    'フラグ項目
    Public Property CantExtension As System.Nullable(Of Integer)        '使用期限延長不可
    Public Property IsOnetime As System.Nullable(Of Integer)            '使い切り
    Public Property IsEnsureStock As System.Nullable(Of Integer)        '在庫確保
    Public Property IsSimilar As System.Nullable(Of Integer)            '類似原料
    Public Property IsLotUnmanaged As System.Nullable(Of Integer)       'ロット管理対象 ※ Issue#56 で項目の意味を逆転
    Public Property IsLabelExcept As System.Nullable(Of Integer)        'ラベル発行対象外
    Public Property IsResetExtensionWhenOpen As System.Nullable(Of Integer) '開封時使用期限再設定（一ヶ月）

    Public Property MasterSupplier_Id As System.Nullable(Of Integer)
    <ForeignKey("MasterSupplier_Id")> Public Overridable Property MasterSupplier As MasterSupplier

#Region "文字列化表示項目"
    <NotMapped> Public ReadOnly Property ExpirationDateTypeText As String
        Get
            Select Case ExpirationDateType
                Case ExpirationType.ByArrival
                    Return "受入日"
                Case ExpirationType.ByProduction
                    Return "製造日"
                Case Else
                    Return "使用期限なし"
            End Select
        End Get
    End Property
    <NotMapped> Public ReadOnly Property ExpirationDateProductionVisibility As Visibility
        Get
            If ExpirationDateType = ExpirationType.ByProduction Then
                Return Visibility.Visible
            Else
                Return Visibility.Hidden
            End If
        End Get
    End Property

    <NotMapped> Public ReadOnly Property AnnotationText As String
        Get
            Return GetAnnotationText(vbCrLf)
        End Get
    End Property

    <NotMapped> Public ReadOnly Property AnnotationTextOneLine As String
        Get
            Return GetAnnotationText(" ")
        End Get
    End Property

    Private Function GetAnnotationText(delimiter As String) As String
        Dim s As String = ""

        s = ConcatText(s, CantExtension, "使用期限延長不可", delimiter)
        s = ConcatText(s, IsOnetime, "使い切り", delimiter)
        s = ConcatText(s, IsEnsureStock, "在庫確保", delimiter)
        s = ConcatText(s, IsSimilar, "類似原料", delimiter)
        s = ConcatText(s, IsLotUnmanaged, "ロット管理対象", delimiter)
        s = ConcatText(s, IsLabelExcept, "ラベル発行対象外", delimiter)
        s = ConcatText(s, IsResetExtensionWhenOpen, "開封時使用期限再設定", delimiter)

        Return s
    End Function

    Private Function ConcatText(s As String, IsValid As Integer?, word As String, delimiter As String) As String
        If IsValid Is Nothing OrElse IsValid = 0 Then
            Return s
        End If

        Return s + If(s.Length > 0, delimiter, String.Empty) + word
    End Function

    <NotMapped> Public ReadOnly Property Location As String
        Get
            Return WarehouseCd & "-" & RackCd
        End Get
    End Property

    <NotMapped> Public ReadOnly Property IsImageRegisteredText As String
        Get
            If Image IsNot Nothing Then
                Return "画像登録済"
            Else
                Return String.Empty
            End If
        End Get
    End Property

    <NotMapped> Public ReadOnly Property AllNotes As String
        Get
            Dim s As String = Note1
            If Note2 IsNot Nothing AndAlso Note2.Length > 0 Then
                s += vbCrLf & Note2
            End If
            If Note3 IsNot Nothing AndAlso Note3.Length > 0 Then
                s += vbCrLf & Note3
            End If

            Return s
        End Get
    End Property
#End Region

    Public Overrides Sub Detach(ByRef db As Entity.DbContext)
        Dim d As ICommonModels = DirectCast(db, ICommonModels)

        Me.MasterSupplier = Nothing
        db.Entry(d.Materials.Find(Me.Id)).State = Entity.EntityState.Detached
    End Sub

    Public Overrides Function IntegrityCheck(ByRef db As Entity.DbContext) As Boolean
        Dim d = TryCast(db, SMContext)
        If d Is Nothing Then
            Return MyBase.IntegrityCheck(db)
        End If

        ' 原材料CDの重複チェック
        Dim q = d.Materials.Where(Function(o) o.Deplecated = 0 AndAlso o.MaterialCd = MaterialCd AndAlso o.Id <> Id)
        If q.Any Then
            ErrorMessage += "原材料CDが重複しています"
            Return False
        End If

        Return MyBase.IntegrityCheck(db)
    End Function
End Class
