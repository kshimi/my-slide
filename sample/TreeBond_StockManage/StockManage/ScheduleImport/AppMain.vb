﻿Imports System.IO
Imports System.Configuration
Imports System.Diagnostics
Imports DataModels
Imports AppCommon.Models

Module AppMain

    Sub Main()
        Try
            Dim dirName As String = ConfigurationManager.AppSettings.Get("CsvDir")
            Dim dirInfo As DirectoryInfo = New DirectoryInfo(dirName)
            Dim doneDirInfo As DirectoryInfo = New DirectoryInfo(dirName & "\done")

            If Not dirInfo.Exists Then
                WriteLog("受入予定ファイルフォルダが見つかりませんでした。 " & DateTime.Now.ToString & " :" & dirName, EventLogEntryType.Error)
                Exit Sub
            End If

            If Not doneDirInfo.Exists Then
                doneDirInfo.Create()
            End If

            For Each f In dirInfo.EnumerateFiles()
                Dim importCsv As New ImportScheduleModel(f.FullName)
                If Not importCsv.IsValid OrElse importCsv.ScheduleDate <> DateTime.Today Then
                    Continue For
                End If

                ' CSVファイルの受入予定をデータベースに登録
                ImportScheduleModel.UpdateScheduleFromItems(importCsv.GetData)

                ' 処理済みCSVファイルを済みフォルダに移動
                MoveToDoneDir(f, doneDirInfo)
            Next

            WriteLog("受入予定ファイルを読み込みました。", EventLogEntryType.Information)

        Catch ex As Exception
            Dim msg = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg += ex.InnerException.Message
                If ex.InnerException.InnerException IsNot Nothing Then
                    msg += ex.InnerException.InnerException.Message
                End If
            End If

            WriteLog("受入予定取り込み中にエラーが発生しました。取り込み予定ファイルの内容を確認した上、手動取り込みしてください。" &
                    " " & DateTime.Now.ToString & " :" & vbCrLf & msg & vbCrLf & ex.ToString,
                   EventLogEntryType.Error)
        End Try
    End Sub

    ' CSVファイルの内容をデータベースに登録
    Private Sub ImportSchedule(items As IList(Of ImportScheduleModel.Item), db As SMContext)
        For Each item In items
            If Not item.IsMasterExist OrElse item.ScheduleNum = 0 Then
                Continue For
            End If

            Dim s = New Schedule

            s.ScheduleDate = item.ScheduleDate
            s.NumSchedule = item.ScheduleNum
            s.MasterMaterial_Id = item.MasterMaterial_Id

            s.InsertUser = 0
            s.UpdateUser = 0

            db.Schedules.Add(s)
        Next

        db.SaveChanges()
    End Sub

    ' 処理済みCSVファイルを済みフォルダに移動
    Private Sub MoveToDoneDir(f As FileInfo, doneDirInfo As DirectoryInfo)
        Dim dstFileName As String = doneDirInfo.FullName & "\" & f.Name
        Dim dstFile As New FileInfo(dstFileName)

        If dstFile.Exists Then
            dstFile.Delete()
        End If

        f.MoveTo(dstFileName)
    End Sub

    Private Sub WriteLog(m As String, t As EventLogEntryType)
        Const sName = "StockScheduleImport"
        If Not EventLog.SourceExists(sName) Then
            EventLog.CreateEventSource(sName, "Application")
        End If

        EventLog.WriteEntry(sName, m, t)
    End Sub
End Module
