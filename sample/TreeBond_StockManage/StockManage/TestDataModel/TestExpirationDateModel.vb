﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports DataModels

<TestClass()> Public Class TestExpirationDateModel

    Public mat As MasterMaterial
    Public model As ExpirationDateModel

    <TestInitialize()> Public Sub Initialize()
        mat = New MasterMaterial With {
            .ExpirationDateType = ExpirationType.ByArrival,
            .ExpirationDateMonths = 3
        }

    End Sub

    <TestMethod()> Public Sub TestMethod1()
        model = New ExpirationDateModel() With {.material = mat, .ArrivalDate = New Date(2015, 7, 15)}

        Assert.AreEqual(New Date(2015, 10, 31), model.DefaultExpirationDate())

        Console.WriteLine(model.DefaultExpirationDate())
    End Sub


    <TestMethod()>
    Public Sub TestMethod2()
        model = New ExpirationDateModel() With {.material = mat, .ArrivalDate = New Date(2015, 11, 8)}

        Assert.AreEqual(New Date(2016, 2, 29), model.DefaultExpirationDate())
    End Sub


    <TestMethod()>
    Public Sub TestMethod3()
        model = New ExpirationDateModel() With {.material = mat, .ArrivalDate = New Date(2014, 11, 8)}

        Assert.AreEqual(New Date(2015, 2, 28), model.DefaultExpirationDate())
    End Sub


    <TestMethod()>
    Public Sub TestMethod4()
        model = New ExpirationDateModel() With {.material = mat, .ArrivalDate = New Date(2015, 7, 15), .OpenedDate = New Date(2015, 8, 8)}

        Assert.AreEqual(New Date(2015, 9, 7), model.DefaultExpirationDate())
    End Sub


    <TestMethod()>
    Public Sub TestMethod5()
        model = New ExpirationDateModel() With {.material = mat, .ArrivalDate = New Date(2015, 7, 15), .OpenedDate = New Date(2016, 1, 31)}

        Assert.AreEqual(New Date(2016, 2, 28), model.DefaultExpirationDate())

        Console.WriteLine(model.DefaultExpirationDate())
    End Sub


    <TestMethod()>
    Public Sub TestMethod6()
        model = New ExpirationDateModel() With {.material = mat, .ArrivalDate = New Date(2015, 7, 15), .OpenedDate = New Date(2016, 10, 1)}

        Assert.AreEqual(New Date(2016, 10, 31), model.DefaultExpirationDate())

        Console.WriteLine(model.DefaultExpirationDate())
    End Sub

    <TestMethod()>
    Public Sub TestMaterialNothing()
        model = New ExpirationDateModel() With {.ArrivalDate = New Date(2015, 7, 7)}

        Console.WriteLine(model.DefaultExpirationDate())
        Assert.IsNull(model.DefaultExpirationDate)
    End Sub



End Class