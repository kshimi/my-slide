﻿Imports System
Imports System.Text
Imports System.Linq
Imports System.Data.Entity
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports DataModels

<TestClass()> Public Class UnitTest1

    <TestMethod()> Public Sub TestMethod1()
        Using db As New SMContext
            Dim sec = New MasterSection With {.Name = "aaa"}
            db.Sections.Add(sec)
            db.SaveChanges()

            Dim query = From s In db.Sections
                        Order By s.Name
                        Select s

            For Each item In query
                Console.WriteLine(item.Name & item.InsertDate)
            Next
        End Using
    End Sub

End Class