﻿CREATE TABLE [dbo].[MasterUser]
(
	[UserCd] INT NOT NULL PRIMARY KEY, 
    [UserName] NVARCHAR(50) NOT NULL, 
    [SectionCd] INT NULL, 
    [InsertDate] DATETIME NULL, 
    [InsertUser] INT NULL, 
    [UpdateDate] DATETIME NULL, 
    [UpdateUser] INT NULL, 
    CONSTRAINT [FK_MasterUser_MasterSection] FOREIGN KEY ([SectionCd]) REFERENCES [MasterSection]([SectionCd])
)
