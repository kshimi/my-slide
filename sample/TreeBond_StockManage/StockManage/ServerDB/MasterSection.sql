﻿CREATE TABLE [dbo].[MasterSection]
(
	[SectionCd] INT NOT NULL PRIMARY KEY, 
    [SectionName] NVARCHAR(60) NOT NULL, 
    [InsertDate] DATETIME NULL, 
    [InsertUser] INT NULL, 
    [UpdateDate] DATETIME NULL, 
    [UpdateUser] INT NULL
)
