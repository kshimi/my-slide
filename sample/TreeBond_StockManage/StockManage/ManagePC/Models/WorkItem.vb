﻿Imports DataModels

Public Class WorkItem
    Inherits ModelBase

    Public Property WorkFlag As WorkType
    Public Property WorkDate As DateTime
    Public Property WorkUser As MasterUser
    Public Property MasterMaterial As MasterMaterial
    Public Property Stock As Stock

    Public Property ArrivalHistory As ArrivalHistory
    Public Property LabelReissue As LabelReissue
    Public Property Picking As Picking

    '作業種別
    Enum WorkType As Integer
        Arrival         '受入
        Picking         'ピッキング
        Reissue         'ラベル再発行
    End Enum

    Public ReadOnly Property WorkFlagText As String
        Get
            Select Case WorkFlag
                Case WorkType.Arrival
                    Return "受入"
                Case WorkType.Picking
                    Return "ピッキング"
                Case WorkType.Reissue
                    Return "ラベル再発行"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    Public ReadOnly Property WorkText As String
        Get
            Select Case WorkFlag
                Case WorkType.Arrival
                    Return ArrivalWorkText()
                Case WorkType.Picking
                    Return PickingWorkText()
                Case WorkType.Reissue
                    Return ""
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    Private Function ArrivalWorkText() As String
        Dim s As String = ""
        If MasterMaterial.ExpirationDateType = ExpirationType.ByArrival Then
            s += "受入日:" & ArrivalHistory.ArrivalDate.Value.ToString("yyyy/MM/dd")
        ElseIf MasterMaterial.ExpirationDateType = ExpirationType.ByProduction Then
            s += "製造日:" & ArrivalHistory.ProductionDate.Value.ToString("yyyy/MM/dd")
        End If
        s += " 受入数量:" & ArrivalHistory.NumArrived.Value.ToString

        Return s
    End Function

    Private Function PickingWorkText() As String
        Dim s As String = ""
        Select Case Picking.PickingFlag
            Case PickingType.TakingOut
                s += "持ち出し"
            Case PickingType.ReturningInNotopen
                s += "持ち戻り"
            Case PickingType.ReturningInOpen
                s += "持ち戻り（開封）"
        End Select

        Return s
    End Function

    Public Overrides Sub Detach(ByRef db As System.Data.Entity.DbContext)
    End Sub
End Class
