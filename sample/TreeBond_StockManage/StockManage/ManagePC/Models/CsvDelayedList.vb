﻿Namespace Models
    Public Class CsvDelayedList
        Inherits CsvListBase

        Protected Overrides Sub WriteHeader(ByRef writer As CsvHelper.CsvWriter)
            ' ヘッダ行の出力
            writer.WriteField("発行予定日")
            writer.WriteField("発行日")
            writer.WriteField("発行予定数量")
            writer.WriteField("発行済数量")

            writer.WriteField("倉庫")
            writer.WriteField("棚")
            writer.WriteField("備考")
            writer.WriteField("原材料CD")
            writer.WriteField("原材料名")
            writer.WriteField("容量規格")
            writer.WriteField("1梱包単位数量")
            writer.WriteField("使用期限管理方法")
            writer.WriteField("使用期限設定月数")
        End Sub

        Protected Overrides Sub WriteDataRow(ByRef writer As CsvHelper.CsvWriter, ByRef row As Object)
            writer.WriteField(gf(row, "ScheduleDate"))
            writer.WriteField(gf(row, "ArrivalDate"))
            writer.WriteField(gf(row, "NumSchedule"))
            writer.WriteField(gf(row, "NumArrival"))

            writer.WriteField(gf(gf(row, "MasterMaterial"), "WarehouseCd"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "RackCd"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "AllNotes"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "MaterialCd"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "Name"))

            writer.WriteField(gf(gf(row, "MasterMaterial"), "CapacityDesc"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "UnitNum"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "ExpirationDateTypeText"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "ExpirationDateMonths"))
        End Sub
    End Class
End Namespace
