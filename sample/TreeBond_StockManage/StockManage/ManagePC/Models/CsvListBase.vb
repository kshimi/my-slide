﻿Imports System.IO
Imports Microsoft.Win32

Namespace Models
    Public MustInherit Class CsvListBase

        Public Sub CsvOut(Data As IList)
            Dim dlg As New SaveFileDialog
            dlg.FilterIndex = 1
            dlg.Filter = "CSV ファイル(.csv)|*.csv"

            If Data Is Nothing OrElse Data.Count = 0 Then
                MsgBox("出力データがありません")
                Exit Sub
            End If

            Dim result As Boolean = dlg.ShowDialog()
            If Not result Then
                Exit Sub
            End If

            Using writer As CsvHelper.CsvWriter =
                New CsvHelper.CsvWriter(New StreamWriter(dlg.FileName, False, System.Text.Encoding.UTF8))

                ' ヘッダ行の出力
                WriteHeader(writer)
                writer.NextRecord()

                ' データ行の出力
                For Each row In Data
                    WriteDataRow(writer, row)
                    writer.NextRecord()
                Next
            End Using

            MsgBox("CSVファイルを出力しました。")
        End Sub

        Protected MustOverride Sub WriteHeader(ByRef writer As CsvHelper.CsvWriter)

        Protected MustOverride Sub WriteDataRow(ByRef writer As CsvHelper.CsvWriter, ByRef row As Object)

        Protected Shared Function gf(r As Object, pName As String) As Object
            If r Is Nothing Then
                Return ""
            End If

            Dim t = r.GetType
            Dim p = t.GetProperty(pName)
            If p Is Nothing Then
                Return ""
            End If

            Dim value = p.GetValue(r)
            If value Is Nothing Then
                Return ""
            End If

            If value.GetType() = GetType(String) Then
                Return CType(value, String)
            Else
                Return value
            End If
        End Function
    End Class
End Namespace