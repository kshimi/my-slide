﻿Namespace Models
    Public Class CsvExpirationList
        Inherits CsvListBase

        Protected Overrides Sub WriteHeader(ByRef writer As CsvHelper.CsvWriter)
            ' ヘッダ行の出力
            writer.WriteField("倉庫")
            writer.WriteField("棚")
            writer.WriteField("備考")
            writer.WriteField("原材料CD")
            writer.WriteField("原材料名")

            writer.WriteField("ロット番号")
            writer.WriteField("数量")
            writer.WriteField("未開封")
            writer.WriteField("開封")
            writer.WriteField("使用期限")
            writer.WriteField("使用期限(未開封)")
            writer.WriteField("使用期限(開封)")
        End Sub

        Protected Overrides Sub WriteDataRow(ByRef writer As CsvHelper.CsvWriter, ByRef row As Object)
            writer.WriteField(gf(gf(row, "MasterMaterial"), "WarehouseCd"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "RackCd"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "AllNotes"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "MaterialCd"))
            writer.WriteField(gf(gf(row, "MasterMaterial"), "Name"))

            writer.WriteField(gf(row, "LotNo"))
            writer.WriteField(gf(row, "NumLot"))
            writer.WriteField(gf(row, "NotopenCount"))
            writer.WriteField(gf(row, "OpenedCount"))
            writer.WriteField(gf(row, "ExpirationDate"))
            writer.WriteField(gf(row, "ExpirationDateNotopen"))
            writer.WriteField(gf(row, "ExpirationDateOpened"))
        End Sub
    End Class
End Namespace