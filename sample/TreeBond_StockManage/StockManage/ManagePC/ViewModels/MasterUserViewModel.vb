﻿Imports System.Linq
Imports System.Data.Entity
Imports DataModels
Imports AppCommon
Imports Livet.Commands

Namespace ViewModels
    Public Class MasterUserViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Public Overloads Sub Initialize()
            'グリッドデータの初期化定義
            Dim fq = Function()
                         Using db As New SMContext
                             Dim q = From u In db.Users
                                     Order By u.Name
                                     Where u.Deplecated = 0
                                     Select u

                             q.Load()
                             Return q.ToList()
                         End Using
                     End Function

            MyBase.Initialize(GetType(MasterUser), fq)

            Using db As New SMContext
                Dim sq = From s In db.Sections
                         Order By s.Name
                         Select s

                SectionItems = sq.ToList()
            End Using

            'グリッドデータ初期表示
            MyBase.DataUpdate()
        End Sub

#Region "SectionItems変更通知プロパティ"
        Private _SectionItems As IList

        Public Property SectionItems() As IList
            Get
                Return _SectionItems
            End Get
            Set(ByVal value As IList)
                If (_SectionItems Is value) Then Return
                _SectionItems = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

    End Class
End Namespace