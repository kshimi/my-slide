﻿Imports System.Data.Entity
Imports DataModels

Namespace ViewModels
    Public Class MasterSupplierViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Public Overloads Sub Initialize()

            Dim fq = Function()
                         Using db As New SMContext
                             Dim q = From s In db.Suppliers
                                     Order By s.Code
                                     Where s.Deplecated = 0
                                     Select s

                             q.Load()
                             Return q.ToList()
                         End Using
                     End Function

            MyBase.Initialize(GetType(MasterSupplier), fq)
            MyBase.DataUpdate()
        End Sub

    End Class
End Namespace