﻿Imports System.IO
Imports System.Drawing
Imports System.Linq
Imports System.Data.Entity
Imports DataModels
Imports AppCommon
Imports AppCommon.Models
Imports Livet.Commands

Namespace ViewModels
    Public Class MasterMaterialViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        Public Overloads Sub Initialize()
            Condition = New ScheduleConditionModel(GetType(SMContext))
            RaisePropertyChanged("Condition")

            'グリッドデータの初期化定義
            MyBase.Initialize(GetType(MasterMaterial), ConditionLambda)

            Using db As New SMContext
                Dim sq = From s In db.Suppliers
                         Order By s.Code
                         Select s

                SupplierItems = sq.ToList()
            End Using

            'グリッドデータ初期表示
            MyBase.DataUpdate()
        End Sub


#Region "Condition変更通知プロパティ"
        Private _Condition As ScheduleConditionModel

        Public Property Condition() As ScheduleConditionModel
            Get
                Return _Condition
            End Get
            Set(ByVal value As ScheduleConditionModel)
                If (_Condition Is value) Then Return
                _Condition = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ListSearchCommand"
        Private _ListSearchCommand As ListenerCommand(Of String)

        Public ReadOnly Property ListSearchCommand() As ListenerCommand(Of String)
            Get
                If _ListSearchCommand Is Nothing Then
                    _ListSearchCommand = New ListenerCommand(Of String)(AddressOf ListSearch, AddressOf CanListSearch)
                End If
                Return _ListSearchCommand
            End Get
        End Property

        Private Function CanListSearch() As Boolean
            Return True
        End Function

        Private Sub ListSearch(ByVal parameter As String)
            MyBase.Initialize(GetType(MasterMaterial), ConditionLambda)
            MyBase.DataUpdate()
        End Sub

        ' 一覧検索クエリ
        Private Function ConditionLambda() As Func(Of IList)
            Return Function()
                       Using db As New SMContext
                           Dim q = db.Materials.Where(Function(o) o.Deplecated = 0)

                           ' 原材料条件
                           If _Condition.MasterWarehouse IsNot Nothing Then
                               q = q.Where(Function(o) o.WarehouseCd = _Condition.MasterWarehouse)
                           End If
                           If _Condition.MasterRack IsNot Nothing Then
                               q = q.Where(Function(o) o.RackCd = _Condition.MasterRack)
                           End If
                           If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                               q = q.Where(Function(o) o.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
                           End If
                           If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                               q = q.Where(Function(o) o.Name.Contains(_Condition.MasterMaterial.Name))
                           End If
                           If _Condition.Supplier IsNot Nothing Then
                               q = q.Where(Function(o) o.MasterSupplier_Id = _Condition.Supplier.Id)
                           End If

                           Return q.OrderBy(Function(o) New With {
                                                o.WarehouseCd,
                                                o.RackCd,
                                                o.MaterialCd
                                            }).ToList()
                       End Using
                   End Function
        End Function
#End Region

#Region "SupplierItems変更通知プロパティ"
        Private _SupplierItems As IList

        Public Property SupplierItems() As IList
            Get
                Return _SupplierItems
            End Get
            Set(ByVal value As IList)
                If (_SupplierItems Is value) Then Return
                _SupplierItems = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "OpenImageSelectionCommand"
        Private _OpenImageSelectionCommand As ListenerCommand(Of OpeningFileSelectionMessage)

        Public ReadOnly Property OpenImageSelectionCommand() As ListenerCommand(Of OpeningFileSelectionMessage)
            Get
                If _OpenImageSelectionCommand Is Nothing Then
                    _OpenImageSelectionCommand = New ListenerCommand(Of OpeningFileSelectionMessage)(AddressOf OpenImageSelection)
                End If
                Return _OpenImageSelectionCommand
            End Get
        End Property

        Private Sub OpenImageSelection(ByVal parameter As OpeningFileSelectionMessage)
            If parameter Is Nothing OrElse parameter.Response Is Nothing OrElse parameter.Response.First Is Nothing Then
                Exit Sub
            End If
            Dim fileName As String = parameter.Response.First
            If File.Exists(fileName) Then

                Using image As New Bitmap(fileName)

                    Const targetWidth As Double = 500.0
                    Dim ratio As Double = targetWidth / image.Width
                    Dim w As Integer = System.Math.Floor(image.Width * ratio)
                    Dim h As Integer = System.Math.Floor(image.Height * ratio)
                    Dim dstImage As New Bitmap(w, h)
                    Dim g As Graphics = Graphics.FromImage(dstImage)
                    g.DrawImage(image, 0, 0, w, h)

                    Using st As MemoryStream = New MemoryStream()
                        dstImage.Save(st, System.Drawing.Imaging.ImageFormat.Jpeg)
                        st.Seek(0, SeekOrigin.Begin)
                        Using br As New BinaryReader(st)

                            CType(DataItem, MasterMaterial).Image = br.ReadBytes(st.Length)
                        End Using
                    End Using

                    dstImage.Dispose()
                    g.Dispose()
                End Using

                RaisePropertyChanged("DataItem")
            End If
        End Sub
#End Region

    End Class
End Namespace