﻿Imports System.Linq
Imports System.Data.Entity
Imports AppCommon
Imports AppCommon.Models
Imports DataModels

Namespace ViewModels
    Public Class ListExpirationViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        'DBコンテキスト。原材料マスタ等マスタ項目の遅延評価が出来るようにする為に保持したままにする。
        Private db As New SMContext

        Public Overloads Sub initialize()
            Condition = New StockConditionModel(GetType(SMContext))
            Condition.ExpirationDate = DateTime.Today
            RaisePropertyChanged("Condition")

            MyBase.Initialize(GetType(Stock), ConditionLambda)

            'グリッドデータ初期表示
            MyBase.DataUpdate()
        End Sub

#Region "Condition変更通知プロパティ"
        Private _Condition As StockConditionModel

        Public Property Condition() As StockConditionModel
            Get
                Return _Condition
            End Get
            Set(ByVal value As StockConditionModel)
                If (_Condition Is value) Then Return
                _Condition = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ListSearchCommand"
        Private _ListSearchCommand As ViewModelCommand

        Public ReadOnly Property ListSearchCommand() As ViewModelCommand
            Get
                If _ListSearchCommand Is Nothing Then
                    _ListSearchCommand = New ViewModelCommand(AddressOf ListSearch, AddressOf CanListSearch)
                End If
                Return _ListSearchCommand
            End Get
        End Property

        Private Function CanListSearch() As Boolean
            Return True
        End Function

        Private Sub ListSearch()
            MyBase.Initialize(GetType(Stock), ConditionLambda)
            MyBase.DataUpdate()
        End Sub

        ' 一覧検索クエリ
        Private Function ConditionLambda() As Func(Of IList)
            Return Function()
                       Dim q = db.Stocks.Where(Function(o) o.Deplecated = 0 AndAlso o.MasterMaterial IsNot Nothing AndAlso o.MasterMaterial.Deplecated = 0)

                       ' 原材料条件
                       If _Condition.MasterWarehouse IsNot Nothing Then
                           q = q.Where(Function(o) o.MasterMaterial.WarehouseCd = _Condition.MasterWarehouse)
                       End If
                       If _Condition.MasterRack IsNot Nothing Then
                           q = q.Where(Function(o) o.MasterMaterial.RackCd = _Condition.MasterRack)
                       End If
                       If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                           q = q.Where(Function(o) o.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
                       End If
                       If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                           q = q.Where(Function(o) o.MasterMaterial.Name.Contains(_Condition.MasterMaterial.Name))
                       End If

                       ' ロット番号単位にサマリ
                       Dim gq = From o In q
                                      Group o By o.MasterMaterial, o.LotNo Into Group
                                      Select New With {
                                          MasterMaterial,
                                          LotNo,
                                          .NumLot = Group.Count(),
                                          .OpenedDate = Group.Min(Function(it) it.OpeningDate),
                                          .ExpirationDate = Group.Min(Function(it) it.ExpirationDate),
                                          .ExpirationDateNotopen = Group.Where(Function(it) it.OpenedFlag = OpenedType.NotOpen).Min(Function(it) it.ExpirationDate),
                                          .ExpirationDateOpened = Group.Where(Function(it) it.OpenedFlag = OpenedType.Opened).Min(Function(it) it.ExpirationDate),
                                          .NotopenCount = Group.Count(Function(it) it.OpenedFlag = OpenedType.NotOpen),
                                          .OpenedCount = Group.Count(Function(it) it.OpenedFlag = OpenedType.Opened),
                                          .ArrivalDate = Group.Min(Function(it) it.ArrivalHistory.ArrivalDate),
                                          .ProductionDate = Group.Min(Function(it) it.ArrivalHistory.ProductionDate)
                                      }

                       ' 在庫条件（サマリ結果に対して適用する）
                       If _Condition.ArrivalDateFrom IsNot Nothing Then
                           gq = gq.Where(Function(o) o.ArrivalDate >= _Condition.ArrivalDateFrom)
                       End If
                       If _Condition.ArrivalDateTo IsNot Nothing Then
                           gq = gq.Where(Function(o) o.ArrivalDate <= _Condition.ArrivalDateTo)
                       End If
                       If _Condition.ProductionDateFrom IsNot Nothing Then
                           gq = gq.Where(Function(o) o.ProductionDate >= _Condition.ProductionDateFrom)
                       End If
                       If _Condition.ProductionDateTo IsNot Nothing Then
                           gq = gq.Where(Function(o) o.ProductionDate <= _Condition.ProductionDateTo)
                       End If
                       If _Condition.OpeningDateFrom IsNot Nothing Then
                           gq = gq.Where(Function(o) o.OpenedDate >= _Condition.OpeningDateFrom)
                       End If
                       If _Condition.OpeningDateTo IsNot Nothing Then
                           gq = gq.Where(Function(o) o.OpenedDate <= _Condition.OpeningDateTo)
                       End If

                       If _Condition.ExpirationDate IsNot Nothing Then
                           gq = gq.Where(Function(o) o.ExpirationDate <= _Condition.ExpirationDate)
                       End If

                       Return gq.OrderBy(Function(o) New With {
                                             o.MasterMaterial.WarehouseCd,
                                             o.MasterMaterial.RackCd,
                                             o.MasterMaterial.MaterialCd,
                                             .ExpirationDate = If(o.ExpirationDate, DateTime.MaxValue),
                                             o.LotNo
                                         }).ToList()

                   End Function
        End Function
#End Region

#Region "CSVCommand"
        Private _CSVCommand As ViewModelCommand

        Public ReadOnly Property CSVCommand() As ViewModelCommand
            Get
                If _CSVCommand Is Nothing Then
                    _CSVCommand = New ViewModelCommand(AddressOf CSV, AddressOf CanCSV)
                End If
                Return _CSVCommand
            End Get
        End Property

        Private Function CanCSV() As Boolean
            Return True
        End Function

        Private Sub CSV()
            Dim csv As New Models.CsvExpirationList
            csv.CsvOut(DataSource)
        End Sub
#End Region

    End Class
End Namespace