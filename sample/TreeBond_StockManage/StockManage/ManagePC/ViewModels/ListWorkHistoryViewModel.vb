﻿Imports AppCommon
Imports AppCommon.Models
Imports DataModels

Namespace ViewModels
    Public Class ListWorkHistoryViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Private db As New SMContext

        Public Overloads Sub Initialize()
            Condition = New StockConditionModel(GetType(SMContext))
            Condition.WorkDateFrom = DateTime.Today
            Condition.WorkDateTo = DateTime.Today
            RaisePropertyChanged("Condition")

            MyBase.Initialize(GetType(WorkItem), ConditionLambda)
            MyBase.DataUpdate()
        End Sub

#Region "Condition変更通知プロパティ"
        Private _Condition As StockConditionModel

        Public Property Condition() As StockConditionModel
            Get
                Return _Condition
            End Get
            Set(ByVal value As StockConditionModel)
                If (_Condition Is value) Then Return
                _Condition = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ListSearchCommand"
        Private _ListSearchCommand As ViewModelCommand

        Public ReadOnly Property ListSearchCommand() As ViewModelCommand
            Get
                If _ListSearchCommand Is Nothing Then
                    _ListSearchCommand = New ViewModelCommand(AddressOf ListSearch, AddressOf CanListSearch)
                End If
                Return _ListSearchCommand
            End Get
        End Property

        Private Function CanListSearch() As Boolean
            Return True
        End Function

        Private Sub ListSearch()
            MyBase.Initialize(GetType(WorkItem), ConditionLambda)
            MyBase.DataUpdate()
        End Sub
#End Region


#Region "CSVCommand"
        Private _CSVCommand As ViewModelCommand

        Public ReadOnly Property CSVCommand() As ViewModelCommand
            Get
                If _CSVCommand Is Nothing Then
                    _CSVCommand = New ViewModelCommand(AddressOf CSV, AddressOf CanCSV)
                End If
                Return _CSVCommand
            End Get
        End Property

        Private Function CanCSV() As Boolean
            Return True
        End Function

        Private Sub CSV()
            Dim csv As New Models.CsvDelayedList
            csv.CsvOut(DataSource)
        End Sub
#End Region

        ' 一覧検索クエリ
        Private Function ConditionLambda() As Func(Of IList)
            Return Function()
                       Dim endOfDay = _Condition.WorkDateTo.Value.AddDays(1)
                       Dim list As New List(Of WorkItem)

                       Using db As New SMContext
                           ' 受入履歴
                           list.AddRange(getArrivalList(db, endOfDay))

                           ' ラベル再発行
                           list.AddRange(getReissueList(db, endOfDay))

                           ' ピッキング
                           list.AddRange(getPickingList(db, endOfDay))
                       End Using

                       Return list.OrderBy(Function(o) o.WorkDate).ToList()
                   End Function
        End Function

        Private Function getArrivalList(db As SMContext, endOfDay As DateTime) As IList(Of WorkItem)
            Dim q = From arr In db.ArrivalHistories
                    Group Join u In db.Users On arr.InsertUser Equals u.Id Into user = FirstOrDefault()
                    Group Join s In db.Stocks On arr.Id Equals s.ArrivalHistory_Id Into stocks = Group
                    Where arr.Deplecated = 0
            q = q.Where(Function(o) o.arr.InsertDate >= _Condition.WorkDateFrom AndAlso o.arr.InsertDate <= endOfDay)

            ' 原材料条件
            If _Condition.MasterWarehouse IsNot Nothing Then
                q = q.Where(Function(o) o.arr.MasterMaterial.WarehouseCd = _Condition.MasterWarehouse)
            End If
            If _Condition.MasterRack IsNot Nothing Then
                q = q.Where(Function(o) o.arr.MasterMaterial.RackCd = _Condition.MasterRack)
            End If
            If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                q = q.Where(Function(o) o.arr.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
            End If
            If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                q = q.Where(Function(o) o.arr.MasterMaterial.Name.Contains(_Condition.MasterMaterial.Name))
            End If
            If _Condition.Supplier IsNot Nothing Then
                q = q.Where(Function(o) o.arr.MasterMaterial.MasterSupplier_Id = _Condition.Supplier.Id)
            End If

            Return q.Select(Function(o) New WorkItem() With {
                                .WorkFlag = WorkItem.WorkType.Arrival,
                                .WorkDate = o.arr.InsertDate,
                                .WorkUser = o.user,
                                .MasterMaterial = o.arr.MasterMaterial,
                                .Stock = o.stocks.FirstOrDefault,
                                .ArrivalHistory = o.arr
                        }).ToList()
        End Function

        Private Function getReissueList(db As SMContext, endOfDay As DateTime) As IList(Of WorkItem)
            Dim q = From reissue In db.LabelReissues
                     Group Join u In db.Users On reissue.InsertUser Equals u.Id Into user = FirstOrDefault()
                     Where reissue.Deplecated = 0
            q = q.Where(Function(o) o.reissue.InsertDate >= _Condition.WorkDateFrom AndAlso o.reissue.InsertDate <= endOfDay)

            ' 原材料条件
            If _Condition.MasterWarehouse IsNot Nothing Then
                q = q.Where(Function(o) o.reissue.Stock.MasterMaterial.WarehouseCd = _Condition.MasterWarehouse)
            End If
            If _Condition.MasterRack IsNot Nothing Then
                q = q.Where(Function(o) o.reissue.Stock.MasterMaterial.RackCd = _Condition.MasterRack)
            End If
            If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                q = q.Where(Function(o) o.reissue.Stock.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
            End If
            If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                q = q.Where(Function(o) o.reissue.Stock.MasterMaterial.Name.Contains(_Condition.MasterMaterial.Name))
            End If
            If _Condition.Supplier IsNot Nothing Then
                q = q.Where(Function(o) o.reissue.Stock.MasterMaterial.MasterSupplier_Id = _Condition.Supplier.Id)
            End If

            Return q.Select(Function(o) New WorkItem() With {
                                        .WorkFlag = WorkItem.WorkType.Reissue,
                                        .WorkDate = o.reissue.InsertDate,
                                        .WorkUser = o.user,
                                        .MasterMaterial = o.reissue.Stock.MasterMaterial,
                                        .Stock = o.reissue.Stock,
                                        .LabelReissue = o.reissue
                                    }).ToList()
        End Function

        Private Function getPickingList(db As SMContext, endOfDay As DateTime) As IList(Of WorkItem)
            Dim q = From picking In db.Pickings
                    Group Join u In db.Users On picking.InsertUser Equals u.Id Into user = FirstOrDefault()
                    Where picking.Deplecated = 0
            q = q.Where(Function(o) o.picking.InsertDate >= _Condition.WorkDateFrom AndAlso o.picking.InsertDate <= endOfDay)

            ' 原材料条件
            If _Condition.MasterWarehouse IsNot Nothing Then
                q = q.Where(Function(o) o.picking.Stock.MasterMaterial.WarehouseCd = _Condition.MasterWarehouse)
            End If
            If _Condition.MasterRack IsNot Nothing Then
                q = q.Where(Function(o) o.picking.Stock.MasterMaterial.RackCd = _Condition.MasterRack)
            End If
            If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                q = q.Where(Function(o) o.picking.Stock.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
            End If
            If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                q = q.Where(Function(o) o.picking.Stock.MasterMaterial.Name.Contains(_Condition.MasterMaterial.Name))
            End If
            If _Condition.Supplier IsNot Nothing Then
                q = q.Where(Function(o) o.picking.Stock.MasterMaterial.MasterSupplier_Id = _Condition.Supplier.Id)
            End If

            Return q.Select(Function(o) New WorkItem() With {
                                        .WorkFlag = WorkItem.WorkType.Picking,
                                        .WorkDate = o.picking.InsertDate,
                                        .WorkUser = o.user,
                                        .MasterMaterial = o.picking.Stock.MasterMaterial,
                                        .Stock = o.picking.Stock,
                                        .Picking = o.picking
                                    }).ToList()
        End Function

    End Class
End Namespace