﻿Imports System.IO
Imports System.Data.Entity
Imports System.Configuration
Imports DataModels
Imports AppCommon
Imports AppCommon.Models

Namespace ViewModels
    Public Class ImportScheduleViewModel
        Inherits ViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Public Sub Initialize()
        End Sub

#Region "Message変更通知プロパティ"
        Private _Message As String

        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                If (_Message = value) Then Return
                _Message = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ImportCsv変更通知プロパティ"
        Private _ImportCsv As ImportScheduleModel

        Public Property ImportCsv() As ImportScheduleModel
            Get
                Return _ImportCsv
            End Get
            Set(ByVal value As ImportScheduleModel)
                If (_ImportCsv Is value) Then Return
                _ImportCsv = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ImportItems変更通知プロパティ"
        Private _ImportItems As IList(Of ImportScheduleModel.Item)

        Public Property ImportItems() As IList(Of ImportScheduleModel.Item)
            Get
                Return _ImportItems
            End Get
            Set(ByVal value As IList(Of ImportScheduleModel.Item))
                If (_ImportItems Is value) Then Return
                _ImportItems = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "OpenCsvSelectionCommand"
        Private _OpenCsvSelectionCommand As ListenerCommand(Of OpeningFileSelectionMessage)

        Public ReadOnly Property OpenCsvSelectionCommand() As ListenerCommand(Of OpeningFileSelectionMessage)
            Get
                If _OpenCsvSelectionCommand Is Nothing Then
                    _OpenCsvSelectionCommand = New ListenerCommand(Of OpeningFileSelectionMessage)(AddressOf OpenCsvSelection)
                End If
                Return _OpenCsvSelectionCommand
            End Get
        End Property

        Private Sub OpenCsvSelection(ByVal parameter As OpeningFileSelectionMessage)
            If parameter Is Nothing OrElse parameter.Response Is Nothing OrElse parameter.Response.First Is Nothing Then
                Exit Sub
            End If
            Dim fileName As String = parameter.Response.First

            ImportCsv = New ImportScheduleModel(fileName)
            ImportItems = ImportCsv.GetData

            _UpdateItemsCommand.RaiseCanExecuteChanged()
            RaisePropertyChanged("ImportCsv")
            RaisePropertyChanged("ImportItems")
            RaisePropertyChanged("Message")
        End Sub
#End Region

#Region "UpdateItemsCommand"
        Private _UpdateItemsCommand As ViewModelCommand

        Public ReadOnly Property UpdateItemsCommand() As ViewModelCommand
            Get
                If _UpdateItemsCommand Is Nothing Then
                    _UpdateItemsCommand = New ViewModelCommand(AddressOf UpdateItems, AddressOf CanUpdateItems)
                End If
                Return _UpdateItemsCommand
            End Get
        End Property

        Private Function CanUpdateItems() As Boolean
            Return If(ImportCsv Is Nothing, False, ImportCsv.IsValid)
        End Function

        Private Sub UpdateItems()
            ImportScheduleModel.UpdateScheduleFromItems(ImportItems)

            MsgBox("受取予定を取り込みました")
            ImportCsv = Nothing
            ImportItems = Nothing
        End Sub
#End Region

        Public Property CsvDefaultDir As String = ConfigurationManager.AppSettings.Get("CsvDir")

    End Class
End Namespace