﻿Imports System.Linq
Imports System.Data.Entity
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Configuration
Imports System.Threading.Tasks
Imports AppCommon
Imports DataModels

Namespace ViewModels
    Public Class LabelPrintViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        'DBコンテキスト。原材料マスタ等マスタ項目の遅延評価が出来るようにする為に保持したままにする。
        Private db As New SMContext

        Public Overloads Sub Initialize()
            MyBase.Initialize(GetType(Stock), ConditionLambda)
            MyBase.DataUpdate()
        End Sub

        Private Function ConditionLambda() As Func(Of IList)
            Return Function()
                       db.Dispose()
                       db = Nothing
                       db = New SMContext

                       Dim q = db.Stocks.Where(
                           Function(o) o.Deplecated = 0 AndAlso
                               o.ArrivalHistory.Status = LabelPrintStatusType.NotYet AndAlso
                               o.ArrivalHistory.Deplecated = 0)

                       ' ロット番号単位にサマリ
                       Dim gq = From o In q
                                Group o By o.MasterMaterial, o.ArrivalHistory, o.LotNo Into Group
                                Select New LabelPrintItem With {
                                    .MasterMaterial = MasterMaterial,
                                    .ArrivalHistory = ArrivalHistory,
                                    .LotNo = LotNo,
                                    .NumLot = Group.Count(),
                                    .ExpirationDate = Group.Min(Function(it) it.ExpirationDate)
                                }

                       Return gq.OrderBy(Function(o) New With {
                                             o.MasterMaterial.WarehouseCd,
                                             o.MasterMaterial.RackCd,
                                             o.MasterMaterial.MaterialCd,
                                             .ExpirationDate = If(o.ExpirationDate, DateTime.MaxValue),
                                             o.LotNo
                                         }).ToList()

                   End Function
        End Function


#Region "Message変更通知プロパティ"
        Private _Message As String

        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                If (_Message = value) Then Return
                _Message = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "PrintCommand"
        Private _PrintCommand As ViewModelCommand

        Public ReadOnly Property PrintCommand() As ViewModelCommand
            Get
                If _PrintCommand Is Nothing Then
                    _PrintCommand = New ViewModelCommand(AddressOf Print, AddressOf CanPrint)
                End If
                Return _PrintCommand
            End Get
        End Property

        Private Function CanPrint() As Boolean
            Return True
        End Function

        Private Sub Print()
            Message = ""

            Dim dataitems As IQueryable(Of LabelPrintItem) = DataSource.AsQueryable
            Dim checked = dataitems.Where(Function(o) o.Check = True)

            For Each o As LabelPrintItem In checked

                '対応する在庫データ取得
                Dim stocks = db.Stocks.Where(
                    Function(it) it.ArrivalHistory_Id = o.ArrivalHistory.Id AndAlso it.Deplecated = 0).ToList()

                Dim isLast = DataSource.IndexOf(o) = DataSource.IndexOf(checked.Last)

                'ラベル印刷
                If LabelPrint(stocks, isLast) Then
                    o.ArrivalHistory.Status = LabelPrintStatusType.Printed
                    db.SaveChanges()
                Else
                    Exit Sub
                End If
            Next

            Message = "ラベルを発行しました。"
            MyBase.Initialize(GetType(Stock), ConditionLambda)
            MyBase.DataUpdate()
        End Sub

        Private Function LabelPrint(stocks As IList(Of Stock), isLast As Boolean) As Boolean
            Dim client As New HttpClient
            Dim uri As String = ConfigurationManager.AppSettings.Get("baseUri") & "LabelPrint"

            For Each s In stocks

                ' 最後のラベルを判定（印刷時の切断制御）
                If isLast AndAlso stocks.IndexOf(s) = stocks.IndexOf(stocks.Last) Then
                    s.IsLastLabel = True
                End If

                Dim res As Task(Of HttpResponseMessage) = client.PostAsJsonAsync(uri, s)
                If Not res.Result.IsSuccessStatusCode Then
                    Dim c = res.Result.Content
                    Dim p = c.ReadAsStringAsync()
                    Message = "ラベル印刷ができませんでした。 :" & p.Result

                    Return False
                End If
            Next

            Return True
        End Function
#End Region

        Public Class LabelPrintItem
            Public Property Check As Boolean = True
            Public Property MasterMaterial As MasterMaterial
            Public Property ArrivalHistory As ArrivalHistory
            Public Property LotNo As String
            Public Property NumLot As Integer
            Public Property ExpirationDate As DateTime?
        End Class

    End Class
End Namespace