﻿Imports System.Linq
Imports System.Data.Entity
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Configuration
Imports AppCommon
Imports AppCommon.Models
Imports DataModels
Imports System.Threading.Tasks

Namespace ViewModels
    Public Class LabelReissueViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        'DBコンテキスト。原材料マスタ等マスタ項目の遅延評価が出来るようにする為に保持したままにする。
        Private db As New SMContext

        Public Overloads Sub initialize()
            MyBase.Initialize(GetType(LabelReissue), ConditionLambda)
            MyBase.DataUpdate()
        End Sub

        Private Function ConditionLambda() As Func(Of IList)
            Return Function()
                       db.Dispose()
                       db = Nothing
                       db = New SMContext

                       Dim q = db.LabelReissues.Where(
                           Function(o) o.Deplecated = 0 AndAlso
                               o.Status = LabelPrintStatusType.NotYet).OrderBy(
                               Function(o) New With {
                                   .WarehouseCd = o.Stock.MasterMaterial.WarehouseCd,
                                   .RackCd = o.Stock.MasterMaterial.RackCd,
                                   .MaterialCd = o.Stock.MasterMaterial.MaterialCd,
                                   .ExpirationDate = If(o.Stock.ExpirationDate, DateTime.MaxValue),
                                   .LotNo = o.Stock.LotNo,
                                   .LotBranchNo = o.Stock.LotBranchNo
                               })

                       Return q.ToList()
                   End Function
        End Function

        Protected Overrides Sub Update()
            Dim o As LabelReissue = CType(DataItem, LabelReissue)
            o.ErrorMessage = ""

            ' 未開封（半端原料でない）ときは開封日の設定をクリアする
            If o.ModifyOpenedFlag = OpenedType.NotOpen Then
                o.ModifyOpeningDate = Nothing
            End If

            ' 開封済み（半端原料）のとき、開封日が設定されていないとエラーにする
            If o.ModifyOpenedFlag = OpenedType.Opened AndAlso Not o.ModifyOpeningDate.HasValue Then
                o.ErrorMessage = "半端原料の場合、開封日の設定が必要です。"
                RaisePropertyChanged("DataItem")
                Exit Sub
            End If

            MyBase.Update()
        End Sub

#Region "Message変更通知プロパティ"
        Private _Message As String

        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                If (_Message = value) Then Return
                _Message = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ReissueCommand"
        Private _ReissueCommand As ViewModelCommand

        Public ReadOnly Property ReissueCommand() As ViewModelCommand
            Get
                If _ReissueCommand Is Nothing Then
                    _ReissueCommand = New ViewModelCommand(AddressOf Reissue, AddressOf CanReissue)
                End If
                Return _ReissueCommand
            End Get
        End Property

        Private Function CanReissue() As Boolean
            Return True
        End Function

        Private Sub Reissue()
            Message = ""

            '同一ロットに対する更新のチェック
            If CheckStockOverlap() Then
                Exit Sub
            End If

            Dim dataitems As IQueryable(Of LabelReissue) = DataSource.AsQueryable
            Dim checked = dataitems.Where(Function(o) o.Check = True)

            For Each o As LabelReissue In checked

                '在庫更新
                Dim stock As Stock = o.Stock
                stock.OpeningDate = o.ModifyOpeningDate
                stock.OpenedFlag = o.ModifyOpenedFlag
                stock.ExtendedFlag = o.ModifyExtendedFlag
                stock.ExpirationDate = o.ModifyExpirationDate
                stock.ReprintFlag = ReprintType.Reprinted
                db.Entry(stock).State = EntityState.Modified

                ' 最後のラベルを判定（印刷時の切断制御）
                If DataSource.IndexOf(o) = DataSource.IndexOf(checked.Last) Then
                    stock.IsLastLabel = True
                End If

                'ラベル印刷
                If Not LabelPrint(stock) Then
                    db.Entry(stock).State = EntityState.Unchanged
                    Continue For
                End If

                '再発行データ更新
                o.Status = LabelPrintStatusType.Printed
                db.Entry(o).State = EntityState.Modified
            Next

            db.SaveChanges()

            If Message Is Nothing OrElse Message.Length = 0 Then
                Message = "ラベルを再発行し、在庫を更新しました。"
            End If
            MyBase.Initialize(GetType(LabelReissue), ConditionLambda)
            MyBase.DataUpdate()
        End Sub
#End Region

        Private Function CheckStockOverlap() As Boolean
            Dim LabelReissues As IList(Of LabelReissue) = DataSource
            Dim q = LabelReissues.GroupBy(
                Function(o) o.Stock,
                Function(key, e) New With {
                    .Key = key,
                    .Count = e.Count()
                }).Where(Function(o) o.Count > 1)

            If q.Any() Then
                Dim stock = q.First().Key
                Message = "一つのロットに対する再発行要求が複数あります。重複している再発行要求を削除してください。" &
                    ":" & stock.MasterMaterial.MaterialCd &
                    "  " & stock.MasterMaterial.Name &
                    "  " & stock.LotNo &
                    "  " & stock.LotBranchNo

                Return True
            End If

            Return False
        End Function

        Private Function LabelPrint(stock As Stock) As Boolean
            Dim client As New HttpClient
            Dim uri As String = ConfigurationManager.AppSettings.Get("baseUri") & "LabelPrint"

            Dim res As Task(Of HttpResponseMessage) = client.PostAsJsonAsync(uri, stock)
            If res.Result.IsSuccessStatusCode Then
                Return True
            Else
                Dim c = res.Result.Content
                Dim p = c.ReadAsStringAsync()

                Message = "ラベル印刷ができませんでした。 :" & p.Result
                Return False
            End If

        End Function

    End Class
End Namespace