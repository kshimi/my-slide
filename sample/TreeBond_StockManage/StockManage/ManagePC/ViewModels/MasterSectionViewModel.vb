﻿Imports System.Linq
Imports System.Data.Entity
Imports DataModels
Imports AppCommon
Imports Livet.Commands

Namespace ViewModels
    Public Class MasterSectionViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        Public Overloads Sub Initialize()

            Dim fq = Function()
                         Using db As New SMContext
                             Dim q = From s In db.Sections
                                     Order By s.Name
                                     Where s.Deplecated = 0
                                     Select s

                             q.Load()
                             Return q.ToList()
                         End Using
                     End Function

            MyBase.Initialize(GetType(MasterSection), fq)

            MyBase.DataUpdate()
        End Sub

    End Class
End Namespace