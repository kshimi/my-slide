﻿Imports System.Linq
Imports System.Data.Entity
Imports AppCommon
Imports AppCommon.Models
Imports DataModels
Imports Livet
Imports Livet.Commands

Namespace ViewModels
    Public Class StockViewModel
        Inherits AppCommon.ViewModels.DataGridViewModel

        '  lvcom   : ViewModelCommand
        '  lvcomn  : ViewModelCommand(CanExecute無)
        '  llcom   : ListenerCommand(パラメータ有のコマンド)
        '  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
        '  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)

        'DBコンテキスト。原材料マスタ等マスタ項目の遅延評価が出来るようにする為に保持したままにする。
        Private db As New SMContext

        Public Overloads Sub Initialize()
            Condition = New StockConditionModel(GetType(SMContext))
            RaisePropertyChanged("Condition")
        End Sub

#Region "Condition変更通知プロパティ"
        Private _Condition As StockConditionModel

        Public Property Condition() As StockConditionModel
            Get
                Return _Condition
            End Get
            Set(ByVal value As StockConditionModel)
                If (_Condition Is value) Then Return
                _Condition = value
                RaisePropertyChanged()
            End Set
        End Property
#End Region

#Region "ListSearchCommand"
        Private _ListSearchCommand As ViewModelCommand

        Public ReadOnly Property ListSearchCommand() As ViewModelCommand
            Get
                If _ListSearchCommand Is Nothing Then
                    _ListSearchCommand = New ViewModelCommand(AddressOf ListSearch, AddressOf CanListSearch)
                End If
                Return _ListSearchCommand
            End Get
        End Property

        Private Function CanListSearch() As Boolean
            Return True
        End Function

        Private Sub ListSearch()
            MyBase.Initialize(GetType(Stock), ConditionLambda)
            MyBase.DataUpdate()
        End Sub

        ' 一覧検索クエリ
        Private Function ConditionLambda() As Func(Of IList)
            Return Function()
                       db.Dispose()
                       db = Nothing
                       db = New SMContext

                       Dim q = db.Stocks.Where(Function(o) o.Deplecated = 0).OrderBy(Function(o) New With {
                                                                                         o.MasterMaterial.WarehouseCd,
                                                                                         o.MasterMaterial.RackCd,
                                                                                         o.MasterMaterial.MaterialCd,
                                                                                         .ExpirationDate = If(o.ExpirationDate, DateTime.MaxValue),
                                                                                         o.LotNo,
                                                                                         o.LotBranchNo
                                                                                     })

                       ' 原材料条件
                       If _Condition.MasterWarehouse IsNot Nothing Then
                           q = q.Where(Function(o) o.MasterMaterial.WarehouseCd = _Condition.MasterWarehouse)
                       End If
                       If _Condition.MasterRack IsNot Nothing Then
                           q = q.Where(Function(o) o.MasterMaterial.RackCd = _Condition.MasterRack)
                       End If
                       If _Condition.MasterMaterial.MaterialCd IsNot Nothing AndAlso _Condition.MasterMaterial.MaterialCd.Length > 0 Then
                           q = q.Where(Function(o) o.MasterMaterial.MaterialCd.Contains(_Condition.MasterMaterial.MaterialCd))
                       End If
                       If _Condition.MasterMaterial.Name IsNot Nothing AndAlso _Condition.MasterMaterial.Name.Length > 0 Then
                           q = q.Where(Function(o) o.MasterMaterial.Name.Contains(_Condition.MasterMaterial.Name))
                       End If

                       ' ロット条件
                       If _Condition.LotNo IsNot Nothing Then
                           q = q.Where(Function(o) o.LotNo = _Condition.LotNo)
                       End If
                       If _Condition.LotBranchNo IsNot Nothing Then
                           q = q.Where(Function(o) o.LotBranchNo = _Condition.LotBranchNo)
                       End If

                       If _Condition.TakingOut Then
                           q = q.Where(Function(o) o.StockFlag = StockType.TakingOut)
                       Else
                           q = q.Where(Function(o) o.StockFlag <> StockType.TakingOut)
                       End If

                       Return q.ToList()
                   End Function
        End Function
#End Region

    End Class
End Namespace