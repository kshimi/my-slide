﻿Namespace Views
    ' ViewModelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedWeakEventListenerや
    ' CollectionChangedWeakEventListenerを使うと便利です。独自イベントの場合はLivetWeakEventListenerが使用できます。
    ' クローズ時などに、LivetCompositeDisposableに格納した各種イベントリスナをDisposeする事でイベントハンドラの開放が容易に行えます。
    '
    ' WeakEventListenerなので明示的に開放せずともメモリリークは起こしませんが、できる限り明示的に開放するようにしましょう。
    '
    Class MenuPage

        Private Sub StockMaintenance_Click(sender As Object, e As RoutedEventArgs) Handles StockMaintenanceButton.Click
            Dim page As New Uri("Views/StockPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub LabelPrintButton_Click(sender As Object, e As RoutedEventArgs) Handles LabelPrintButton.Click
            Dim page As New Uri("Views/LabelPrintPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub LabelReissueButton_Click(sender As Object, e As RoutedEventArgs) Handles LabelReissueButton.Click
            Dim page As New Uri("Views/LabelReissuePage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub ListExpirationButton_Click(sender As Object, e As RoutedEventArgs) Handles ListExpirationButton.Click
            Dim page As New Uri("Views/ListExpirationPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub ListDelayedButton_Click(sender As Object, e As RoutedEventArgs) Handles ListDelayedButton.Click
            Dim page As New Uri("Views/ListDelayedPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub ImportScheduleButton_Click(sender As Object, e As RoutedEventArgs) Handles ImportScheduleButton.Click
            Dim page As New Uri("Views/ImportSchedulePage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub MasterMenuButton_Click(sender As Object, e As RoutedEventArgs) Handles MasterMenuButton.Click
            Dim page As New Uri("Views/MasterMenuPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub ListWorkHistoryButton_Click(sender As Object, e As RoutedEventArgs) Handles ListWorkHistoryButton.Click
            Dim page As New Uri("Views/ListWorkHistoryPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub
    End Class
End Namespace
