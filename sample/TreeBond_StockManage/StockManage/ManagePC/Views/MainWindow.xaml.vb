﻿Imports AppCommon.Models

Namespace Views
    Class MainWindow

        Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
            LoginModel.DisposeLoginUser()

            Dim o As New Views.LoginPage
            MainFrame.NavigationService.Navigate(o)
        End Sub
    End Class
End Namespace