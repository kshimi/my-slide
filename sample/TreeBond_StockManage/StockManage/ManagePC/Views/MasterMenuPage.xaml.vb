﻿Namespace Views
    ' ViewModelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedWeakEventListenerや
    ' CollectionChangedWeakEventListenerを使うと便利です。独自イベントの場合はLivetWeakEventListenerが使用できます。
    ' クローズ時などに、LivetCompositeDisposableに格納した各種イベントリスナをDisposeする事でイベントハンドラの開放が容易に行えます。
    '
    ' WeakEventListenerなので明示的に開放せずともメモリリークは起こしませんが、できる限り明示的に開放するようにしましょう。
    '
    Class MasterMenuPage

        Private Sub UserMaster_Click(sender As Object, e As RoutedEventArgs) Handles UserMaster.Click
            Dim page As New Uri("Views/MasterUserPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub SectionMaster_Click(sender As Object, e As RoutedEventArgs) Handles SectionMaster.Click
            Dim page As New Uri("Views/MasterSectionPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub SupplierMaster_Click(sender As Object, e As RoutedEventArgs) Handles SupplierMaster.Click
            Dim page As New Uri("Views/MasterSupplierPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub MaterialMaster_Click(sender As Object, e As RoutedEventArgs) Handles MaterialMaster.Click
            Dim page As New Uri("Views/MasterMaterialPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

        Private Sub TurnBack_Click(sender As Object, e As RoutedEventArgs) Handles TurnBack.Click
            Dim page As New Uri("Views/MenuPage.xaml", UriKind.Relative)
            NavigationService.Source = page
        End Sub

    End Class

End Namespace