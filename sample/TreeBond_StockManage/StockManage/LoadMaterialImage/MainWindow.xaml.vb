﻿Imports System.IO
Imports System.Drawing
Imports System.Data.Entity
Imports Microsoft.Win32
Imports DataModels

Class MainWindow

    Private Sub bSelectDir_Click(sender As Object, e As RoutedEventArgs) Handles bSelectDir.Click
        Dim dlg As New OpenFileDialog
        dlg.ShowDialog()
        ImageFileDirectory.Content = dlg.FileName
    End Sub

    Private Sub bLoadImage_Click(sender As Object, e As RoutedEventArgs) Handles bLoadImage.Click
        Dim file As New FileInfo(ImageFileDirectory.Content)
        For Each f In file.Directory.EnumerateFiles
            Dim fname = f.Name
            Dim mname = fname.Split(" ").First

            Using db As New SMContext
                Dim m = db.Materials.Where(Function(o) o.Deplecated = 0 AndAlso o.MaterialCd = mname)
                If Not m.Any() Then
                    Continue For
                End If

                Dim material As MasterMaterial = m.First
                Using image As New Bitmap(f.FullName)

                    Const targetWidth As Double = 500.0
                    Dim ratio As Double = targetWidth / image.Width
                    Dim w As Integer = System.Math.Floor(image.Width * ratio)
                    Dim h As Integer = System.Math.Floor(image.Height * ratio)
                    Dim dstImage As New Bitmap(w, h)
                    Dim g As Graphics = Graphics.FromImage(dstImage)
                    g.DrawImage(image, 0, 0, w, h)

                    Using st As MemoryStream = New MemoryStream()
                        dstImage.Save(st, System.Drawing.Imaging.ImageFormat.Jpeg)
                        st.Seek(0, SeekOrigin.Begin)
                        Using br As New BinaryReader(st)
                            material.Image = br.ReadBytes(st.Length)
                        End Using
                    End Using

                    Message.Text += vbCrLf & f.Name

                    dstImage.Dispose()
                    g.Dispose()
                    db.SaveChanges()
                End Using
            End Using
        Next

        MsgBox("loaded")
    End Sub
End Class
