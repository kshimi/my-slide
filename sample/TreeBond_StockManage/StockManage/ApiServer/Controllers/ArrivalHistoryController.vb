﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Linq
Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports System.Web.Http.Description
Imports DataModels

Public Class ArrivalHistoryController
    Inherits ApiController

    Private db As New SMContext

    <Route("api/ArrivalHistoriesByRevision/{revision}")>
    Function GetArrivalHistorysByRevision(ByVal revision As Integer) As IEnumerable(Of ArrivalHistory)
        Dim q = From o In db.ArrivalHistories
                Where o.Revision > revision
                Order By o.Revision
                Select o

        Return q.Take(1000).ToArray()
    End Function

    ' GET: api/ArrivalHistories
    Function GetArrivalHistories() As IEnumerable(Of ArrivalHistory)
        Return db.ArrivalHistories.ToArray()
    End Function

    ' GET: api/ArrivalHistoryNewest
    <Route("api/ArrivalHistoryNewest")>
    <ResponseType(GetType(ArrivalHistory))>
    Function GetArrivalHistoryNewest() As IHttpActionResult
        If db.ArrivalHistories.Count > 0 Then
            Return Ok(db.ArrivalHistories.OrderByDescending(Function(u) u.Revision).First)
        Else
            Return NotFound()
        End If
    End Function

    ' GET api/ArrivalHistorys/5
    <ResponseType(GetType(ArrivalHistory))>
    Function GetValue(ByVal id As Integer) As IHttpActionResult
        Dim arrivalHistory As ArrivalHistory = db.ArrivalHistories.Find(id)
        If IsNothing(arrivalHistory) Then
            Return NotFound()
        End If

        Return Ok(arrivalHistory)
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If (disposing) Then
            db.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
End Class
