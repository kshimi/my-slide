﻿Imports System.Net
Imports System.Web.Http
Imports System.Web.Http.Description
Imports DataModels

Public Class MasterSupplierController
    Inherits ApiController

    Private db As New SMContext

    <Route("api/MasterSuppliersByRevision/{revision}")>
    Function GetSuppliersByRevision(ByVal revision As Integer) As IEnumerable(Of MasterSupplier)
        Dim q = From s In db.Suppliers
                Where s.Revision > revision
                Order By s.Revision
                Select s

        Return q.Take(1000).ToArray()
    End Function

    ' GET api/MasterSuppliers
    Public Function GetValues() As IEnumerable(Of MasterSupplier)
        Return db.Suppliers.ToArray()
    End Function

    ' GET api/MasterSupplierNewest
    <Route("api/MasterSupplierNewest")>
    <ResponseType(GetType(MasterSupplier))>
    Public Function GetMasterSupplierNewest() As IHttpActionResult
        If db.Suppliers.Count > 0 Then
            Return Ok(db.Suppliers.OrderByDescending(Function(s) s.Revision).First)
        Else
            Return NotFound()
        End If
    End Function

    ' GET api/MasterSuppliers/5
    <ResponseType(GetType(MasterSupplier))>
    Public Function GetValue(ByVal id As Integer) As IHttpActionResult
        Dim masterSupplier As MasterSupplier = db.Suppliers.Find(id)
        If IsNothing(masterSupplier) Then
            Return NotFound()
        End If

        Return Ok(masterSupplier)
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If (disposing) Then
            db.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private Function MasterSupplierExists(ByVal id As String) As Boolean
        Return db.Suppliers.Count(Function(e) e.Id = id) > 0
    End Function
End Class
