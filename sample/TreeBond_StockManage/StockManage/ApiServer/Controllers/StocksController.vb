﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Linq
Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports System.Web.Http.Description
Imports DataModels

Namespace Controllers
    Public Class StocksController
        Inherits ApiController

        Private db As New SMContext

        <Route("api/StocksByRevision/{revision}")>
        Function GetStocksByRevision(ByVal revision As Integer) As IEnumerable(Of Stock)
            Dim q = From o In db.Stocks
                    Where o.Revision > revision
                    Order By o.Revision
                    Select o

            Return q.Take(1000).ToArray()
        End Function

        ' GET: api/Stocks
        Function GetStocks() As IEnumerable(Of Stock)
            Return db.Stocks.ToArray()
        End Function

        ' GET: api/StockNewest
        <Route("api/StockNewest")>
        <ResponseType(GetType(Stock))>
        Function GetStockNewest() As IHttpActionResult
            If db.Stocks.Count > 0 Then
                Return Ok(db.Stocks.OrderByDescending(Function(u) u.Revision).First)
            Else
                Return NotFound()
            End If
        End Function

        ' GET api/Stocks/5
        <ResponseType(GetType(Stock))>
        Function GetStock(ByVal id As Integer) As IHttpActionResult
            Dim stock As Stock = db.Stocks.Find(id)
            If IsNothing(stock) Then
                Return NotFound()
            End If

            Return Ok(stock)
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace