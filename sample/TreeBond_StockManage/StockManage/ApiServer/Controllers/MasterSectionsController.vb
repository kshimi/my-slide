﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Linq
Imports System.Net
Imports System.Web.Http
Imports System.Web.Http.Description
Imports DataModels

Namespace Controllers
    Public Class MasterSectionsController
        Inherits ApiController

        Private db As New SMContext

        <Route("api/MasterSectionsByRevision/{revision}")>
        Function GetSectionsByRevision(ByVal revision As Integer) As IEnumerable(Of MasterSection)
            Dim q = From u In db.Sections
                    Where u.Revision > revision
                    Order By u.Revision
                    Select u

            Return q.Take(1000).ToArray()
        End Function

        ' GET: api/MasterSections
        Function GetSections() As IEnumerable(Of MasterSection)
            Return db.Sections.ToArray()
        End Function

        ' GET: api/MasterSectionNewest
        <Route("api/MasterSectionNewest")>
        <ResponseType(GetType(MasterSection))>
        Function GetMasterSectionNewest() As IHttpActionResult
            If db.Sections.Count > 0 Then
                Return Ok(db.Sections.OrderByDescending(Function(u) u.Revision).First)
            Else
                Return NotFound()
            End If
        End Function

        ' GET: api/MasterSections/5
        <ResponseType(GetType(MasterSection))>
        Function GetMasterSection(ByVal id As String) As IHttpActionResult
            Dim masterSection As MasterSection = db.Sections.Find(id)
            If IsNothing(masterSection) Then
                Return NotFound()
            End If

            Return Ok(masterSection)
        End Function


        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Private Function MasterSectionExists(ByVal id As String) As Boolean
            Return db.Sections.Count(Function(e) e.Id = id) > 0
        End Function
    End Class
End Namespace