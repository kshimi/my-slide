﻿Imports System.Net
Imports System.Web.Http
Imports DataModels
Imports AppCommon

Namespace Controllers
    Public Class PickingController
        Inherits ApiController

        ' POST: api/Picking
        Public Function PostValue(<FromBody()> ByVal value As Picking) As IHttpActionResult

            Using db As New SMContext

                ' 在庫検索
                Dim stock = db.Stocks.Find(value.Stock_Id)

                ' ピッキング履歴データの登録
                Dim picking = New Picking
                value.ShallowCopy(picking)
                picking.Id = Nothing
                picking.Stock = Nothing
                db.Pickings.Add(picking)

                ' 在庫状態確認と状態変更
                If stock Is Nothing Then
                    Dim res As New Results.BadRequestErrorMessageResult("stock is empty", Me)
                    Return res
                End If

                If value.PickingFlag = PickingType.TakingOut AndAlso stock.StockFlag = StockType.TakingOut Then
                    Dim res As New Results.BadRequestErrorMessageResult("stock is already taking out", Me)
                    Return res
                End If

                Select Case value.PickingFlag
                    Case PickingType.TakingOut
                        stock.StockFlag = StockType.TakingOut
                    Case PickingType.ReturningInOpen
                        stock.OpenedFlag = OpenedType.Opened
                        stock.OpeningDate = Date.Today()
                        stock.StockFlag = StockType.Stock
                    Case PickingType.ReturningInNotopen
                        stock.StockFlag = StockType.Stock
                End Select
                stock.UpdateDate = Now
                stock.UpdateUser = value.InsertUser

                db.SaveChanges()
            End Using
            Return Ok()
        End Function

    End Class
End Namespace