﻿Imports System.Net
Imports System.Web.Http
Imports DataModels

Namespace Controllers
    Public Class LabelPrintController
        Inherits ApiController

        ' POST: api/LabelPrint
        Public Function PostValue(<FromBody()> ByVal value As Stock) As IHttpActionResult

            Try
                If value IsNot Nothing Then
                    Models.LabelPrintModel.PrintLabel(value)
                End If

                Return Ok()
            Catch ex As Exception
                Dim mesg = ex.Message + " : " + ex.StackTrace
                Return New Results.BadRequestErrorMessageResult(mesg, Me)
            End Try
        End Function

    End Class
End Namespace