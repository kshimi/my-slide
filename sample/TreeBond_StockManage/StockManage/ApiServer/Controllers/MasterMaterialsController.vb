﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Linq
Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports System.Web.Http.Description
Imports DataModels

Namespace Controllers
    Public Class MasterMaterialsController
        Inherits System.Web.Http.ApiController

        Private db As New SMContext

        <Route("api/MasterMaterialsByRevision/{revision}")>
        Function GetMaterialsByRevision(ByVal revision As Integer) As IEnumerable(Of MasterMaterial)
            Dim q = From o In db.Materials
                    Where o.Revision > revision
                    Order By o.Revision
                    Select o

            Return q.Take(1000).ToArray()
        End Function

        ' GET: api/MasterMaterials
        Function GetMaterials() As IEnumerable(Of MasterMaterial)
            Return db.Materials.ToArray()
        End Function

        ' GET: api/MasterMaterialNewest
        <Route("api/MasterMaterialNewest")>
        <ResponseType(GetType(MasterMaterial))>
        Function GetMasterMaterialNewest() As IHttpActionResult
            If db.Materials.Count > 0 Then
                Return Ok(db.Materials.OrderByDescending(Function(u) u.Revision).First)
            Else
                Return NotFound()
            End If
        End Function

        ' GET: api/MasterMaterials/5
        <ResponseType(GetType(MasterMaterial))>
        Function GetMasterMaterial(ByVal id As Integer) As IHttpActionResult
            Dim masterMaterial As MasterMaterial = db.Materials.Find(id)
            If IsNothing(masterMaterial) Then
                Return NotFound()
            End If

            Return Ok(masterMaterial)
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace