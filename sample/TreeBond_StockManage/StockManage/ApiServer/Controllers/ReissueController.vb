﻿Imports System.Net
Imports System.Web.Http
Imports DataModels
Imports AppCommon

Namespace Controllers
    Public Class ReissueController
        Inherits ApiController

        ' POST: api/Reissue
        Public Function PostValue(<FromBody()> ByVal value As Reissue) As IHttpActionResult

            Using db As New SMContext

                ' 在庫検索
                Dim stock = db.Stocks.Find(value.Stock_Id)

                ' 在庫状態確認
                If stock Is Nothing Then
                    Dim res As New Results.BadRequestErrorMessageResult("stock is empty", Me)
                    Return res
                End If

                ' ラベル再発行データの登録
                Dim reissue = New LabelReissue(value)
                reissue.Stock_Id = value.Stock_Id
                reissue.Status = LabelPrintStatusType.NotYet
                reissue.ModifyExpirationDate = value.ModifyExpirationDate
                reissue.ModifyExtendedFlag = value.ModifyExtendedFlag
                reissue.ModifyOpeningDate = value.ModifyOpeningDate
                reissue.ModifyOpenedFlag = value.ModifyOpenedFlag

                db.LabelReissues.Add(reissue)
                db.SaveChanges()
            End Using

            Return Ok()
        End Function

    End Class
End Namespace