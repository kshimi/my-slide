﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Linq
Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports System.Web.Http.Description
Imports DataModels

Namespace Controllers
    Public Class MasterUsersController
        Inherits System.Web.Http.ApiController

        Private db As New SMContext

        <Route("api/MasterUsersByRevision/{revision}")>
        Function GetUsersByRevision(ByVal revision As Integer) As IEnumerable(Of MasterUser)
            Dim q = From u In db.Users
                    Where u.Revision > revision
                    Order By u.Revision
                    Select u

            Return q.Take(1000).ToArray()
        End Function

        ' GET: api/MasterUsers
        Function GetUsers() As IEnumerable(Of MasterUser)
            Return db.Users.ToArray()
        End Function

        ' GET: api/MasterUserNewest
        <Route("api/MasterUserNewest")>
        <ResponseType(GetType(MasterUser))>
        Function GetMasterUserNewest() As IHttpActionResult
            If db.Users.Count > 0 Then
                Return Ok(db.Users.OrderByDescending(Function(u) u.Revision).First)
            Else
                Return NotFound()
            End If
        End Function

        ' GET: api/MasterUsers/5
        <ResponseType(GetType(MasterUser))>
        Function GetMasterUser(ByVal id As String) As IHttpActionResult
            Dim masterUser As MasterUser = db.Users.Find(id)
            If IsNothing(masterUser) Then
                Return NotFound()
            End If

            Return Ok(masterUser)
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace