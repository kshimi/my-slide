﻿Imports System.Data
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Linq
Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports System.Web.Http.Description
Imports DataModels

Namespace Controllers
    Public Class SchedulesController
        Inherits System.Web.Http.ApiController

        Private db As New SMContext

        <Route("api/SchedulesByRevision/{revision}")>
        Function GetSchedulesByRevision(ByVal revision As Integer) As IEnumerable(Of Schedule)
            Dim q = From o In db.Schedules
                    Where o.Revision > revision
                    Order By o.Revision
                    Select o

            Return q.Take(1000).ToArray()
        End Function

        ' GET: api/Schedules
        Function GetSchedules() As IEnumerable(Of Schedule)
            Return db.Schedules.ToArray()
        End Function

        ' GET: api/ScheduleNewest
        <Route("api/ScheduleNewest")>
        <ResponseType(GetType(Schedule))>
        Function GetScheduleNewest() As IHttpActionResult
            If db.Schedules.Count > 0 Then
                Return Ok(db.Schedules.OrderByDescending(Function(u) u.Revision).First)
            Else
                Return NotFound()
            End If
        End Function

        ' GET: api/Schedules/5
        <ResponseType(GetType(Schedule))>
        Function GetSchedule(ByVal id As Integer) As IHttpActionResult
            Dim schedule As Schedule = db.Schedules.Find(id)
            If IsNothing(schedule) Then
                Return NotFound()
            End If

            Return Ok(schedule)
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace