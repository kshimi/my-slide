﻿Imports System.Data
Imports System.Data.Entity
Imports System.Net
Imports System.Web.Http
Imports DataModels
Imports AppCommon

Namespace Controllers
    Public Class ArrivalController
        Inherits ApiController

        ' POST: api/Arrival
        Public Function PostValue(<FromBody()> ByVal value As Arrival) As IHttpActionResult
            Try
                Using db As New SMContext

                    ' 原材料を新規入力されたとき、原材料を登録する
                    Dim material As MasterMaterial
                    If value.NewMaterial IsNot Nothing Then
                        material = db.Materials.Add(value.NewMaterial)
                        db.SaveChanges()

                        value.MasterMaterial = material
                        value.MasterMaterial_Id = material.Id
                    Else
                        material = If(value.MasterMaterial, db.Materials.Find(value.MasterMaterial_Id))
                    End If

                    ' 発行履歴発行
                    Dim arrivalHistory As New ArrivalHistory(value)
                    db.ArrivalHistories.Add(arrivalHistory)

                    ' ラベル発行対象外の原材料は在庫作成しない
                    Dim stocks As IList(Of Stock) = Nothing
                    If material.IsLabelExcept Is Nothing OrElse material.IsLabelExcept = 0 Then

                        ' 発行済み在庫の最大ロット枝番取得
                        Dim LastBranchNo = DataModels.Stock.LastBranchNo(db, value)

                        ' 在庫作成
                        stocks = DataModels.Stock.StockFactory(value, arrivalHistory, LastBranchNo)

                        ' 在庫に原材料への紐付け
                        If value.MasterMaterial_Id Is Nothing AndAlso
                            value.Schedule_Id IsNot Nothing Then

                            Dim materialId As System.Nullable(Of Integer) = db.Schedules.Find(value.Schedule_Id).MasterMaterial_Id
                            For Each s As Stock In stocks
                                s.MasterMaterial_Id = materialId
                            Next

                        End If

                        db.Stocks.AddRange(stocks)
                    Else
                        arrivalHistory.Status = LabelPrintStatusType.ExceptIssue
                    End If

                    db.SaveChanges()

                    ' 発行結果を発行予定に反映（合計の発行数量が予定数量以上になったら、完了状態になる）
                    If value.Schedule_Id IsNot Nothing Then
                        Schedule.Arrival(db, value.Schedule_Id, value.InsertUser)
                    End If
                    db.SaveChanges()

                    ' ラベル発行
                    If value.IsDelayedPrint = 0 AndAlso stocks IsNot Nothing Then
                        Dim hasError As Boolean = False
                        stocks.Last.IsLastLabel = True
                        For Each stock In stocks
                            If stock.MasterMaterial Is Nothing Then
                                stock.MasterMaterial = material
                            End If

                            Try
                                Models.LabelPrintModel.PrintLabel(stock)
                            Catch ex As Exception
                                hasError = True
                            End Try
                        Next

                        If Not hasError Then
                            arrivalHistory.Status = LabelPrintStatusType.Printed
                            arrivalHistory.UpdateDate = Now
                            arrivalHistory.UpdateUser = value.InsertUser
                        End If
                        db.SaveChanges()
                    End If
                End Using

                Return Ok()
            Catch ex As Exception
                Dim mesg = ex.Message + " : " + ex.StackTrace
                Return New Results.BadRequestErrorMessageResult(mesg, Me)
            End Try
        End Function
    End Class
End Namespace