﻿Imports System.Configuration
Imports System.Web
Imports SATO
Imports AppCommon.Models
Imports DataModels

Namespace Models
    Public Class LabelPrintModel

        Public Shared Sub PrintLabel(stock As Stock)
            Dim ml As New MLComponent.MLComponent
            Dim label As New LabelQRModel(stock)

            Dim layoutFile As String = ConfigurationManager.AppSettings.Item("LabelLayoutFile").ToString
            Dim layoutFilePath As String = System.Web.Hosting.HostingEnvironment.MapPath("/App_Data/" & layoutFile)

            ml.LayoutFile = layoutFilePath
            ml.Setting = ConfigurationManager.AppSettings.Item("LabelPrinterName").ToString
            ml.PrnData = label.LabelPrintStr()

            If stock.IsLastLabel Then
                ml.EjectCut = True
            End If

            Dim result As Integer

            result = ml.OpenPort(1)
            If result <> 0 Then
                '接続エラー
                Throw New Exception("ML Component OpenPort error " & result.ToString)
            End If

            result = ml.Output
            If result <> 0 Then
                '発行エラー
                Throw New Exception("ML Component Output error " & result.ToString & vbCrLf & layoutFilePath)
            End If

            result = ml.ClosePort
            If result <> 0 Then
                '切断エラー
                Throw New Exception("ML Component ClosePort error " & result.ToString)
            End If
        End Sub
    End Class
End Namespace