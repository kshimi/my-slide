﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports AppCommon.Models
Imports ManagePC

<TestClass()> Public Class TestImportScheduleModel

    <TestMethod()> Public Sub TestMethod1()
        Dim m = New ImportScheduleModel("HOGE_150729.csv")

        Assert.IsTrue(m.IsValid)
    End Sub

    <TestMethod()> Public Sub TestMethod2()
        Dim m = New ImportScheduleModel("HOGE_HOGE.csv")

        Assert.IsFalse(m.IsValid)
    End Sub

    <TestMethod()> Public Sub TestMethod3()
        Dim m = New ImportScheduleModel("150729")

        Assert.IsFalse(m.IsValid)
    End Sub

    <TestMethod()> Public Sub TestMethod4()
        Dim m = New ImportScheduleModel("150729HOGE.csv")

        Assert.IsFalse(m.IsValid)
    End Sub

    <TestMethod()> Public Sub TestMethod5()
        Dim m = New ImportScheduleModel("150729_HOGE.csv")

        Assert.IsTrue(m.IsValid)
    End Sub
End Class