﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports MasterEdit

Public Class ProductionLot
    Inherits ModelBase

    <Key()> <Column(Order:=0)> Public Property ProductionMonth As DateTime
    <Key()> <Column(Order:=1)> Public Property ItemId As Integer
    <ForeignKey("ItemId")> Public Overridable Property MasterItem As MasterItem

    <Key()> <Column(Order:=2)> Public Property ItemNo As Integer
    Public Property LotNo As String
    Public Property LotMemo As String

    Public Shared Function NewLot(ExistingLots As IEnumerable(Of ProductionLot), ProductionMonth As DateTime, Item As MasterItem, numLot As Integer, db As IBContext) As IList(Of ProductionLot)
        Dim ProductionMonthWithLead = ProductionMonth.AddMonths(If(Item.LeadTime, 0) * -1)
        Dim lastLots = ExistingLots.Where(Function(x) x.ProductionMonth < ProductionMonth)
        If db IsNot Nothing Then
            lastLots = lastLots.Union(db.ProductionLots.Where(Function(x) x.ItemId = Item.Id And x.ProductionMonth < ProductionMonth).ToList)
        End If

        Dim lotPrefix As String = If(ProductionMonthWithLead.Month >= 4, ProductionMonthWithLead.ToString("yy"), ProductionMonthWithLead.AddYears(-1).ToString("yy"))
        Dim lotNum As Integer = 0
        Dim lotPostfix As String = "A"
        If lastLots IsNot Nothing AndAlso lastLots.Any() Then
            Dim lastLot = lastLots.Max(Function(x) x.LotNo)
            Dim lastLotPrefix = Left(lastLot, 2)
            Dim lotNumStr = Mid(lastLot, 3, 2)
            lotPostfix = lastLot.Substring(4)
            If lastLotPrefix = lotPrefix AndAlso IsNumeric(lotNumStr) Then
                lotNum = CInt(lotNumStr)
            End If
        End If

        Return Enumerable.Range(lotNum + 1, numLot).
            Select(Function(x) lotPrefix & x.ToString("00") & lotPostfix).
            Select(Function(x) New ProductionLot With {
                       .ProductionMonth = ProductionMonth,
                       .ItemId = Item.Id,
                       .LotNo = x
                   }).ToList
    End Function
End Class
