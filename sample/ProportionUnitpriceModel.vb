﻿Imports System.Data.Entity.SqlServer

Public Class ProportionUnitpriceModel

    ''' <summary>
    ''' 比例費単価の算出（販売予算取り込み時）
    ''' </summary>
    ''' <param name="importModel"></param>
    Public Shared Sub DoCalculation(importModel As SalesBudget, userid As String, db As DbEntities)
        If importModel.budgetClass Is Nothing Then Exit Sub

        Dim binditems = From budget In db.SALES_BUDGET
                        Where budget.BUDGET_DIV = importModel.budgetClass And
                            budget.TARGET_YEAR = importModel.budgetYear.Year
                        Group Join cost In db.STANDARD_COST On budget.ITEM_CD Equals cost.ITEM_CD And budget.BUDGET_DIV Equals cost.BUDGET_DIV Into costList = Group
                        From cost In costList.DefaultIfEmpty
                        Group Join item In db.ITEMs On budget.ITEM_CD Equals item.ITEM_CD Into itemList = Group
                        From item In itemList.DefaultIfEmpty
                        Select
                            SECTION_CD = If(item Is Nothing, budget.SECTION_CD, item.SECTION_CD),
                            BRAND_DIVISION_PARENT = budget.BRAND_DIVISION_PARENT,
                            BRAND_DIVISION_CHILD = budget.BRAND_DIVISION_CHILD,
                            ARTICLE_DIVISION = If(item Is Nothing, budget.ARTICLE_DIVISION, item.ARTICLE_DIVISION),
                            MATERIAL_DIV = If(item Is Nothing, budget.MATERIAL_DIV, item.MATERIAL_DIV),
                            ITEM_CD = If(item Is Nothing, budget.ITEM_CD, item.ITEM_CD),
                            budget,
                            costList

        Dim unitprices = New Dictionary(Of Key, Value)
        Dim newUnitprices = New List(Of PROPORTION_UNITPRICE)
        Dim sections = db.REPRESENTATIVE_SECTION.ToDictionary(Function(x) x.SECTION_CD)

        For Each eachItem In binditems.ToList
            Dim costByQuarter = eachItem.costList.ToDictionary(Function(it) it.ACTIVE_DATE)
            If Not sections.ContainsKey(eachItem.SECTION_CD) Then Continue For

            For Each budgetDetail In eachItem.budget.SALES_BUDGET_DETAIL
                Dim q = QMonth(eachItem.budget.TARGET_YEAR, budgetDetail.TARGET_MONTH)
                If Not costByQuarter.ContainsKey(q) Then Continue For
                Dim standardCost = costByQuarter(q)

                budgetDetail.VP_COST = (standardCost.VP_COST_SUM / 1000) * budgetDetail.WEIGHT                          '比例製造費
                budgetDetail.VP_SELLING_COST = (standardCost.VP_SELLING_COST_SUM / 1000) * budgetDetail.WEIGHT          '比例販売費
                budgetDetail.MARGINAL_PROFIT = budgetDetail.COST - budgetDetail.VP_COST - budgetDetail.VP_SELLING_COST  '限界利益

                Dim key = New Key With {
                    .BUDGET_DIV = eachItem.budget.BUDGET_DIV,
                    .TARGET_MONTH = GetMonth(eachItem.budget.TARGET_YEAR, budgetDetail.TARGET_MONTH),
                    .REPRESENTATIVE_SECTION_CD = sections(eachItem.SECTION_CD).REPRESENTATIVE_SECTION_CD,
                    .BRAND_DIVISION_PARENT = eachItem.BRAND_DIVISION_PARENT,
                    .BRAND_DIVISION_CHILD = eachItem.BRAND_DIVISION_CHILD,
                    .ARTICLE_DIVISION = eachItem.ARTICLE_DIVISION,
                    .MATERIAL_DIV = eachItem.MATERIAL_DIV
                }

                Dim unitprice As Value
                If unitprices.ContainsKey(key) Then
                    unitprice = unitprices(key)
                Else
                    unitprice = New Value
                    unitprices.Add(key, unitprice)
                End If
                unitprice.weightSum += budgetDetail.WEIGHT
                unitprice.vpCostSum += budgetDetail.VP_COST
                unitprice.vpSellingCostSum += budgetDetail.VP_SELLING_COST
            Next
        Next

        Dim existUnitPrices = db.PROPORTION_UNITPRICE.ToDictionary(Function(it) New Key With {
                                                                        .BUDGET_DIV = it.BUDGET_DIV,
                                                                        .TARGET_MONTH = it.TARGET_MONTH,
                                                                        .REPRESENTATIVE_SECTION_CD = it.REPRESENTATIVE_SECTION_CD,
                                                                        .BRAND_DIVISION_PARENT = it.BRAND_DIVISION_PARENT,
                                                                        .BRAND_DIVISION_CHILD = it.BRAND_DIVISION_CHILD,
                                                                        .ARTICLE_DIVISION = it.ARTICLE_DIVISION,
                                                                        .MATERIAL_DIV = it.MATERIAL_DIV})
        For Each unitprice In unitprices
            NewUnitprice(newUnitprices, unitprice.Key, unitprice.Value, userid, existUnitPrices, db)
        Next

        db.PROPORTION_UNITPRICE.AddRange(newUnitprices)
        UpdateStatusModel.AddUpdateStatus(UpdateStatusModel.FuncType.ProportionUnitprice,
                                                              UpdateStatusModel.Operation.CalcWbudget,
                                                              importModel.budgetClass,
                                                              Nothing,
                                                              Nothing,
                                                              importModel.budgetYear.Year * 100,
                                                              Nothing,
                                                              Nothing,
                                                              userid,
                                                              db)
    End Sub

    ''' <summary>
    ''' 比例費単価の算出（標準原価マスタ取り込み時）
    ''' </summary>
    ''' <param name="importModel"></param>
    Public Shared Sub DoCalculation(importModel As StandardCost, userid As String, db As DbEntities)
        If importModel.budgetClass Is Nothing Then Exit Sub

        Dim activeDate = QuarterModel.GetActiveDate(importModel.budgetYear, importModel.budgetQuarter)
        Dim binditems = From cost In db.STANDARD_COST
                        Where cost.BUDGET_DIV = importModel.budgetClass And
                            cost.ACTIVE_DATE = activeDate
                        Group Join budget In db.SALES_BUDGET On cost.ITEM_CD Equals budget.ITEM_CD And cost.BUDGET_DIV Equals budget.BUDGET_DIV Into budgetList = Group
                        From budget In budgetList.DefaultIfEmpty
                        Group Join item In db.ITEMs On budget.ITEM_CD Equals item.ITEM_CD Into itemList = Group
                        From item In itemList.DefaultIfEmpty
                        Select
                            SECTION_CD = If(item Is Nothing, budget.SECTION_CD, item.SECTION_CD),
                            BRAND_DIVISION_PARENT = budget.BRAND_DIVISION_PARENT,
                            BRAND_DIVISION_CHILD = budget.BRAND_DIVISION_CHILD,
                            ARTICLE_DIVISION = If(item Is Nothing, budget.ARTICLE_DIVISION, item.ARTICLE_DIVISION),
                            MATERIAL_DIV = If(item Is Nothing, budget.MATERIAL_DIV, item.MATERIAL_DIV),
                            ITEM_CD = If(item Is Nothing, budget.ITEM_CD, item.ITEM_CD),
                            budget,
                            cost

        Dim unitprices = New Dictionary(Of Key, Value)
        Dim newUnitprices = New List(Of PROPORTION_UNITPRICE)
        Dim sections = db.REPRESENTATIVE_SECTION.ToDictionary(Function(x) x.SECTION_CD)

        For Each eachItem In binditems.ToList
            If eachItem.SECTION_CD Is Nothing OrElse
                    eachItem.BRAND_DIVISION_PARENT Is Nothing OrElse
                    eachItem.BRAND_DIVISION_CHILD Is Nothing OrElse
                    eachItem.ARTICLE_DIVISION Is Nothing OrElse
                    eachItem.MATERIAL_DIV Is Nothing Then Continue For

            Dim budgetByMonth = eachItem.budget.SALES_BUDGET_DETAIL.ToLookup(Function(it) it.TARGET_MONTH)
            For Each m In MonthOfQList(eachItem.cost.ACTIVE_DATE)
                For Each budgetDetail In budgetByMonth(m)
                    Dim standardCost = eachItem.cost

                    budgetDetail.VP_COST = (standardCost.VP_COST_SUM / 1000) * budgetDetail.WEIGHT                   '比例製造費
                    budgetDetail.VP_SELLING_COST = (standardCost.VP_SELLING_COST_SUM / 1000) * budgetDetail.WEIGHT   '比例販売費
                    budgetDetail.MARGINAL_PROFIT = budgetDetail.COST - budgetDetail.VP_COST - budgetDetail.VP_SELLING_COST  '限界利益

                    Dim key = New Key With {
                        .BUDGET_DIV = eachItem.cost.BUDGET_DIV,
                        .TARGET_MONTH = GetMonth(eachItem.budget.TARGET_YEAR, budgetDetail.TARGET_MONTH),
                        .REPRESENTATIVE_SECTION_CD = sections(eachItem.SECTION_CD).REPRESENTATIVE_SECTION_CD,
                        .BRAND_DIVISION_PARENT = eachItem.BRAND_DIVISION_PARENT,
                        .BRAND_DIVISION_CHILD = eachItem.BRAND_DIVISION_CHILD,
                        .ARTICLE_DIVISION = eachItem.ARTICLE_DIVISION,
                        .MATERIAL_DIV = eachItem.MATERIAL_DIV
                        }

                    Dim unitprice As Value
                    If unitprices.ContainsKey(key) Then
                        unitprice = unitprices(key)
                    Else
                        unitprice = New Value
                        unitprices.Add(key, unitprice)
                    End If
                    unitprice.weightSum += If(budgetDetail.WEIGHT, 0.0D)
                    unitprice.vpCostSum += If(budgetDetail.VP_COST, 0.0D)
                    unitprice.vpSellingCostSum += If(budgetDetail.VP_SELLING_COST, 0.0D)
                Next
            Next
        Next

        Dim existUnitPrices = db.PROPORTION_UNITPRICE.ToDictionary(Function(it) New Key With {
                                                                        .BUDGET_DIV = it.BUDGET_DIV,
                                                                        .TARGET_MONTH = it.TARGET_MONTH,
                                                                        .REPRESENTATIVE_SECTION_CD = it.REPRESENTATIVE_SECTION_CD,
                                                                        .BRAND_DIVISION_PARENT = it.BRAND_DIVISION_PARENT,
                                                                        .BRAND_DIVISION_CHILD = it.BRAND_DIVISION_CHILD,
                                                                        .ARTICLE_DIVISION = it.ARTICLE_DIVISION,
                                                                        .MATERIAL_DIV = it.MATERIAL_DIV})
        For Each unitprice In unitprices
            NewUnitprice(newUnitprices, unitprice.Key, unitprice.Value, userid, existUnitPrices, db)
        Next

        db.PROPORTION_UNITPRICE.AddRange(newUnitprices)
        UpdateStatusModel.AddUpdateStatus(UpdateStatusModel.FuncType.ProportionUnitprice,
                                                              UpdateStatusModel.Operation.CalcWprice,
                                                              importModel.budgetClass,
                                                              Nothing,
                                                              Nothing,
                                                              importModel.budgetYear.Year * 100,
                                                              Nothing,
                                                              Nothing,
                                                              userid,
                                                              db)
    End Sub

    Private Shared Sub NewUnitprice(ByRef unitprices As List(Of PROPORTION_UNITPRICE), key As Key, value As Value, userid As String, existUnitPrices As Dictionary(Of Key, PROPORTION_UNITPRICE), db As DbEntities)
        If value.weightSum = 0 Then Exit Sub

        Dim unitprice = New PROPORTION_UNITPRICE With {
            .BUDGET_DIV = key.BUDGET_DIV,
            .TARGET_MONTH = key.TARGET_MONTH,
            .REPRESENTATIVE_SECTION_CD = key.REPRESENTATIVE_SECTION_CD,
            .BRAND_DIVISION_PARENT = key.BRAND_DIVISION_PARENT,
            .BRAND_DIVISION_CHILD = key.BRAND_DIVISION_CHILD,
            .ARTICLE_DIVISION = key.ARTICLE_DIVISION,
            .MATERIAL_DIV = key.MATERIAL_DIV,
            .VP_UNITPRICE = (value.vpCostSum * 1000) / value.weightSum,
            .VP_SELLING_UNITPRICE = (value.vpSellingCostSum * 1000) / value.weightSum,
            .INPUT_DATE = Now,
            .INPUTOR_CD = userid,
            .UPDATE_DATE = Now,
            .UPDATOR_CD = userid,
            .FIXED = True
            }

        If Not existUnitPrices.ContainsKey(key) Then
            unitprices.Add(unitprice)
        Else
            Dim exists = existUnitPrices(key)

            exists.VP_UNITPRICE = unitprice.VP_UNITPRICE
            exists.VP_SELLING_UNITPRICE = unitprice.VP_SELLING_UNITPRICE
            exists.UPDATE_DATE = Now
            exists.UPDATOR_CD = userid
        End If
    End Sub

    Private Shared Function QMonth(year As Decimal, month As Decimal) As Date
        Dim m As Integer
        Dim yearAdd As Integer = 0
        Select Case CInt(month)
            Case 4, 5, 6
                m = 4
            Case 7, 8, 9
                m = 7
            Case 10, 11, 12
                m = 10
            Case 1, 2, 3
                m = 1
                yearAdd = 1
            Case Else
                m = 4
        End Select

        Return New Date(year + yearAdd, m, 1)
    End Function
    Private Shared Function MonthOfQList(activeDate As Date) As IEnumerable(Of Integer)
        Dim firstMonth = activeDate.Month
        Return Enumerable.Range(firstMonth, 3).AsEnumerable
    End Function

    Public Shared Function GetMonth(year As Decimal, month As Decimal) As Decimal
        Dim resultMonth = (year + If(month <= 3, 1, 0)) * 100
        Return resultMonth + month
    End Function

    Public Structure Key
        Public Property BUDGET_DIV As String
        Public Property TARGET_MONTH As Decimal
        Public Property REPRESENTATIVE_SECTION_CD As String
        Public Property BRAND_DIVISION_PARENT As String
        Public Property BRAND_DIVISION_CHILD As String
        Public Property ARTICLE_DIVISION As Decimal?
        Public Property MATERIAL_DIV As Double?
    End Structure

    Public Class Value
        Public Sub New()
            weightSum = 0D
            vpCostSum = 0D
            vpSellingCostSum = 0D
        End Sub

        Public Property weightSum As Decimal
        Public Property vpCostSum As Decimal
        Public Property vpSellingCostSum As Decimal
    End Class
End Class
