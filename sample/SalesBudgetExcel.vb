﻿Imports ClosedXML.Excel

Public Class SalesBudgetExcel
    Inherits ExcelBase

    ''' <summary>
    ''' Excelからデータ取り込み
    ''' </summary>
    ''' <param name="importModel"></param>
    ''' <returns></returns>
    Public Shared Function ImportExcel(importModel As SalesBudget, userid As String) As IList(Of ImportItem)

        Using book = New XLWorkbook(importModel.uploadFile.InputStream)

            '項目ごとの取り込み処理定義
            Using db As New DbEntities
                Dim cellImportItems = SetImportItems(db)
                Dim organizationsById = db.ORGANIZATIONs.ToDictionary(Function(x) x.ORGANIZATION_CD)

                Dim dataItems As New List(Of ImportItem)
                For Each dataSheet In book.Worksheets.Where(Function(x) (Not x.Name.Contains("計")) AndAlso (Not x.Name.Contains("リスト")))

                    Dim firstRowNum = 5
                    Dim firstColNum = 2
                    Dim itemColNum = 10
                    Dim lastRow = dataSheet.LastRowUsed
                    Dim lastCol = dataSheet.LastColumnUsed
                    Dim dataRange = dataSheet.Range(firstRowNum, firstColNum, lastRow.RowNumber, lastCol.ColumnNumber)

                    'Excelからの値取り込み
                    For Each r In dataRange.Rows
                        ' === 行ごと処理項目を初期化する
                        Dim initComp = cellImportItems(0)
                        initComp.setValue(Nothing, Nothing)

                        ' === 品目を取り込む
                        Dim item As New ImportItem With {
                            .DataObject = New SALES_BUDGET With {
                                .TARGET_YEAR = importModel.budgetYear.Year,
                                .ORGANIZATION_CD = importModel.organization,
                                .ORGANIZATION_NAME = If(organizationsById.ContainsKey(importModel.organization), organizationsById(importModel.organization).ORGANIZATION_NAME, String.Empty),
                                .BUDGETKIND_CD = importModel.budgetKind,
                                .INPUT_DATE = Now,
                                .INPUTOR_CD = userid,
                                .UPDATE_DATE = Now,
                                .UPDATOR_CD = userid},
                            .ErrorMessages = New List(Of ErrorInfo),
                            .RowData = r.Cells.Select(Function(it) If(it.HasFormula, it.ValueCached, it.GetString)).ToList}

                        '品名コードのチェックを先に行う（品名コードがマスタに引き当るかのチェック）
                        Dim itemCell = r.Cells(Function(cell) cell.Address.ColumnNumber = itemColNum)
                        Dim itemComp = cellImportItems(itemColNum)
                        Dim itemErrors = itemComp.check(itemCell.First)
                        If Not itemErrors.Any Then
                            itemComp.setValue(item.DataObject, itemCell.First)
                        Else
                            '' エラーメッセージの記録
                            item.ErrorMessages.AddRange(itemErrors)
                        End If

                        ' === 品目以外の項目を取り込む
                        For Each c In r.Cells
                            If Not cellImportItems.ContainsKey(c.Address.ColumnNumber) Then Continue For
                            If c.Address.ColumnNumber = itemColNum Then Continue For

                            Dim comp = cellImportItems(c.Address.ColumnNumber)
                            Dim errors = comp.check(c)
                            If Not errors.Any Then
                                comp.setValue(item.DataObject, c)
                            Else
                                '' エラーメッセージの記録
                                item.ErrorMessages.AddRange(errors)
                            End If
                        Next

                        '会計部門コードが設定され、「区分」が画面での選択と一致し、月ごとの数量、重量、金額が少なくとも一件設定されている行のみ取り込む
                        If Not String.IsNullOrWhiteSpace(DirectCast(item.DataObject, SALES_BUDGET).ITEM_CD) AndAlso
                                DirectCast(item.DataObject, SALES_BUDGET).BUDGET_DIV = importModel.budgetClass AndAlso
                            DirectCast(item.DataObject, SALES_BUDGET).SALES_BUDGET_DETAIL.Any(Function(it) If(it.QTY, 0) <> 0 OrElse If(it.WEIGHT, 0) <> 0 OrElse If(it.COST, 0) <> 0) Then

                            dataItems.Add(item)
                        End If
                    Next
                Next

                Return dataItems
            End Using
        End Using
    End Function

    ''' <summary>
    ''' 既存データ（同一キー）の削除
    ''' </summary>
    ''' <param name="model"></param>
    ''' <param name="db"></param>
    Public Shared Sub RemoveExists(model As SalesBudget, db As DbEntities)
        Dim unitprice_exists = db.PROPORTION_UNITPRICE.Where(Function(x) x.BUDGET_DIV = model.budgetClass And
                                                                 x.TARGET_MONTH >= model.budgetYear.Year * 100 + 4 And
                                                                 x.TARGET_MONTH <= (model.budgetYear.Year + 1) * 100 + 3)
        db.PROPORTION_UNITPRICE.RemoveRange(unitprice_exists)

        Dim exists = db.SALES_BUDGET.Where(Function(x) x.TARGET_YEAR = model.budgetYear.Year And
                                                  x.ORGANIZATION_CD = model.organization And
                                                  x.BUDGETKIND_CD = model.budgetKind And
                                                  x.BUDGET_DIV = model.budgetClass)
        db.SALES_BUDGET.RemoveRange(exists)
    End Sub

    ''' <summary>
    ''' Excel取り込み列の定義
    ''' </summary>
    ''' <returns></returns>
    Private Shared Function SetImportItems(db As DbEntities) As Dictionary(Of Integer, CellImportItem)
        Dim items = New Dictionary(Of Integer, CellImportItem)
        Dim idx = 1

        ' === マスタデータ
        Dim itemMaster = DictMaster(Of String, ITEM).MakeMaster(db.ITEMs, Function(it) it.ITEM_CD, "品目", Function(it) GetString(it))
        Dim namesMaster = db.NAMES.ToList
        Dim namesByName = namesMaster.ToLookup(Function(x) Tuple.Create(x.NAME_DIVISION, x.NAME01))
        Dim namesById = namesMaster.ToLookup(Function(x) Tuple.Create(x.NAME_DIVISION, x.NAME_CD))
        Dim namesByNumberedId = namesMaster.Where(Function(x) IsNumeric(x.NAME_CD)).ToLookup(Function(x) Tuple.Create(x.NAME_DIVISION, CStr(CInt(x.NAME_CD))))
        Dim sectionById = DictMaster(Of String, SECTION).MakeMaster(db.SECTIONs, Function(it) it.SECTION_CD, "会計部門", Function(it) GetString(it))
        Dim venderById = LookupMaster(Of String, VENDER).MakeMaster(db.VENDERs.Where(Function(it) it.VENDER_DIVISION = "TS").OrderByDescending(Function(it) it.VENDER_VERSION),
                                                                    Function(it) it.VENDER_CD, "得意先", Function(it) GetString(it))
        Dim loginById = DictMaster(Of String, LOGIN).MakeMaster(db.LOGINs, Function(it) it.TANTO_CD, "担当者", Function(it) GetString(it))
        Dim budgetclassMaster = LookupMaster(Of String, BUDGETCLASS).MakeMaster(db.BUDGETCLASSes, Function(it) it.BUDGETCLASS_NAME, "区分", Function(it) GetString(it))

        ' === 行単位項目の初期化
        Dim isItemMasterExist As Boolean
        Dim itemId As String
        Dim item As ITEM = Nothing
        Dim brandDivisionPid As String
        items.Add(0, New CellImportItem With {
                  .setValue = Sub(o, cell)
                                  isItemMasterExist = False
                                  itemId = String.Empty
                                  item = Nothing
                                  brandDivisionPid = String.Empty
                              End Sub})

        ' === 列ごとのチェック、値取得処理定義
        items.Add(inc(idx), New CellImportItem With {                   '会計部門コード
                  .check = Function(x) If(isItemMasterExist, {}, CheckRequired("会計部門コード", x).Union(sectionById.CheckKey(x))).ToList,
                  .setValue = Sub(o, cell)
                                  Dim s = DirectCast(o, SALES_BUDGET)
                                  s.SECTION_CD = If(isItemMasterExist, item.SECTION_CD, GetString(cell))
                                  If sectionById.ContainsKey(s.SECTION_CD) Then s.SECTION_NAME = sectionById(s.SECTION_CD).SECTION_NAME
                              End Sub})
        idx += 1 '会計部門
        items.Add(inc(idx), New CellImportItem With {                   '銘柄区分(親)コード
                  .check = Function(x) CheckRequired("銘柄区分(親)コード", x),
                  .setValue = Sub(o, cell)
                                  brandDivisionPid = GetString(cell)
                                  DirectCast(o, SALES_BUDGET).BRAND_DIVISION_PARENT = brandDivisionPid
                              End Sub})
        items.Add(inc(idx), New CellImportItem With {                   '銘柄区分(親)
                  .check = Function(x) CheckRequired("銘柄区分(親)", x),
                  .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).BRAND_DIVISION_PARENT_NAME = GetString(cell)})
        items.Add(inc(idx), New CellImportItem With {                   '銘柄区分(子)コード
                  .check = Function(x) CheckRequired("銘柄区分(子)コード", x),
                  .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).BRAND_DIVISION_CHILD = GetString(cell)})
        items.Add(inc(idx), New CellImportItem With {                   '銘柄区分(子)
                  .check = Function(x) CheckRequired("銘柄区分(子)", x),
                  .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).BRAND_DIVISION_CHILD_NAME = GetString(cell)})
        items.Add(inc(idx), New CellImportItem With {                   '製品群種別コード
                  .check = Function(x) If(isItemMasterExist, {}, CheckRequired("製品群種別コード", x).Union(CheckNameMaster(namesByNumberedId, "PRODUCT_DIV", "製品群種別", x, True))).ToList,
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).PRODUCT_DIV = If(isItemMasterExist, item.PRODUCT_DIV, CDec(GetString(cell)))
                                  DirectCast(o, SALES_BUDGET).PRODUCT_NAME = NameById(namesByNumberedId, "PRODUCT_DIV", DirectCast(o, SALES_BUDGET).PRODUCT_DIV)
                              End Sub})
        idx += 1 '製品群種別

        '品目コード *** 品目コード取り込みは最初に呼び出す必要がある ***
        items.Add(inc(idx), New CellImportItem With {
           .check = Function(x) CheckRequired("品目コード", x),
           .setValue = Sub(o, cell)
                           itemId = GetString(cell)
                           isItemMasterExist = itemMaster.ContainsKey(itemId)
                           If isItemMasterExist Then
                               item = itemMaster(itemId)
                           Else
                               item = Nothing
                           End If
                           DirectCast(o, SALES_BUDGET).ITEM_CD = itemId
                       End Sub})

        items.Add(inc(idx), New CellImportItem With {                 '品目名称
            .check = Function(x) If(isItemMasterExist, {}, CheckRequired("品目名称", x)),
            .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).ITEM_NAME = If(isItemMasterExist, item.ITEM_NAME, GetString(cell))}) '
        idx += 1 '廃番
        items.Add(inc(idx), New CellImportItem With {                   'ユーザーコード
                  .check = Function(x) {},
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).USER_CD = GetString(cell)
                                  DirectCast(o, SALES_BUDGET).USER_NAME = NameById(namesById, "USER", DirectCast(o, SALES_BUDGET).USER_CD)
                              End Sub})
        idx += 1 'ユーザー名称
        items.Add(inc(idx), New CellImportItem With {                   '得意先コード
                  .check = Function(x) {},
                  .setValue = Sub(o, cell)
                                  Dim s = DirectCast(o, SALES_BUDGET)
                                  s.TOP_VENDER_CD = GetString(cell)
                                  If venderById.Contains(s.TOP_VENDER_CD) Then s.TOP_VENDER_NAME = If(venderById(s.TOP_VENDER_CD).First.VENDER_NAME1, String.Empty)
                              End Sub})
        idx += 1 '得意先名称
        items.Add(inc(idx), New CellImportItem With {                   '二次商コード
                  .check = Function(x) {},
                  .setValue = Sub(o, cell)
                                  Dim s = DirectCast(o, SALES_BUDGET)
                                  s.VENDER_CD = GetString(cell)
                                  If venderById.Contains(s.VENDER_CD) Then s.VENDER_NAME = If(venderById(s.VENDER_CD).First.VENDER_NAME1, String.Empty)
                              End Sub})
        idx += 1 '二次商名称
        items.Add(inc(idx), New CellImportItem With {                   '担当者コード
                  .check = Function(x) CheckRequired("担当者コード", x).Union(loginById.CheckKey(x)).ToList,
                  .setValue = Sub(o, cell)
                                  Dim s = DirectCast(o, SALES_BUDGET)
                                  s.TANTO_CD = GetString(cell)
                                  If loginById.ContainsKey(s.TANTO_CD) Then s.TANTO_NAME = If(loginById(s.TANTO_CD).TANTO_NAME, String.Empty)
                              End Sub})
        idx += 1 '担当者名
        items.Add(inc(idx), New CellImportItem With {                   '用途分類コード
                  .check = Function(x) If(isItemMasterExist, {}, CheckNameMaster(namesByNumberedId, "USE_DIV", "用途分類コード", x, True)),
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).USE_DIV = If(isItemMasterExist, item.USE_DIV, CDec(GetString(cell)))
                                  DirectCast(o, SALES_BUDGET).USE_NAME = NameById(namesByNumberedId, "USE_DIV", DirectCast(o, SALES_BUDGET).USE_DIV)
                              End Sub})
        idx += 1 '用途分類
        items.Add(inc(idx), New CellImportItem With {                   '構成種別コード
                  .check = Function(x) If(isItemMasterExist, {}, CheckNameMaster(namesByNumberedId, "COMPOSITION_DIV", "構成種別コード", x, True)),
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).COMPOSITION_DIV = If(isItemMasterExist, item.COMPOSITION_DIV, CDec(GetString(cell)))
                                  DirectCast(o, SALES_BUDGET).COMPOSITION_NAME = NameById(namesByNumberedId, "COMPOSITION_DIV", DirectCast(o, SALES_BUDGET).COMPOSITION_DIV)
                              End Sub})
        idx += 1 '構成種別
        items.Add(inc(idx), New CellImportItem With {                   '原料分類コード
                  .check = Function(x) If(isItemMasterExist, {}, (CheckRequired("原料分類コード", x)).Union(CheckNameMaster(namesByNumberedId, "MATERIAL_DIV", "原料分類コード", x, True))).ToList,
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).MATERIAL_DIV = If(isItemMasterExist, item.MATERIAL_DIV, CDec(GetString(cell)))
                                  DirectCast(o, SALES_BUDGET).MATERIAL_NAME = NameById(namesByNumberedId, "MATERIAL_DIV", DirectCast(o, SALES_BUDGET).MATERIAL_DIV)
                              End Sub})
        idx += 1 '原料分類コード
        items.Add(inc(idx), New CellImportItem With {                   '仕様区分コード
                  .check = Function(x) If(isItemMasterExist, {}, CheckNameMaster(namesByNumberedId, "SPEC_DIV", "仕様区分コード", x, True)),
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).SPEC_DIV = If(isItemMasterExist, item.SPEC_DIV, CDec(GetString(cell)))
                                  DirectCast(o, SALES_BUDGET).SPEC_NAME = NameById(namesByNumberedId, "SPEC_DIV", DirectCast(o, SALES_BUDGET).SPEC_DIV)
                              End Sub})
        idx += 1 '仕様区分
        items.Add(inc(idx), New CellImportItem With {                   '汎用区分コード
                  .check = Function(x) If(isItemMasterExist, {}, CheckNameMaster(namesByNumberedId, "CUSTOM_DIV", "汎用区分コード", x, True)),
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).CUSTOM_DIV = If(isItemMasterExist, item.CUSTOM_DIV, CDec(GetString(cell)))
                                  DirectCast(o, SALES_BUDGET).CUSTOM_NAME = NameById(namesByNumberedId, "CUSTOM_DIV", DirectCast(o, SALES_BUDGET).CUSTOM_DIV)
                              End Sub})
        idx += 1 '汎用区分
        items.Add(inc(idx), New CellImportItem With {                   '販売品区分コード
                  .check = Function(x) If(isItemMasterExist, {}, CheckRequired("販売品区分コード", x).Union(ArticleDivisionModel.CheckId(x))).ToList,
                  .setValue = Sub(o, cell)
                                  DirectCast(o, SALES_BUDGET).ARTICLE_DIVISION = If(isItemMasterExist, item.ARTICLE_DIVISION, CDec(GetString(cell)))
                                  DirectCast(o, SALES_BUDGET).ARTICLE_NAME = ArticleDivisionModel.DispName(DirectCast(o, SALES_BUDGET).ARTICLE_DIVISION)
                              End Sub})
        idx += 1 '販売品区分コード
        items.Add(inc(idx), New CellImportItem With {                   '区分
                  .check = Function(x) CheckRequired("区分", x).Union(budgetclassMaster.CheckKey(x)).ToList,
                  .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).BUDGET_DIV = ReferBudgetclassMaster(budgetclassMaster, cell)})
        items.Add(inc(idx), New CellImportItem With {                   '単重
                  .check = Function(x) If(isItemMasterExist, {}, CheckRequired("単重", x).Union(CheckNumber("単重", x))).ToList,
                  .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).UNIT_WEIGHT = If(isItemMasterExist, item.UNIT_WEIGHT, GetNumeric(cell))})
        items.Add(inc(idx), New CellImportItem With {                   '上期単価
                  .check = Function(x) CheckRequired("上期単価", x).Union(CheckNumberNoZero("上期単価", x)).ToList,
                  .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).UNITPRICE_FIRST = GetNumeric(cell)})
        items.Add(inc(idx), New CellImportItem With {                   '下期単価
                  .check = Function(x) CheckRequired("下期単価", x).Union(CheckNumberNoZero("下期単価", x)).ToList,
                  .setValue = Sub(o, cell) DirectCast(o, SALES_BUDGET).UNITPRICE_SECOND = GetNumeric(cell)})
        idx += 1 '上期Kg単価
        idx += 1 '下期Kg単価

        items.Add(inc(idx), New CellImportItem With {                   '4月数量
                  .check = Function(x) CheckNumber("4月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 4, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '4月重量
                  .check = Function(x) CheckNumber("4月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 4, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '4月金額
                  .check = Function(x) CheckNumber("4月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 4, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '5月数量
                  .check = Function(x) CheckNumber("5月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 5, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '5月重量
                  .check = Function(x) CheckNumber("5月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 5, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '5月金額
                  .check = Function(x) CheckNumber("5月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 5, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '6月数量
                  .check = Function(x) CheckNumber("6月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 6, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '6月重量
                  .check = Function(x) CheckNumber("6月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 6, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '6月金額
                  .check = Function(x) CheckNumber("6月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 6, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '7月数量
                  .check = Function(x) CheckNumber("7月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 7, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '7月重量
                  .check = Function(x) CheckNumber("7月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 7, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '7月金額
                  .check = Function(x) CheckNumber("7月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 7, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '8月数量
                  .check = Function(x) CheckNumber("8月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 8, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '8月重量
                  .check = Function(x) CheckNumber("8月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 8, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '8月金額
                  .check = Function(x) CheckNumber("8月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 8, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '9月数量
                  .check = Function(x) CheckNumber("9月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 9, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '9月重量
                  .check = Function(x) CheckNumber("9月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 9, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '9月金額
                  .check = Function(x) CheckNumber("9月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 9, cell, AddressOf SetCost)})

        idx += 1 '上期計数量
        idx += 1 '下期計重量
        idx += 1 '下期計金額

        items.Add(inc(idx), New CellImportItem With {                   '10月数量
                  .check = Function(x) CheckNumber("10月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 10, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '10月重量
                  .check = Function(x) CheckNumber("10月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 10, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '10月金額
                  .check = Function(x) CheckNumber("10月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 10, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '11月数量
                  .check = Function(x) CheckNumber("11月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 11, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '11月重量
                  .check = Function(x) CheckNumber("11月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 11, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '11月金額
                  .check = Function(x) CheckNumber("11月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 11, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '12月数量
                  .check = Function(x) CheckNumber("12月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 12, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '12月重量
                  .check = Function(x) CheckNumber("12月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 12, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '12月金額
                  .check = Function(x) CheckNumber("12月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 12, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '1月数量
                  .check = Function(x) CheckNumber("1月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 1, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '1月重量
                  .check = Function(x) CheckNumber("1月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 1, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '1月金額
                  .check = Function(x) CheckNumber("1月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 1, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '2月数量
                  .check = Function(x) CheckNumber("2月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 2, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '2月重量
                  .check = Function(x) CheckNumber("2月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 2, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '2月金額
                  .check = Function(x) CheckNumber("2月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 2, cell, AddressOf SetCost)})

        items.Add(inc(idx), New CellImportItem With {                   '3月数量
                  .check = Function(x) CheckNumber("3月数量", x),
                  .setValue = Sub(o, cell) SetValue(o, 3, cell, AddressOf SetQty)})
        items.Add(inc(idx), New CellImportItem With {                   '3月重量
                  .check = Function(x) CheckNumber("3月重量", x),
                  .setValue = Sub(o, cell) SetValue(o, 3, cell, AddressOf SetWeight)})
        items.Add(inc(idx), New CellImportItem With {                   '3月金額
                  .check = Function(x) CheckNumber("3月金額", x),
                  .setValue = Sub(o, cell) SetValue(o, 3, cell, AddressOf SetCost)})

        Return items
    End Function

    Protected Shared Sub SetValue(o As SALES_BUDGET, month As Integer, cell As IXLCell, setAction As Action(Of SALES_BUDGET_DETAIL, Double))
        Dim detail As SALES_BUDGET_DETAIL

        If o.SALES_BUDGET_DETAIL.Any(Function(x) x.TARGET_MONTH = month) Then
            detail = o.SALES_BUDGET_DETAIL.First(Function(x) x.TARGET_MONTH = month)
        Else
            detail = New SALES_BUDGET_DETAIL With {
                .TARGET_MONTH = month,
                .INPUT_DATE = Now,
                .INPUTOR_CD = o.INPUTOR_CD,
                .UPDATE_DATE = Now,
                .UPDATOR_CD = o.UPDATOR_CD
            }
            o.SALES_BUDGET_DETAIL.Add(detail)
        End If

        setAction(detail, GetNumeric(cell))
    End Sub

    Protected Shared Sub SetQty(detail As SALES_BUDGET_DETAIL, value As Double)
        detail.QTY = value
    End Sub

    Protected Shared Sub SetWeight(detail As SALES_BUDGET_DETAIL, value As Double)
        detail.WEIGHT = value * 1000
    End Sub

    Protected Shared Sub SetCost(detail As SALES_BUDGET_DETAIL, value As Double)
        detail.COST = value
    End Sub

    Public Shared HeaderItems As String() = {
        "会計部門コード",
        "会計部門",
        "銘柄区分(親コード",
        "銘柄区分(親",
        "銘柄区分(子コード",
        "銘柄区分(子",
        "製品群種別コード",
        "製品群種別",
        "品目コード",
        "品目名称",
        "廃番",
        "ユーザーコード",
        "ユーザー名称",
        "得意先コード",
        "得意先名称",
        "二次商コード",
        "二次商名称",
        "担当者コード",
        "担当者名",
        "用途分類コード",
        "用途分類",
        "構成種別コード",
        "構成種別",
        "原料分類コード",
        "原料分類",
        "仕様区分コード",
        "仕様区分",
        "汎用区分コード",
        "汎用区分",
        "販売品区分コード",
        "販売品区分",
        "区分",
        "単重",
        "上期単価",
        "下期単価",
        "上期Kg単価",
        "下期Kg単価",
        "4月 数量",
        "4月 重量",
        "4月 金額",
        "5月 数量",
        "5月 重量",
        "5月 金額",
        "6月 数量",
        "6月 重量",
        "6月 金額",
        "7月 数量",
        "7月 重量",
        "7月 金額",
        "8月 数量",
        "8月 重量",
        "8月 金額",
        "9月 数量",
        "9月 重量",
        "9月 金額",
        "上期計 数量",
        "上期計 重量",
        "上期計 金額",
        "10月 数量",
        "10月 重量",
        "10月 金額",
        "11月 数量",
        "11月 重量",
        "11月 金額",
        "12月 数量",
        "12月 重量",
        "12月 金額",
        "1月 数量",
        "1月 重量",
        "1月 金額",
        "2月 数量",
        "2月 重量",
        "2月 金額",
        "3月 数量",
        "3月 重量",
        "3月 金額",
        "下期計 数量",
        "下期計 重量",
        "下期計 金額",
        "年計 数量",
        "年計 重量",
        "年計 金額",
        "前期実績あり"
    }

End Class

