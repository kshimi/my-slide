﻿Imports System.Linq
Imports ClosedXML.Excel

Public Class ExcelBase
#Region "入力値チェック処理"

    Protected Shared Function CheckRequired(itemName As String, cell As IXLCell) As IList(Of ErrorInfo)
        Dim errorMessages = New List(Of ErrorInfo)
        If cell.IsEmpty() Then
            errorMessages.Add(New ErrorInfo(cell, itemName & "が記入されていません。"))
        End If

        Return errorMessages
    End Function

    Protected Shared Function CheckNumber(itemName As String, cell As IXLCell) As IList(Of ErrorInfo)
        Dim errorMessages = New List(Of ErrorInfo)
        If cell.IsEmpty() Then Return errorMessages

        If (cell.DataType <> XLCellValues.Number) AndAlso
            Not (cell.HasFormula AndAlso IsNumeric(cell.ValueCached)) Then

            errorMessages.Add(New ErrorInfo(cell, itemName & "に記入されている値が数値になっていません。"))
        End If

        Return errorMessages
    End Function

    Protected Shared Function CheckNumberNoZero(itemName As String, cell As IXLCell, Optional t As ErrorInfo.ErrorType = ErrorInfo.ErrorType.TypeError) As IList(Of ErrorInfo)
        Dim errorMessages = CheckNumber(itemName, cell)
        Dim value = GetNumeric(cell)
        If value = 0.0 Then
            errorMessages.Add(New ErrorInfo(cell, itemName & "の値が0になっています。" & If(t = ErrorInfo.ErrorType.TypeWarning, "[確認]-取込み対象になります。", ""), t))
        End If

        Return errorMessages
    End Function

    Protected Shared Function CheckNameMaster(master As ILookup(Of Tuple(Of String, String), NAME), nameDiv As String, itemName As String, cell As IXLCell, Optional IsIdNumbered As Boolean = False) As IList(Of ErrorInfo)
        Dim errorMessages = New List(Of ErrorInfo)
        If cell.IsEmpty() Then Return errorMessages

        Dim idString = GetString(cell)
        Dim key = Tuple.Create(nameDiv, If(IsIdNumbered, CStr(CDec(idString)), idString))
        If Not master.Contains(key) Then
            errorMessages.Add(New ErrorInfo(cell, itemName & "で該当するマスタ項目がありません。"))
        End If

        Return errorMessages
    End Function

#End Region

#Region "マスター参照処理"

    Protected Shared Function ReferBudgetclassMaster(master As ILookup(Of String, BUDGETCLASS), cell As IXLCell) As String
        If cell.IsEmpty Then Return Nothing

        Return master(cell.GetString.Trim).First.BUDGETCLASS_CD
    End Function

    Protected Shared Function ReferNameMaster(master As ILookup(Of Tuple(Of String, String), NAME), nameDiv As String, cell As IXLCell) As String
        If cell.IsEmpty Then Return Nothing

        Return master(Tuple.Create(nameDiv, GetString(cell))).First.NAME_CD
    End Function

    Protected Shared Function NameById(master As ILookup(Of Tuple(Of String, String), NAME), nameDiv As String, id As String) As String
        If String.IsNullOrWhiteSpace(id) Then Return String.Empty
        Dim key = Tuple.Create(nameDiv, id)
        If Not master.Contains(key) Then Return String.Empty
        Dim item = master(key).FirstOrDefault

        Return If(item IsNot Nothing, item.NAME01, String.Empty)
    End Function

    Protected Shared Function GetNumeric(cell As IXLCell) As Double
        If cell.DataType = XLCellValues.Number Then
            Return cell.GetDouble
        ElseIf cell.HasFormula Then
            Return GetNumeric(cell.ValueCached)
        Else
            Return 0.0
        End If
    End Function

    Protected Shared Function GetNumeric(s As String) As Double
        If IsNumeric(s) Then
            Return Double.Parse(s)
        Else
            Return 0.0
        End If
    End Function

    Public Shared Function GetString(cell As IXLCell) As String
        Try
            Return cell.GetString.Trim
        Catch ex As Exception
            Return cell.ValueCached
        End Try
    End Function
#End Region

    Protected Shared Function inc(ByRef i As Integer) As Integer
        i += 1
        Return i
    End Function

    Protected Class CellImportItem
        Public check As Func(Of IXLCell, IList(Of ErrorInfo))
        Public setValue As Action(Of Object, IXLCell)
    End Class

    Public Class ErrorInfo
        Public sheetName As String
        Public cellAddress As String
        Public lineNo As Long
        Public message As String
        Public type As ErrorType

        Public Sub New(lineNo As Long, m As String)
            Me.lineNo = lineNo
            Me.message = m
        End Sub

        Public Sub New(cell As IXLCell, m As String, Optional t As ErrorType = ErrorType.TypeError)
            Me.sheetName = cell.Worksheet.Name
            Me.cellAddress = cell.Address.ToStringRelative
            Me.lineNo = cell.WorksheetRow.RowNumber
            Me.message = m
            Me.type = t
        End Sub

        Public Function WholeMessage() As String
            Return sheetName & ":" & cellAddress & ":" & message
        End Function

        Public Enum ErrorType
            TypeError
            TypeWarning
        End Enum
    End Class
    Public Class DictMaster(Of TKey, TValue)
        Inherits Dictionary(Of TKey, TValue)

        Public Property masterName As String = String.Empty
        Public Property getKey As Func(Of IXLCell, TKey)

        Public Sub New(dict As Dictionary(Of TKey, TValue))
            MyBase.New(dict)
        End Sub

        Public Function CheckKey(cell As IXLCell) As IList(Of ErrorInfo)
            Dim errorMessages = New List(Of ErrorInfo)
            If Not Me.ContainsKey(getKey(cell)) Then
                errorMessages.Add(New ErrorInfo(cell, masterName & "で該当するマスタ項目がありません。"))
            End If

            Return errorMessages
        End Function

        Public Shared Function MakeMaster(data As IEnumerable(Of TValue),
                                          makeKey As Func(Of TValue, TKey),
                                          name As String,
                                          getKey As Func(Of IXLCell, TKey)
                                          ) As DictMaster(Of TKey, TValue)

            Dim master = New DictMaster(Of TKey, TValue)(data.ToDictionary(makeKey))
            master.masterName = name
            master.getKey = getKey

            Return master
        End Function

    End Class

    Public Class LookupMaster(Of TKey, TValue)
        Implements ILookup(Of TKey, TValue)

        Private lookup As Lookup(Of TKey, TValue)
        Public Property masterName As String = String.Empty
        Public Property getKey As Func(Of IXLCell, TKey)

        Public ReadOnly Property Count As Integer Implements ILookup(Of TKey, TValue).Count
            Get
                Return lookup.Count
            End Get
        End Property

        Default Public ReadOnly Property Item(key As TKey) As IEnumerable(Of TValue) Implements ILookup(Of TKey, TValue).Item
            Get
                Return lookup.Item(key)
            End Get
        End Property

        Public Function Contains(key As TKey) As Boolean Implements ILookup(Of TKey, TValue).Contains
            Return lookup.Contains(key)
        End Function

        Public Function GetEnumerator() As IEnumerator(Of IGrouping(Of TKey, TValue)) Implements IEnumerable(Of IGrouping(Of TKey, TValue)).GetEnumerator
            Return lookup.GetEnumerator()
        End Function

        Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
            Return DirectCast(lookup, IEnumerable).GetEnumerator
        End Function

        Public Sub New(lookup As Lookup(Of TKey, TValue))
            Me.lookup = lookup
        End Sub

        Public Function CheckKey(cell As IXLCell) As IList(Of ErrorInfo)
            Dim errorMessages = New List(Of ErrorInfo)
            If Not Me.Contains(getKey(cell)) Then
                errorMessages.Add(New ErrorInfo(cell, masterName & "で該当するマスタ項目がありません。"))
            End If

            Return errorMessages
        End Function

        Public Shared Function MakeMaster(data As IEnumerable(Of TValue),
                                          makeKey As Func(Of TValue, TKey),
                                          name As String,
                                          getKey As Func(Of IXLCell, TKey)
                                          ) As LookupMaster(Of TKey, TValue)

            Dim master = New LookupMaster(Of TKey, TValue)(data.ToLookup(makeKey))
            master.masterName = name
            master.getKey = getKey

            Return master
        End Function

    End Class
End Class